import csv
import argparse


def read_csv(filename):
    """"""
    with open(filename) as ifile:
        reader = csv.reader(ifile, delimiter=",")
        headers = next(reader)
        column = {}
        for h in headers:
            column[h] = []
        for row in reader:
            for h, v in zip(headers, row):
                column[h].append(int(v))

        return column


def format_data(val_arr):
    """ transform into document level
    also transform 1,2,3 => 1 all positive
    split document with 4
    """
    docs = []
    d = []
    for v in val_arr:
        if v == 4 and (not d):
            # doc is empty
            pass
        elif v == 4 and d:
            docs.append(d)
            d = []
        else:
            if v > 0 and v < 4:
                d.append(1)
            else:
                d.append(v)
    return docs


def count_tp_tn_fp_fn(targets, preds):
    """
    expect document level data
    """
    sent_level = [0, 0, 0, 0]
    doc_level = [0, 0, 0, 0]

    accumulated_sents = 0
    for d_idx, d in enumerate(preds):
        d_pred = 0
        d_truth = 1 if 1 in targets[d_idx] else 0

        for s_idx, s in enumerate(d):
            if s_idx > len(targets[d_idx]) - 1:
                # label issue
                continue
            if s == targets[d_idx][s_idx] and s == 1:
                # TP
                sent_level[0] += 1
                d_pred = 1
            elif s == targets[d_idx][s_idx] and s == 0:
                # TN
                sent_level[1] += 1
            elif s != targets[d_idx][s_idx] and s == 1:
                # FP
                sent_level[2] += 1
                d_pred = 1
            else:
                # FN
                sent_level[3] += 1

        if d_pred == 1 and d_pred == d_truth:
            doc_level[0] += 1
        elif d_pred == 1 and d_pred != d_truth:
            doc_level[2] += 1
            print("FP doc index", d_idx)
            print("around sentences", accumulated_sents)
        elif d_pred == 0 and d_pred == d_truth:
            doc_level[1] += 1
        else:
            doc_level[3] += 1

        accumulated_sents += len(d)

    return doc_level, sent_level


def metrics(tp, tn, fp, fn):
    """ compute acc, prec, recall, f1
    based on tp, tn, fp, fn
    """
    prec = float(tp) / (tp + fp + 0.0001)
    acc = float(tp + tn) / (tp + tn + fp + fn)
    recall = float(tp) / (tp + fn)
    f1 = 2 * prec * recall / (prec + recall + 0.00001)
    return {'acc': acc, 'prec': prec, 'recall': recall, 'f1': f1}


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--data-file", required=True)
    args = arg_parser.parse_args()

    data = read_csv(args.data_file)

    our_label = data["our label"]
    whyper_preds = data["whyper predicts"]

    our_label = format_data(our_label)
    whyper_preds = format_data(whyper_preds)

    d_level, s_level = count_tp_tn_fp_fn(our_label, whyper_preds)
    print("document level", d_level, metrics(*d_level))
    print("sentence level", s_level, metrics(*s_level))


if __name__ == '__main__':
    main()
