import xlrd

def main():
    workbook = xlrd.open_workbook('Record_Audio.xls')
    worksheet = workbook.sheet_by_index(0)

    whyper_label = []
    relabel = []

    for i in range(worksheet.nrows):
        if i > 2 :
            whyper_label .append( worksheet.cell_value(i, 4) )
            relabel.append(worksheet.cell_value(i, 1))

    i = 0
    mismatch = 0
    while (i < len(whyper_label)):
        if relabel[i] != whyper_label[i]:
            mismatch += 1
        i += 1

    print("Total number of mismatch : "+str(mismatch))


if __name__ == "__main__":
    main()