import json
import argparse
from os.path import expanduser


# load the all description at once
# ALL_DESC_PATH = "~/Dev/Whyper/data/49183_apps_desctext.json"
ALL_DESC_PATH = "/Users/faysal/Desktop/My Computer/D/Code Workspace/Research-IoT/Collaboration/leigh/Whyper/data/49183_apps_desctext.json"
ALL_DESC_PATH = expanduser(ALL_DESC_PATH)
with open(ALL_DESC_PATH) as ifile:
    ALL_DESC = json.load(ifile)


def get_desc(apkname):
    """ get description of an application
    """
    if apkname not in ALL_DESC:
        return None

    desc = ALL_DESC[apkname]
    processed_desc = []
    for s in desc:
        item = {
            "s": s,
            "our label": "x",
            "autocog label": "a_l",
            "autocog pred": "a_p"
            }
        processed_desc.append(item)

    return processed_desc


def main():
    """main func"""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--in-file", required=True)
    arg_parser.add_argument("--out-file", required=True)
    args = arg_parser.parse_args()

    need_to_label = {}
    with open(args.in_file) as ifile:
        for line in ifile:
            apkname = line.strip()
            app_desc = get_desc(apkname)
            if app_desc:
                need_to_label[apkname] = app_desc

    with open(args.out_file, 'w') as ofile:
        json.dump(need_to_label, ofile, indent=2)


if __name__ == '__main__':
    main()
