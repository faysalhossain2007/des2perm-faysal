#!/usr/bin/env bash
glovepath='http://nlp.stanford.edu/data/glove.840B.300d.zip'

ZIPTOOL="unzip"

echo $glovepath
mkdir GloVe
curl -LO $glovepath
$ZIPTOOL glove.840B.300d.zip -d GloVe/
rm glove.840B.300d.zip