# Notes

# Dependencies

`pytorch`, 
`gensim`, 
`numpy`, 
`spacy`, 
`tensorboardX`


# How run latest experiments
Move to the `src` folder under terminal, then use following commands to launch
a parameter search experiment.

it will do greedy search based on the base template
`python parameters_search_on_target.py --base-conf ./paramters_search_conf/target/crx_proxy.1.json`

this command has data selection stage,
which uses a trained model on source datas to select finetuning data
in target domain.
`python parameters_search_on_target_ds.py --base-conf ./paramters_search_conf/target/crx_proxy.ds.1.json`



# Some explanations 
## directories
### `src`
This folder contains almost all source code. The lastest model we used is
implemented in `src/DAN` folder. While to invoke this model training 
you can use the script located at `src/train_DAN.py`. There are other scripts
can also invoke this model with different features, e.g. training with 
different training data size.


`src/common`, `src/domain_android`, `src/domain_crx`, `src/domain_ifttt` are
legacy implementation. They are implementation of MLP model with averaged
bag-of-word-vector model. Their functions are covered by `DAN` model.


`src/testCNNmodel.py` provides CNN model training. `src/lstm_with_pairs.py`
provides LSTM model training process. Experiments shows they are no better
than MLP model. Most of the time they are worse. 


### `data`
Data folder contain data files, word-vector models and experiments logs. 
Log path are controlled in scripts, so it is possible `temp` files or `logs`
are stored in different location.


Before cloning, make sure `git-lfs` has been installed.

