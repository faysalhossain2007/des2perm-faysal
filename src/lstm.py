import torch.nn as nn
import torch
import torch.nn.functional as F


class PairsLSTM(nn.Module):
    """
    accept input with a list of pairs
    """
    def __init__(self, args, wv_model):
        """
        :param args: parameters for model construction
        :param wv_model: used for initialize word embedding layer
        :type wv_model: np.array
        """
        super(PairsLSTM, self).__init__()
        self.hidden_dim = args.hidden_size
        self.num_layers = args.layers
        self.dropout = args.dropout
        self.device = torch.device(args.device)
        self.num_directions = 2 if args.bi_direct else 1
        self.bi_direct = args.bi_direct
        self.out_dim = args.out_dim

        # init word embeddings
        vocab_size, word_dim = wv_model.shape
        self.embeddings = nn.Embedding(vocab_size,
                                       word_dim,
                                       padding_idx=1)
        self.embeddings.weight.data.copy_(
            torch.from_numpy(wv_model))
        if args.freeze_wv:
            self.embeddings.weight.requires_grad = False

        # init LSTM layers
        if args.sent_presented:
            self.sent_lstm = nn.LSTM(word_dim, self.hidden_dim,
                                     self.num_layers,
                                     dropout=self.dropout,
                                     bidirectional=self.bi_direct)
            self.sent_hidden = self.init_hidden()

        self.pair_lstm = nn.LSTM(3*word_dim, self.hidden_dim,
                                 self.num_layers,
                                 dropout=self.dropout,
                                 bidirectional=self.bi_direct)
        self.pairs_hidden = self.init_hidden()

        # output layer: linear layer combine all hidden states
        if args.sent_presented:
            self.outlayer = nn.Linear(self.hidden_dim*4, self.out_dim)
        else:
            self.outlayer = nn.Linear(self.hidden_dim*2, self.out_dim)

    def init_hidden(self):
        """ only store last hidden state and cell state
        TODO try average pooling, record all hiddent states
        and cell states
        """
        dim = self.num_layers * self.num_directions
        hidden_states = (
            torch.zeros(dim, 1, self.hidden_dim, device=self.device),
            torch.zeros(dim, 1, self.hidden_dim, device=self.device))

        return hidden_states

    def feature_for_pair(self, pairs):
        """ transform each word in pair to embeddings,
        and take the difference
        """
        if not pairs:
            return torch.zeros(
                self.embeddings.embedding_dim*3,
                device=self.device).view(1, 1, -1)

        features = []
        for p in pairs:
            p = torch.tensor(p, device=self.device)
            w1 = self.embeddings(p[0])
            w2 = self.embeddings(p[1])
            d = w1 - w2
            f = torch.cat((w1, w2, d), 0)
            features.append(f)
        return torch.stack(features).view(len(pairs), 1, -1)

    def forward(self, pairs, sentence=None):
        """ support single sentence, consider sentence padding later
        forward based on pairs,
        if sentence is presented, go through sentence as well
        :param pairs: currently support pairs for single sentence
        :param sentence: support single sentence
        """
        # forward pairs features
        self.pairs_hidden = self.init_hidden()
        self.sent_hidden = self.init_hidden()

        pair_features = self.feature_for_pair(pairs)
        pair_lstm_out, self.pairs_hidden = self.pair_lstm(
            pair_features, self.pairs_hidden)

        if sentence:
            sentence = torch.tensor(sentence, dtype=torch.long,
                                    device=self.device)
            sentence_embeddings = self.embeddings(sentence)
            sent_lstm_out, self.sent_hidden = self.sent_lstm(
                sentence_embeddings.view(len(sentence), 1, -1),
                self.sent_hidden)

        catted = torch.stack((
            self.pairs_hidden[0][-1], self.pairs_hidden[1][-1],
            self.sent_hidden[0][-1], self.sent_hidden[1][-1]))
        final_out = self.outlayer(catted.view(1, -1))
        final_out = F.log_softmax(final_out, dim=1)
        return final_out
