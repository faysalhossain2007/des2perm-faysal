"""
encode title and description of recipes

binary classification based on 150 recipes labeled


"""

from sklearn.model_selection import train_test_split
import json

import numpy as np
from torch.utils.data import DataLoader
from MLP_model import MultiLabelMLP

import torch
from torch import optim
from utils import sent_encoder_setup, LabeledDataset, save_train_test_splits, \
    load_data, eval_performance


source_model_path = ""
base_writeout_dir = '../data/gcontacts_tgt_rs'
negative_pool_file = "../data/ifttt_for_neg_samples.json"


def transfer_learning(flag, pretrained_path, model, freeze_transfered=True):
    if flag:
        """ load pre-trained model from source domain"""
        pretrained_model = torch.load(pretrained_path)
        except_last1layer = {}
        for idx, key in enumerate(pretrained_model):
            if idx < len(pretrained_model) - 2:
                except_last1layer[key] = pretrained_model[key]
        model.load_state_dict(except_last1layer, False)

        if freeze_transfered:
            # set lower layers requires_grad = False
            for idx, param in enumerate(model.parameters()):
                if idx < len(except_last1layer):
                    param.requires_grad = False
    else:
        pass


def train(datafile, n_epochs=20, learning_rate=0.01, batch_size=10,
          random_state = 14, transfer=True, freeze_pretrain=False, test_size=0.4):
    """
    encoding sentences on the fly
    :param random_state: it will set the random state for train/test data split
    :param freeze_pretrain: only affects result when transfer==True
    """
    net_structure = [4096, 1000, 100, 2]

    encoder = sent_encoder_setup()
    dataset = load_data(datafile, negative_pool_file,
                        random_state, test_size, make_balance=True)

    # encode sentences
    X_train_embeddings = encoder.encode(dataset[0])
    X_test_embeddings = encoder.encode(dataset[1])

    train_labeled = LabeledDataset(X_train_embeddings, dataset[2], dataset[0])
    test_labeled = LabeledDataset(X_test_embeddings, dataset[3], dataset[1])

    # init networks
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    clf = MultiLabelMLP(net_structure).to(device)
    opt = optim.SGD(clf.parameters(), lr=learning_rate)
    loss_fn = torch.nn.BCELoss()

    # transfer lower layer from other model
    transfer_learning(transfer,
                      source_model_path,
                      clf,
                      freeze_pretrain)

    test_loader = DataLoader(test_labeled, shuffle=False)
    test_acc_history = []
    # test before train/fine-tune
    eval_performance(test_loader, device, clf, test_labeled, 0, test_acc_history)

    for _ in range(n_epochs):
        train_loader = DataLoader(train_labeled, batch_size, shuffle=True)

        for batch_idx, batch_data in enumerate(train_loader):
            X = batch_data[0].to(device)
            y = batch_data[1].to(device)

            outputs = clf(X)
            sig = torch.nn.Sigmoid()
            loss = loss_fn(sig(outputs), y)
            opt.zero_grad()
            loss.backward()
            opt.step()

            print('loss:', loss.item())

        # eval on test

        eval_performance(test_loader, device, clf, test_labeled, _, test_acc_history)


    save_train_test_splits(dataset, base_writeout_dir + str(random_state))
    save_history_acc(test_acc_history,
                     base_writeout_dir + str(random_state),
                     transfer,
                     freeze_pretrain,
                     learning_rate,
                     test_size)


def save_history_acc(hist_acc, writeoutdir, transfer_flag, freeze_flag, lr, test_size):
    """"""
    outputfilename = writeoutdir+"/acc_hist_lr{}_test{}_trans{}_freeze{}.csv".format(lr,
                                                                                     test_size,
                                                                                     transfer_flag,
                                                                                     freeze_flag)
    with open(outputfilename, 'a') as outputfile:
        for idx, item in enumerate(hist_acc):
            outputfile.write(str(round(item, 4)))
            if idx != len(hist_acc) - 1:
                outputfile.write(',')
            else:
                outputfile.write('\n')



if __name__ == '__main__':

    source_model_path = "../data/src_read_contacts_model.pickle"

    for _ in range(1):
        train("../data/ifttt_gcon_trigger.json.transformed.json",
              n_epochs=100, learning_rate=0.1, batch_size=3, random_state=45,
              transfer=True, freeze_pretrain=False, test_size=0.8)
