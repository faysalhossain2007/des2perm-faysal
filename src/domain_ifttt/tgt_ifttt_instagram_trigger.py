# """
# Transfer from single source domain to instagram trigger
# """
#
# from torch.utils.data import DataLoader
# from MLP_model import MultiLabelMLP
#
# import torch
# from torch import optim
# from utils import sent_encoder_setup, LabeledDataset, save_train_test_splits
# from utils import eval_performance, load_data
#
# clean_data_path = '../data/ifttt_instagram_trigger.json'
# # noise_data_path = "../data/ifttt_noise_fb_action.json"
# base_writeout_dir = '../data/exp/ig_tg_tgt_rs'
# negative_pool_file = "../data/ifttt_for_neg_samples.json"
#
# # src_name = "read_con"
# # source_model_path = "../data/src_read_contacts_model.pickle"
# # src_name = "write_con"
# # source_model_path = "../data/src_write_contacts_model.pickle"
# src_name = "camera"
# source_model_path = "../data/src_camera_model.pickle"
#
#
#
#
#
# def transfer_learning(flag, pretrained_path, model, freeze_transfered=True):
#     if flag:
#         """ load pre-trained model from source domain"""
#         pretrained_model = torch.load(pretrained_path)
#         except_last1layer = {}
#         for idx, key in enumerate(pretrained_model):
#             if idx < len(pretrained_model) - 2:
#                 except_last1layer[key] = pretrained_model[key]
#         model.load_state_dict(except_last1layer, False)
#
#         if freeze_transfered:
#             # set lower layers requires_grad = False
#             for idx, param in enumerate(model.parameters()):
#                 if idx < len(except_last1layer):
#                     param.requires_grad = False
#     else:
#         pass
#
#
# def replace_abbr(data):
#     for idx, s in enumerate(data):
#         s = s.replace("fb", 'facebook')
#         s = s.replace('gcal', 'google calendar')
#         s = s.replace(' insta ', ' instagram ')
#         s = s.replace('IG', 'instagram')
#         data[idx] = s
#
#
#
# def train(n_epochs=20, learning_rate=0.01, batch_size=10,
#           random_state = 14, transfer=True, freeze_pretrain=False,
#           test_size=0.4, unsupervised=False):
#     """
#     encoding sentences on the fly
#     :param random_state: it will set the random state for train/test data split
#     :param freeze_pretrain: only affects result when transfer==True
#     """
#     net_structure = [4096, 1000, 100, 2]
#
#     encoder = sent_encoder_setup()
#     X_train, X_test, y_train, y_test = load_data(clean_data_path, negative_pool_file,
#                                                 random_state, test_size,
#                                                  make_balance=True)
#     replace_abbr(X_train)
#     replace_abbr(X_test)
#
#     if unsupervised:
#         """ use noise data for training"""
#         # X_train, y_train = load_noise_data()
#
#     # encode sentences
#     X_train_embeddings = encoder.encode(X_train)
#     X_test_embeddings = encoder.encode(X_test)
#
#     train_labeled = LabeledDataset(X_train_embeddings, y_train, X_train)
#     test_labeled = LabeledDataset(X_test_embeddings, y_test, X_test)
#
#     # init networks
#     device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#     clf = MultiLabelMLP(net_structure).to(device)
#     opt = optim.SGD(clf.parameters(), lr=learning_rate)
#     loss_fn = torch.nn.BCELoss()
#
#     # transfer lower layer from other model
#     transfer_learning(transfer,
#                       source_model_path,
#                       clf,
#                       freeze_pretrain)
#
#     test_acc_history = []
#     test_loader = DataLoader(test_labeled, shuffle=False)
#
#     # test before train/fine-tune
#     eval_performance(test_loader, device, clf, test_labeled, 0, test_acc_history)
#
#     for _ in range(n_epochs):
#         train_loader = DataLoader(train_labeled, batch_size, shuffle=True)
#
#         for batch_idx, batch_data in enumerate(train_loader):
#             X = batch_data[0].to(device)
#             y = batch_data[1].to(device)
#
#             outputs = clf(X)
#             loss = loss_fn(outputs, y)
#             opt.zero_grad()
#             loss.backward()
#             opt.step()
#
#             print('loss:', loss.item())
#
#         eval_performance(test_loader, device, clf, test_labeled, _, test_acc_history)
#
#     save_train_test_splits((X_train, X_test, y_train, y_test),
#                            base_writeout_dir + str(random_state))
#     save_history_acc(test_acc_history,
#                      base_writeout_dir + str(random_state),
#                      transfer,
#                      freeze_pretrain,
#                      learning_rate,
#                      test_size)
#
#
# def save_history_acc(hist_acc, writeoutdir, transfer_flag, freeze_flag, lr, test_size):
#     """"""
#     outputfilename = writeoutdir+"/from_{}_lr{}_test{}_trans{}_freeze{}.csv".format(
#         src_name, lr,
#         test_size,
#         transfer_flag,
#         freeze_flag)
#
#     with open(outputfilename, 'a') as outputfile:
#         for idx, item in enumerate(hist_acc):
#             outputfile.write(str(round(item, 4)))
#             if idx != len(hist_acc) - 1:
#                 outputfile.write(',')
#             else:
#                 outputfile.write('\n')
#
#
# if __name__ == '__main__':
#
#     for _ in range(1):
#         train(n_epochs=100, learning_rate=0.1, random_state=45,
#               transfer=True, freeze_pretrain=False, test_size=0.9,
#               unsupervised=False)


"""
Transfer from single source domain to evernote trigger
"""
from path_fix import CURR_DIR
from train_func import tgt_doc_level_train as train
from utils import get_logger, BoW_encoder


def main():
    """"""
    src_model_t = '../../data/exp/read_con/' + \
        'ad300_neg{}_dropoutNone_BoW2/model.pickle'
    src_model = None
    src_model_alias_t = 'read_con_neg{}'
    exp_dir_t = '../../data/exp/instagram_tg/from_read_con/{}_to_neg{}_wInit{}'
    tgt_data = '../../data/train-val-data/ifttt-domain/reformated/ifttt_instagram_trigger.json'

    wv_model = '../../data/wv/android_w2v.kv'
    neg_samples = '../../data/train-val-data/ifttt-domain/' + \
        'ifttt_for_neg_samples.json'

    for src_neg in range(1, 2):
        src_model = src_model_t.format(src_neg)
        src_model_alias = src_model_alias_t.format(src_neg)

        # src_model = None
        # src_model_alias = None

        for negvspos in range(1, 10):
            w_copy = True
            exp_dir = exp_dir_t.format(src_model_alias, negvspos, w_copy)
            logger = get_logger(exp_dir)

            train(src_model,
                  src_model_alias,
                  tgt_data,
                  logger,
                  exp_dir,
                  neg_samples,
                  wv_model,
                  BoW_encoder,
                  data_neg_vs_pos=negvspos,
                  epochs=20,
                  weight_copy=w_copy)

            print("-----one exp done---- \n")


if __name__ == '__main__':
    main()