# """
# encode title and description of recipes
#
# binary classification based on 150 recipes labeled
#
#
# """
# from train_func import target_domain_training as train
#
#
#
# if __name__ == '__main__':
#     random_seed = 44
#     test_proportion = 0.5
#     lr = 0.1
#     sent_encoder = 'BoW'
#     net_struct = [300, 1000, 100, 2]
#
#     src_model_path = None
#     src_name = None
#
#     # src_name = "read_con"
#     # source_model_path = "../data/src_read_contacts_model.pickle"
#
#     # src_name = "write_con"
#     # source_model_path = "../data/src_write_contacts_model.pickle"
#
#     # src_name = "camera"
#     # source_model_path = "../data/src_camera_model.pickle"
#
#     # src_name = "send_sms"
#     # source_model_path = "../data/src_send_sms.pickle"
#
#     for _ in range(10):
#         train(data_file='../data/ifttt_gcal_trigger.json.transformed.json',
#               exp_basedir='../data/exp/ifttt_gcal_ifttt_wv_test0.5',
#               negative_samples='../data/ifttt_for_neg_samples.json',
#               src_model=src_model_path,
#               sent_encoder=sent_encoder,
#               embedding_model='../data/wv/ifttt_all_w2v.kv',
#               src_model_alias=src_name,
#               net_struct=net_struct,
#               train_epochs=100,
#               learning_rate=0.1,
#               batch_size=10,
#               train_test_splitting_seed=random_seed,
#               test_proportion=test_proportion,
#               balance_data=True,
#               freeze_transferred=True,
#               data_process_filters=None)
#
#         # train("../data/ifttt_gcal_trigger.json.transformed.json",
#         #       n_epochs=100, learning_rate=0.1, random_state=45,
#         #       transfer=True, freeze_pretrain=False, test_size=0.9,
#         #       unsupervised=False)


from path_fix import CURR_DIR
from train_func import tgt_doc_level_train as train
from utils import get_logger, BoW_encoder


def main():
    """"""
    src_model_t = '../../data/exp/read_cal/' + \
        'ad300_neg{}_dropoutNone_BoW2/model.pickle'
    src_model = None
    src_model_alias_t = 'read_cal_neg{}'
    exp_dir_t = '../../data/exp/gcal_tg/from_read_cal/{}_to_neg{}_wInit{}'
    tgt_data = '../../data/train-val-data/ifttt-domain/reformated/ifttt_gcal_trigger.json.transformed.json'

    wv_model = '../../data/wv/android_w2v.kv'
    neg_samples = '../../data/train-val-data/ifttt-domain/' + \
        'ifttt_for_neg_samples.json'

    for src_neg in range(1, 2):
        src_model = src_model_t.format(src_neg)
        src_model_alias = src_model_alias_t.format(src_neg)

        # src_model = None
        # src_model_alias = None

        for negvspos in range(1, 10):
            w_copy = True
            exp_dir = exp_dir_t.format(src_model_alias, negvspos, w_copy)
            logger = get_logger(exp_dir)

            train(src_model,
                  src_model_alias,
                  tgt_data,
                  logger,
                  exp_dir,
                  neg_samples,
                  wv_model,
                  BoW_encoder,
                  data_neg_vs_pos=negvspos,
                  epochs=20,
                  weight_copy=w_copy)

            print("-----one exp done---- \n")


if __name__ == '__main__':
    main()