"""
Transfer from multiple source domain NNs models to target domain
Aggregate source domain NNs into a bigger NNs, fine-tune on target domain

"""

import torch
import torch.nn as nn
from torch import optim
from torch.utils.data import DataLoader
from utils import load_data, sent_encoder_setup, LabeledDataset
from utils import eval_performance, save_history_acc


class SubNet(nn.Module):
    def __init__(self, net_structure):
        """ use ReLU default activation"""
        super(SubNet, self).__init__()
        layers = []
        for i in range(len(net_structure) - 1):
            input_size = net_structure[i]
            output_size = net_structure[i + 1]
            layers.append(nn.Linear(input_size, output_size))
            layers.append(nn.ReLU())

        self.model = nn.Sequential(*layers)


    def forward(self, X):
        return self.model(X)

    def weight_init(self, weight_path):
        pretrained_model = torch.load(weight_path)
        except_last1layer = {}
        for idx, key in enumerate(pretrained_model):
            if idx < len(pretrained_model) - 2:
                except_last1layer[key.replace('model.', '')] = pretrained_model[key]
        self.model.load_state_dict(except_last1layer, False)


    def freeze_nets(self):
        """ freeze sub net model parameters"""

        for param in self.model.parameters():
            param.requires_grad = False



class AggNet(nn.Module):
    def __init__(self, device, subnet_structures, subnet_inits, output_size):
        super(AggNet, self).__init__()

        assert len(subnet_inits) == len(subnet_structures)

        subnets = []
        sub_output_sizes = []
        for idx, structure in enumerate(subnet_structures):
            net = SubNet(structure).to(device)
            net.weight_init(subnet_inits[idx])
            subnets.append(net)
            sub_output_sizes.append(structure[-1])

        last_layer = nn.Linear(sum(sub_output_sizes), output_size)

        self.subnets = subnets
        self.lastlayer = last_layer


    def freeze_subnets(self):
        for net in self.subnets:
            net.freeze_nets()


    def forward(self, X):
        """"""
        sub_outputs = []
        for net in self.subnets:
            out = net(X)
            sub_outputs.append(out)

        # aggregate
        agg_output = torch.cat(sub_outputs, dim=1)

        out = self.lastlayer(agg_output)
        out = torch.sigmoid(out)
        return out


def train(n_epochs=20, learning_rate=0.01, batch_size=10,
          random_state = 14, freeze_pretrain=False,
          test_size=0.4, unsupervised=False):
    """"""
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    target_clean_dataset = "../data/ifttt_evernote_trigger.json"
    # target_noise_dataset = "../data/ifttt_noise_fb_action.json"
    base_writeout_dir = '../data/exp/multi_src_ifttt_evernote_tg_'
    negative_pool_file = "../data/ifttt_for_neg_samples.json"

    source_models = ['../data/src_read_contacts_model.pickle',
                     '../data/src_camera_model.pickle',
                     '../data/src_write_contacts_model.pickle']

    source_net_structures = [[4096, 1000, 100],
                             [4096, 1000, 100],
                             [4096, 1000, 100]]

    sentence_encoder = sent_encoder_setup()

    X_train, X_test, y_train, y_test = load_data(target_clean_dataset,
                                                 negative_pool_file,
                                                random_state, test_size,
                                                 make_balance=True)

    # encode sentences
    X_train_embeddings = sentence_encoder.encode(X_train)
    X_test_embeddings = sentence_encoder.encode(X_test)

    train_labeled = LabeledDataset(X_train_embeddings, y_train, X_train)
    test_labeled = LabeledDataset(X_test_embeddings, y_test, X_test)

    # init network
    aggnet = AggNet(device, source_net_structures, source_models,
                    output_size=2).to(device)
    if freeze_pretrain: aggnet.freeze_subnets()

    opt = optim.SGD(aggnet.parameters(), lr=learning_rate)
    loss_fn = torch.nn.BCELoss()

    test_acc_history = []
    test_loader = DataLoader(test_labeled, shuffle=False)

    # test before train/fine-tune
    eval_performance(test_loader, device, aggnet, test_labeled, 0, test_acc_history)

    for e in range(1, 1+n_epochs):
        train_loader = DataLoader(train_labeled, batch_size, shuffle=True)
        for batch_idx, batch_data in enumerate(train_loader):
            X = batch_data[0].to(device)
            y = batch_data[1].to(device)

            outs = aggnet(X)
            loss = loss_fn(outs, y)
            opt.zero_grad()
            loss.backward()
            opt.step()

            print('loss:', loss.item())

        eval_performance(test_loader, device, aggnet, test_labeled, e, test_acc_history)

    save_history_acc(test_acc_history,
                     base_writeout_dir + str(random_state),
                     True,
                     freeze_pretrain,
                     learning_rate,
                     test_size)



if __name__ == '__main__':
    for _ in range(10):
        train(n_epochs=100, learning_rate=0.1, random_state=45,
                freeze_pretrain=False, test_size=0.9,
                unsupervised=False)
