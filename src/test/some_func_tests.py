import sys
sys.path.append("../")
from DAN.target_exp import generate_hidden_layers_conf
from DAN.train import FakeSummaryWriter


def hidden_layer_conf():
    base_conf = {
        "hidden_layers": [[100, 500, 100],
                           [100,300,100]]
        }
    pp = generate_hidden_layers_conf(base_conf)
    print(pp)


def fake_summary_():
    summary = FakeSummaryWriter()
    summary.add_scalar(1,2,2)


if __name__ == '__main__':
    hidden_layer_conf()

    fake_summary_()
