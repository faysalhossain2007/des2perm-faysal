"""
source domain, android system access fine location permission

"""
# has to be the first import
from path_fix import CURR_DIR
# ===========================

import time
from os.path import join

import utils
from train_func import source_domain_train as train


if __name__ == '__main__':
    # neg_vs_pos = 1
    # dropout = 0.3
    # test different dropout
    sent_encoder = 'BoW'
    random_seed = 2

    # data_filepath = join(
    #     CURR_DIR,
    #     "../../data/android_access_fine_loc_actlearn_lc_3rd.json")
    # data_filepath = join(
    #     CURR_DIR,
    #     "../../data/android_access_fine_loc_added_pos_trunks.json")
    # data_filepath = join(
    #     CURR_DIR,
    #     "../../data/train-val-data/android-domain/access-fine-location/android.permission.ACCESS_FINE_LOCATION.reformated_train_val.json")

    data_filepath = join(
        CURR_DIR,
        "../../data/train-val-data/android-domain/access-fine-location/added-pos-trunks/android_access_fine_loc_added_pos_trunks.json")



    # data_filepath = join(
    #     CURR_DIR,
    #     "../../data/android_fineloc_plus_coarseloc.json")

    wv_model_path = join(
        CURR_DIR,
        "../../data/wv/android_w2v.kv")

    neg_sample_datapath = join(
        CURR_DIR,
        '../../data/train-val-data/android-domain/android_for_negative_samples.json')

    dropout = None
    for neg_vs_pos in range(1, 10):

        exp_folder = 'fineloc/ad300_neg{}_dropout{}_{}{}'.format(
            str(neg_vs_pos), str(dropout), sent_encoder, random_seed)
        exp_dir = join(CURR_DIR, '../../data/exp/', exp_folder)
        logger = utils.get_logger(exp_dir)

        start_time = time.time()
        for _ in range(1):
            train(logger,
                  data_filepath,
                  neg_samples_file=neg_sample_datapath,
                  n_epochs=100,
                  batch_size=20, learning_rate=0.1,
                  random_state=random_seed,
                  dropout=dropout,
                  exp_dir=exp_dir,
                  output_model='model.pickle',
                  sent_encoder=sent_encoder,
                  embedding_model=wv_model_path,
                  hidden_layers=[1000, 100],
                  dataset_neg_vs_pos=neg_vs_pos)

        end_time = time.time()
        print('cost time: {} s'.format(end_time - start_time))
