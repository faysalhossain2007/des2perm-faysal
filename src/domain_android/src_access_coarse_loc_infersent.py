"""
source domain, android system access fine location permission

"""
from train_func import source_domain_train as train


if __name__ == '__main__':
    train("../data/android_access_coarse_loc_cleaned.json", n_epochs=100,
          batch_size=15, learning_rate=0.5, random_state=25,
          exp_basedir="../data/exp/access_coarse_loc",
          model_name='src_access_coarse_loc.pickle',
          sent_encoder='InferSent',
          hidden_layers=[1000, 100])
