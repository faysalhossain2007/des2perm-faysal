"""
source domain, android system camera permission

"""
#
#
# from sklearn.model_selection import train_test_split
# import json
# import numpy as np
# from torch.utils.data import DataLoader
# from MLP_model import MultiLabelMLP
#
#
# import torch
# from torch import optim
# from utils import sent_encoder_setup, LabeledDataset, \
#     save_train_test_splits, load_data, eval_performance
#
#
# def train(datafile, n_epochs, batch_size, test_size=0.2, learning_rate=0.1, random_state=None):
#     """ training process"""
#     # hyper parameters
#     net_structure = [4096, 1000, 100, 2]
#
#     encoder = sent_encoder_setup()
#     dataset = load_data(datafile, random_state=random_state,
#                         test_proportion=test_size,
#                         make_balance=False)
#     save_train_test_splits(dataset, '../data/write_contacts' + str(random_state))
#
#     # encode sentences
#     X_train_embeddings = encoder.encode(dataset[0])
#     X_test_embeddings = encoder.encode(dataset[1])
#
#     train_labeled = LabeledDataset(X_train_embeddings, dataset[2], dataset[0])
#     test_labeled = LabeledDataset(X_test_embeddings, dataset[3], dataset[1])
#
#     # init network
#     device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#     clf = MultiLabelMLP(net_structure).to(device)
#     opt = optim.SGD(clf.parameters(), lr=learning_rate)
#     loss_fn = torch.nn.BCELoss()
#
#     # training process
#     for _ in range(n_epochs):
#         train_loader = DataLoader(train_labeled, batch_size, shuffle=True)
#
#         for batch_idx, batch_data in enumerate(train_loader):
#             X = batch_data[0].to(device)
#             y = batch_data[1].to(device)
#
#             outputs = clf(X)
#             sig = torch.nn.Sigmoid()
#             loss = loss_fn(sig(outputs), y)
#             opt.zero_grad()
#             loss.backward()
#             opt.step()
#
#             print('loss:', loss.item())
#
#         # eval on test
#         test_loader = DataLoader(test_labeled, shuffle=False)
#         eval_performance(test_loader, device, clf, test_labeled, _, [])
#     # end training
#
#     # saving model parameters
#     torch.save(clf.state_dict(),
#                "../data/src_write_contacts_model.pickle")
#
#
# if __name__ == '__main__':
#     train('../data/android_modify_contacts_546.json',
#           n_epochs=50, batch_size=5, learning_rate=0.1, random_state=16, test_size=0.2)


from path_fix import CURR_DIR
# ===========================

import time
from os.path import join

import utils
from train_func import source_domain_train as train


def main():
    # neg_vs_pos = 1
    # dropout = 0.3
    # test different dropout
    sent_encoder = 'BoW'
    random_seed = 2

    # data_filepath = join(
    #     CURR_DIR,
    #     "../../data/android_access_fine_loc_actlearn_lc_3rd.json")
    # data_filepath = join(
    #     CURR_DIR,
    #     "../../data/android_access_fine_loc_added_pos_trunks.json")
    data_filepath = join(
        CURR_DIR,
        "../../data/train-val-data/android-domain/android_modify_contacts_546.json")

    # data_filepath = join(
    #     CURR_DIR,
    #     "../../data/android_fineloc_plus_coarseloc.json")

    wv_model_path = join(
        CURR_DIR,
        "../../data/wv/android_w2v.kv")

    neg_sample_datapath = join(
        CURR_DIR,
        '../../data/train-val-data/android-domain/android_for_negative_samples.json')

    dropout = None
    for neg_vs_pos in range(1, 10):

        exp_folder = 'writecontacts/ad300_neg{}_dropout{}_{}{}'.format(
            str(neg_vs_pos), str(dropout), sent_encoder, random_seed)
        exp_dir = join(CURR_DIR, '../../data/exp/', exp_folder)
        logger = utils.get_logger(exp_dir)

        start_time = time.time()
        for _ in range(1):
            train(logger,
                  data_filepath,
                  neg_samples_file=neg_sample_datapath,
                  n_epochs=100,
                  batch_size=20, learning_rate=0.1,
                  random_state=random_seed,
                  dropout=dropout,
                  exp_dir=exp_dir,
                  output_model='model.pickle',
                  sent_encoder=sent_encoder,
                  embedding_model=wv_model_path,
                  hidden_layers=[1000, 100],
                  dataset_neg_vs_pos=neg_vs_pos)

        end_time = time.time()
        print('cost time: {} s'.format(end_time - start_time))



if __name__ == '__main__':
    main()