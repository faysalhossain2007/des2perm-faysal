"""
source domain, android system send sms permission

"""
from train_func import source_domain_train as train


if __name__ == '__main__':
    for _ in range(10):
        train("../data/android_send_sms.json", n_epochs=100,
              batch_size=20, learning_rate=0.1, random_state=25,
              exp_basedir="../data/exp/send_sms",
              output_model='src_send_sms.pickle',
              sent_encoder='InferSent',
              hidden_layers=[1000, 100])
