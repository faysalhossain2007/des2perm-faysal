"""
source domain, android system send sms permission

"""
from train_func import source_domain_train as train


if __name__ == '__main__':

    for _ in range(1):
        train("../data/android_zz_write_apn_settings.json", n_epochs=100,
              batch_size=10, learning_rate=0.01, random_state=25,
              exp_basedir="../data/exp/write_apn",
              output_model='src_write_apn.pickle',
              sent_encoder='InferSent',
              hidden_layers=[1000, 100])
