"""
source domain, android system camera permission

"""


from train_func import source_domain_train as train

if __name__ == '__main__':
    train("../data/android_read_sms.json", n_epochs=200,
          batch_size=10, learning_rate=0.3, random_state=25,
          exp_basedir="../data/exp/read_sms",
          output_model='src_read_sms.pickle',
          sent_encoder='InferSent',
          hidden_layers=[1000, 100])
