import torch
import numpy as np
import random
import torch.nn as nn
import torch.functional as F
import argparse
import json
import requests
from sklearn.model_selection import train_test_split
from gensim.models import KeyedVectors
from tqdm import tqdm
from lstm import PairsLSTM

UNK_IDX = 0
END_IDX = 1


def data_split(train, val, test, data, random_state):
    """
    """
    if train + val + test != 1:
        return None
    else:
        train_set, test_set = train_test_split(
            data, test_size=(test+val), random_state=random_state)
        test_set, val_set = train_test_split(
            test_set,
            test_size=val/(test+val),
            random_state=random_state)

        return train_set, val_set, test_set


def depparse(args, sentence):
    """"""
    url = args.depparser_url
    deparse_url = url + "/?properties=" +\
        "{'annotators':'tokenize,ssplit,pos,depparse','outputFormat':'json'}"
    response = requests.post(deparse_url, data=sentence)
    response = response.json()
    return response


def extract_compound(dependencies, target_governor):
    compound_words = []
    for d in dependencies:
        if target_governor == d['governor'] and d['dep'] == 'compound':
            compound_words.append(d['dependentGloss'])
    return compound_words


def extract_word_pairs(args, sent):
    parsed = depparse(args, sent)
    tokens = parsed['sentences'][0]['tokens']
    deps = parsed['sentences'][0]['enhancedPlusPlusDependencies']
    deps_by_governor = {}
    deps_by_dependent = {}
    for d in deps:
        g_id = d['governor']
        d_id = d['dependent']
        if g_id in deps_by_governor:
            deps_by_governor[g_id].append(d)
        else:
            deps_by_governor[g_id] = [d]
        if d_id in deps_by_dependent:
            deps_by_dependent[d_id].append(d)
        else:
            deps_by_dependent[d_id] = [d]

    pairs = []
    for d in deps:
        g_id = d['governor']
        d_id = d['dependent']
        if d['dep'] == "amod" or d['dep'] == "nmod:poss" or \
           d['dep'] == "advmod":
            governor = d['governorGloss']
            dependent = d['dependentGloss']

            pairs.append((dependent.lower(), governor.lower()))

            # also consider the compound word with the dependent
            compound_words = extract_compound(deps, g_id)
            for w in compound_words:
                pairs.append((dependent.lower(), w))

        if d['dep'] == 'compound':
            pairs.append((d['dependentGloss'].lower(),
                          d['governorGloss'].lower()))
        if d['dep'] == 'dobj':
            compound_words = extract_compound(deps, d_id)
            pairs.append((d['governorGloss'].lower(),
                          d['dependentGloss'].lower()))

            for w in compound_words:
                pairs.append((d['governorGloss'].lower(), w))

        if d['dep'] == 'nsubj':
            g_pos = tokens[g_id-1]['pos']
            if g_pos == "VBP":
                # get the child of this governer with advmod dependency
                for s_d in deps_by_governor[g_id]:
                    if s_d['dep'] == "advmod":
                        pairs.append(
                            (d['dependentGloss'].lower(),
                             s_d['dependentGloss'].lower())
                            )

        if d['dep'].startswith('nmod'):
            g_pos = tokens[g_id - 1]['pos']
            if g_pos.startswith("VB"):
                pairs.append(
                    (d['governorGloss'].lower(),
                     d['dependentGloss'].lower())
                    )
    processed_tokens = []
    for t in tokens:
        processed_tokens.append(t['word'].lower())

    return list(set(pairs)), processed_tokens


def flatten_data(data):
    if 'sentence' not in data[0]:
        flatten = []
        for doc in data:
            for s in doc['sentences']:
                flatten.append((
                    s['s'], int(s['manual-eval'])))
    else:
        flatten = []
        for item in data:
            flatten.append(
                (item['sentence'], int(item['label'])))
    return flatten


def balance_data(data, seed):
    positives = []
    negatives = []
    random.seed(seed)
    for item in data:
        if item[1] == 1:
            positives.append(item)
        else:
            negatives.append(item)

    if len(positives) < len(negatives):
        repeated_positives = random.choices(
            positives, k=len(negatives)-len(positives))
        positives += repeated_positives
    else:
        # sample negatives
        repeated_negatives = random.choices(
            negatives, k=len(positives)-len(negatives))
        negatives += repeated_negatives

    balanced_data = positives + negatives
    random.shuffle(balanced_data)

    return balanced_data


def load_kvfile(args):
    """ transform kv file to np array used for
    word embedding layer initialization

    :return: vocabulary, word_vectors
    """
    UNK = '<unk>'
    END = '<end>'
    kv_path = args.kvfile
    kv_model = KeyedVectors.load(kv_path)
    model_vocab = kv_model.index2word
    # remove UNK, and END, because we want to put them at the beginning
    if UNK in model_vocab:
        _idx = model_vocab.index(UNK)
        del model_vocab[_idx]
    if END in model_vocab:
        _idx = model_vocab.index(END)
        del model_vocab[_idx]

    vector_size = kv_model.vector_size
    word_vectors = []

    vocab = [UNK, END] + model_vocab

    # place for UNK
    if UNK in kv_model:
        word_vectors.append(kv_model[UNK])
    else:
        rnd_vec = np.random.uniform(-0.5, 0.5, vector_size)
        word_vectors.append(rnd_vec)

    # place for END
    if END in kv_model:
        word_vectors.append(kv_model[END])
    else:
        # rnd_vec = np.random.uniform(-0.5, 0.5, vector_size)
        zero_vec = np.zeros(vector_size, dtype=np.float)
        word_vectors.append(zero_vec)

    # process the rest words in vocab
    for w in vocab[2:]:
        w_vec = kv_model[w]
        word_vectors.append(w_vec)
    word_vectors = np.array(word_vectors)

    return vocab, word_vectors


def indexing_data(data, vocab):
    """"""
    print('indexing data...')

    indexed_data = []
    for tokens, pairs, label in tqdm(data):
        idx_tokens = []
        for t in tokens:
            t_idx = vocab.index(t) if t in vocab else UNK_IDX
            idx_tokens.append(t_idx)
        idx_pairs = []
        for p in pairs:
            w1 = vocab.index(p[0]) if p[0] in vocab else UNK_IDX
            w2 = vocab.index(p[1]) if p[1] in vocab else UNK_IDX
            idx_pairs.append(
                (w1, w2))
        indexed_data.append((
            idx_tokens,
            idx_pairs,
            label
            ))
    return indexed_data


def data_process(args, vocab, data, balance_it=False):
    """ tokenize data, indexing, get pairs by corenlp & extractor

    currently assume data format at sentence level
    """
    # include a flatten step for unfold document level data
    data = flatten_data(data)
    if balance_it:
        data = balance_data(data, args.random_seed)
    # tokenize and extract pairs
    tokenized_data = []
    for X, y in data:
        _pairs, _tokens = extract_word_pairs(args, X)
        tokenized_data.append(
            (_tokens, _pairs, y))

    # indexing the data
    indexed_data = indexing_data(tokenized_data, vocab)

    return indexed_data


def load_data(args):
    """
    load the data from data file
    parse sentence to get pairs
    split into training, validation, testing (possibly balancing train)
    transform text to index of vocabulary

    :return: train, val, test
    """
    # assume json format
    datafile = args.datafile
    with open(datafile) as ifile:
        data = json.load(ifile)
    # load kv file
    vocab, wv = load_kvfile(args)

    train, val, test = data_split(args.train, args.val,
                                  args.test, data,
                                  args.random_seed)
    train = data_process(args, vocab, train, args.balance_train)
    val = data_process(args, vocab, val)
    test = data_process(args, vocab, test)

    return train, val, test, vocab, wv


def performance_check(dataset, model):
    """
    """
    tp, tn, fp, fn = 0, 0, 0, 0
    with torch.no_grad():
        for tokens, pairs, y in dataset:
            pred = model(pairs, tokens)
            _, pred = pred.max(1)
            pred = pred.item()
            if pred == y and y == 1:
                tp += 1
            elif pred == y and y == 0:
                tn += 1
            elif pred != y and pred == 1:
                fp += 1
            else:
                fn += 1
    return tp, tn, fp, fn


def metrics(tp, tn, fp, fn):
    """ compute acc, prec, recall, f1"""
    acc = (tp + tn) / (tp + tn + fp + fn)
    prec = tp / (tp + fp + 0.000001)
    recall = tp / (tp + fn + 0.000001)
    f1 = 2 * (prec * recall) / (prec + recall + 0.00001)
    return {'acc': acc, 'prec': prec, 'recall': recall, 'f1': f1}


def train(args):
    """
    iterate training samples
    possibly do data augmentation
    """
    training, val, test, vocab, wv = load_data(args)
    model = PairsLSTM(args, wv)
    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters())
    device = torch.device(args.device)
    model.to(device)

    for e in range(args.epoch):
        total_loss = 0
        for s_tokens, pairs, label in tqdm(training):
            label = torch.tensor(label).view(1).to(device)
            model.zero_grad()

            # if the sentence is empty go to next
            if len(s_tokens) < 1:
                continue

            # TODO data augmentation
            if args.sent_presented:
                pred_score = model(pairs, s_tokens)
            else:
                pred_score = model(pairs)
            loss = loss_fn(pred_score, label)
            loss.backward()
            optimizer.step()
            total_loss += loss.item()

        # after one epoch check the performance
        print('epoch', e, 'avg loss', total_loss / len(training))
        val_performance = performance_check(val, model)
        test_performance = performance_check(test, model)
        print('epoch {} at validation set'.format(e),
              metrics(*val_performance), val_performance)
        print('epoch {} at test set'.format(e),
              metrics(*test_performance), test_performance)



def main():
    """
    get training arguments
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--datafile', required=True)
    arg_parser.add_argument('--kvfile', required=True)
    arg_parser.add_argument('--train', default=0.7, type=float)
    arg_parser.add_argument('--val', default=0.1, type=float)
    arg_parser.add_argument('--test', default=0.2, type=float)
    arg_parser.add_argument('--balance-train', default=True, type=bool)
    arg_parser.add_argument('--depparser-url',
                            default='http://localhost:9000',
                            type=str)
    arg_parser.add_argument('--data-augment',
                            help="augment positive samples",
                            default=False,
                            type=bool)
    arg_parser.add_argument('--epoch', default=20, type=int)
    arg_parser.add_argument('--dropout', default=0.3, type=float)
    arg_parser.add_argument('--learning-rate', default=0.1, type=float)
    arg_parser.add_argument('--device',
                            help='running on cpu or cuda',
                            default='cpu',
                            type=str)
    arg_parser.add_argument('--hidden-size', default=30, type=int)
    arg_parser.add_argument('--layers', default=1, type=int)
    arg_parser.add_argument('--bi-direct', default=False, type=bool)
    arg_parser.add_argument('--random-seed', default=1, type=int)
    arg_parser.add_argument('--freeze-wv', default=False, type=bool)
    arg_parser.add_argument('--out-dim', default=2, type=int)
    arg_parser.add_argument('--sent-presented', default=True,
                            type=bool)

    args = arg_parser.parse_args()

    train(args)


if __name__ == "__main__":
    main()
