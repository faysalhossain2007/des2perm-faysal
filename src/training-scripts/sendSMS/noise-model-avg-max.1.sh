#! /bin/zsh

cd ../../

python train_DAN.py --nn-dropout=0.2 \
       --random-state 1 --balance-train True \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --expdir ~/Dev/desc2perm/data/exp/weak-supervision/sendSMS/avg-max-pool/ \
       --data-type=doc --gpu 1 \
       --save-model-to sendSMS.300.300.model \
       --train-data ~/Dev/desc2perm/data/noise-data/sendSMS/train.split.json.p \
       --val-data ~/Dev/desc2perm/data/noise-data/sendSMS/val.split.json.p \
       --test-data ~/Dev/desc2perm/data/noise-data/sendSMS/test.split.json.p \
       --processed True \
       --poolings avg,max
