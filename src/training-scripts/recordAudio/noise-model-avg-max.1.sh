#! /bin/zsh

cd ../../

python train_DAN.py --nn-dropout=0.2 \
       --random-state 1 --balance-train True \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --expdir ~/Dev/desc2perm/data/exp/weak-supervision/recordAudio/avg-max-pool/ \
       --data-type=doc --gpu 0 \
       --save-model-to recordAudio.300.300.model \
       --train-data ~/Dev/desc2perm/data/noise-data/recordAudio/train.split.json.p \
       --val-data ~/Dev/desc2perm/data/noise-data/recordAudio/val.split.json.p \
       --test-data ~/Dev/desc2perm/data/noise-data/recordAudio/test.split.json.p \
       --processed True \
       --poolings avg,max
