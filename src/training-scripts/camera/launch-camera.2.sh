#! /bin/zsh

cd ../../

python train_with_variant_size.py --nn-dropout=0.2 \
       --random-state 1 --balance-train True \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --root-exp-dir ~/Dev/desc2perm/data/exp/src-diff-train-size/camera-avg-max/no-pretrain \
       --data-type=sent --gpu 0 \
       --save-model-to finetune.camera.300.300.model \
       --data-dir ~/Dev/desc2perm/data/train-val-data/android-domain/camera \
       --poolings avg,max
