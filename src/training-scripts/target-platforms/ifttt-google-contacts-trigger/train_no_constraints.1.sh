#! /bin/zsh

cd ../../../

# set the weights penalty to be zero
python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/google-contacts-trigger/train.json\
       --val-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/google-contacts-trigger/val.json\
       --test-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/google-contacts-trigger/test.json\
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-ifttt-google-contacts-trigger/source_models_conf.1.json \
       --penalty-factor 0 \
       --expdir ~/Dev/desc2perm/data/exp/ifttt/google-contacts-trigger/noSourcesConstraints/conf.1 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --poolings avg,max \
       --epoch 40 \
       --gpu 0
