#! /bin/zsh

cd ../../../

# set penalty 0
# change exp dir
# using previous data splitting
python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/exp/evernote_tg/NoTrans/from_None_to_neg1_wInitFalse/train.json\
       --val-data  ~/Dev/desc2perm/data/exp/evernote_tg/NoTrans/from_None_to_neg1_wInitFalse/val.json\
       --test-data  ~/Dev/desc2perm/data/exp/evernote_tg/NoTrans/from_None_to_neg1_wInitFalse/test.json \
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-ifttt-evernote-trigger/source_models_conf.1.json \
       --penalty-factor 0.0 \
       --expdir ~/Dev/desc2perm/data/exp/ifttt/evernote-trigger/noSourcesConstraints/conf.2 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --poolings avg,max \
       --epoch 40 \
       --gpu 0 \
       --hidden-layers 1000,100
