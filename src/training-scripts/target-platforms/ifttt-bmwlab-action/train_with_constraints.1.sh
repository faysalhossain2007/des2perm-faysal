#! /bin/zsh

cd ../../../

python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/bmwlab-action/train.json\
       --val-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/bmwlab-action/val.json\
       --test-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/bmwlab-action/test.json\
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-ifttt-bmwlab-action/source_models_conf.1.json \
       --penalty-factor 0.5 \
       --expdir ~/Dev/desc2perm/data/exp/ifttt/bmwlab-action/withSourcesConstraints/conf.1 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --poolings avg,max \
       --epoch 40 \
       --gpu 0
