#! /bin/zsh

cd ../../../

# set penalty 0
# change exp dir
python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/bmwlab-action/selected/train.json\
       --val-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/bmwlab-action/selected/val.json\
       --test-data  ~/Dev/desc2perm/data/train-val-data/ifttt-domain/reformated/bmwlab-action/selected/test.json\
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-ifttt-bmwlab-action/source_models_conf.1.json \
       --penalty-factor 0.0 \
       --expdir ~/Dev/desc2perm/data/exp/ifttt/bmwlab-action/noSourcesConstraints/conf.2 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --poolings avg,max \
       --epoch 40 \
       --gpu 0
