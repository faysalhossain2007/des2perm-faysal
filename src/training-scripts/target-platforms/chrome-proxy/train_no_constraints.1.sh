#! /bin/zsh

cd ../../../

# set penalty 0
# change exp dir
python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/proxy/train.json\
       --val-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/proxy/val.json\
       --test-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/proxy/test.json\
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-chrome-proxy/source_models_conf.1.json\
       --penalty-factor 0.0 \
       --expdir ~/Dev/desc2perm/data/exp/chrome/proxy/noSourcesConstraints/conf.1 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --poolings avg,max \
       --epoch 40 \
       --gpu 0
