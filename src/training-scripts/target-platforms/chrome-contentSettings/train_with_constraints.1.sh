#! /bin/zsh

cd ../../../

python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/contentSettings/train.json\
       --val-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/contentSettings/val.json\
       --test-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/contentSettings/test.json\
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-chrome-contentSettings/source_models_conf.1.json \
       --penalty-factor 0.5 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --expdir ~/Dev/desc2perm/data/exp/chrome/contentSettings/withSourcesConstraints/conf.1 \
       --poolings avg,max \
       --epoch 40 \
       --gpu 0


