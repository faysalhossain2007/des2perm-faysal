#! /bin/zsh

cd ../../../

python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/geolocation/selected_by_srcs/train.json\
       --val-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/geolocation/selected_by_srcs/val.json\
       --test-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/geolocation/selected_by_srcs/test.json\
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-chrome-geolocation/source_models_conf.1.json\
       --penalty-factor 0.5 \
       --expdir ~/Dev/desc2perm/data/exp/chrome/geolocation/withSourcesConstraints/conf.4 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --poolings avg,max \
       --epoch 40 \
       --gpu 0
