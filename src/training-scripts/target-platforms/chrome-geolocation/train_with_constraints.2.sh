#! /bin/zsh

cd ../../../

python train_target_with_sources.py \
       --train-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/geolocation/train.json\
       --val-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/geolocation/val.json\
       --test-data  ~/Dev/desc2perm/data/train-val-data/crx-domain/reformated/geolocation/test.json\
       --data-type doc \
       --source-models ~/Dev/desc2perm/data/source_models/for-chrome-geolocation/source_models_conf.2.json\
       --penalty-factor 0.8 \
       --expdir ~/Dev/desc2perm/data/exp/chrome/geolocation/withSourcesConstraints/conf.2 \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --poolings avg,max \
       --epoch 100 \
       --gpu 0 \
       --grad-norm-limit 4.0
