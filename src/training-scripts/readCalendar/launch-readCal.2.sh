#! /bin/zsh

cd ../../

python train_with_variant_size.py --nn-dropout=0.2 \
       --random-state 1 --balance-train True \
       --wv-path ~/Dev/desc2perm/data/wv/android_w2v.kv \
       --root-exp-dir ~/Dev/desc2perm/data/exp/src-diff-train-size/readCalendar-avg-max/no-pretrain \
       --data-type=sent --gpu 0 \
       --save-model-to readCalendar.300.300.model \
       --data-dir ~/Dev/desc2perm/data/train-val-data/android-domain/readCalendar \
       --poolings avg,max

