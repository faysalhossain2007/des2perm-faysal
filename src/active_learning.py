"""
active Learning
"""
import json
from os import makedirs
from os.path import exists, join

import torch
from torch.nn import functional as F

from MLP_model import MultiLabelMLP
from utils import BoW_encoder


class ActiveLearning(object):

    def select_unlabeled(self,
                         current_model_path,
                         labeled_data_path,
                         unlabeled_path,
                         wv_model,
                         selecting_method,
                         batch_size,
                         writeout_dir,
                         cudaN=0):
        """
        Args:
            current_model_path (string): directory to a trained model.
            labeled_data_path (string): those labeled data should not
                be included.
            unlabeled_path (string): get all unlabeled data, take out
                labeled ones.
            wv_model (string): specify the model for word embedding lookup
            selecting_method (string): selecting unlabeled data based
                on 'least confident', 'samllest margin' or 'entropy'.
            writeout_dir (string): write out the selected examples to
                the directory.
        """
        cudaN = str(cudaN)
        self.device = torch.device('cuda:'+cudaN if torch.cuda.is_available()
                                   else 'cpu')

        trained_model = self.init_model(current_model_path, self.device)
        data_pool = self.load_data(labeled_data_path, unlabeled_path)
        predictions = self.make_prediction(trained_model, data_pool,
                                           wv_model)
        selected_indices = self.make_decision(predictions, batch_size,
                                              method=selecting_method)

        selected_samples = [data_pool[i] for i in selected_indices]
        self.writeout_selected(selected_samples, writeout_dir)

    def init_model(self, model_path, device):
        """"""
        # using fixed network structure
        # TODO get structure from pre-trained weights
        net_structure = [300, 1000, 100, 2]
        if torch.cuda.is_available():
            weights = torch.load(model_path)
        else:
            weights = torch.load(model_path, map_location='cpu')
        model = MultiLabelMLP(net_structure)
        model.to(device)
        model.load_state_dict(weights)
        return model

    def load_data(self, labeled_path, unlabeled_path):
        """ take out those data labeled
        """
        with open(labeled_path) as ifile:
            data_labeled = json.load(ifile)
        with open(unlabeled_path) as ifile:
            data_unlabeled = json.load(ifile)

        labeled_sentences = set()
        for item in data_labeled:
            sentence = item['sentence'].lower()
            labeled_sentences.add(sentence)
        unlabeled_sentences = set()
        for s in data_unlabeled:
            unlabeled_sentences.add(s.lower())

        unlabeled_sentences = unlabeled_sentences - labeled_sentences
        unlabeled_sentences = sorted(list(unlabeled_sentences))
        return unlabeled_sentences

    def make_prediction(self, model, unlabeled_data, wv_path):
        """ go through all sentence give a classification score

        Returns:
            torch.tensor
        """
        import time
        print('encoding sentences as vectors')
        starttime = time.time()
        unlabeled_data = self.encode_sentences(unlabeled_data, wv_path)
        print('encoding ends, taks {} s'.format(time.time()-starttime))
        results = []
        # save GPU RAM
        with torch.no_grad():
            for s in unlabeled_data:
                s = torch.tensor(s).to(self.device)
                pred = model(s)
                results.append(pred)
        print('end prediction')
        results = torch.stack(results)
        return results

    def encode_sentences(self, data, wv):
        # NOTE only use BoW encoder now
        encoded_data = BoW_encoder(data, wv)
        return encoded_data

    def make_decision(self, predictions, batch_size, method='lc'):
        """ Based on prediction results from current model
        and selecting strategy to decide sentence need be queried

        Args:
            predictions (torch.tensor): prediction results from trained model
        """
        # TODO expand the selecting strategy
        if method == 'lc':
            pred = predictions.max(1)[0]
            # second return values is the indices i the original ordering
            sorted_confidences = torch.sort(pred)
            sorted_indices = sorted_confidences[1]
            least_confidences = sorted_indices[:batch_size]
            return least_confidences
        elif method == 'sm':
            margins = torch.abs(predictions[:, 0] - predictions[:, 1])
            sorted_margins = torch.sort(margins)
            sorted_indices = sorted_margins[1]
            smallest_margins = sorted_indices[:batch_size]
            return smallest_margins
        elif method == 'entropy':
            probs = F.softmax(predictions, dim=1)
            log_p = F.log_softmax(predictions, dim=1)
            entropies = - torch.sum(probs * log_p, dim=1)
            sorted_entropies = torch.sort(entropies)
            sorted_indices = sorted_entropies[1]
            largest_entropies = sorted_indices[-batch_size:]
            return largest_entropies
        else:
            print('no other selecting strategy available')
            return None

    def writeout_selected(self, samples, writeout_dir):
        """
        """
        if not exists(writeout_dir):
            makedirs(writeout_dir)
        outfilepath = join(writeout_dir, 'query_instances.json')
        with open(outfilepath, 'w') as ofile:
            json.dump(samples, ofile, indent=2)


if __name__ == '__main__':
    # fire.Fire(ActiveLearning)
    # curr_model = "../data/exp/fine_loc_new_data_ad_BoW25/wv_src_fine_loc.pickle"
    # curr_model = "../data/exp/fineloc_ac1_ad300_neg1_dropout0.3_BoW25/wv_src_fine_loc.pickle"
    curr_model = "../data/exp/fineloc_ac2_ad300_neg1_dropout0.3_BoW25/wv_src_fine_loc.pickle"
    # labeled_data = '../data/android_access_fine_loc_newly_aggregated.json'
    labeled_data = '../data/android_access_fine_loc_actlearn_lc_2nd.json'

    unlabeled_data = '../data/android_for_negative_samples.json'
    wv_modelpath = '../data/wv/android_w2v.kv'
    batch_size = 50
    selecting_strategy = 'lc'
    writeout_dir = '../data/active_learning/fineloc'

    learner = ActiveLearning()
    learner.select_unlabeled(curr_model,
                             labeled_data,
                             unlabeled_data,
                             wv_modelpath,
                             selecting_strategy,
                             batch_size,
                             writeout_dir)
