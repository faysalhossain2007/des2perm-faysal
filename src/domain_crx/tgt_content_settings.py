"""
Transfer from single source domain to chrome extension content settings
"""

# has to be the first import
from path_fix import CURR_DIR
# ===========================

from train_func import tgt_doc_level_train as train
from utils import get_logger, BoW_encoder
from tqdm import tqdm
import sys

def main():
    from_setting = sys.argv[1]
    src_model_t = "../../data/exp/for_crx_contentSettings/{}/".format(from_setting) +\
        "neg{}_dropoutNone_BoW2/model.pickle"
    # src_model = None
    src_model_alias_t = from_setting + '_neg{}'

    exp_dir_t = '../../data/exp/crx_content_settings/{}/'.format(from_setting) + \
        'from_{}_to_neg{}_wInit{}'

    tgt_data = '../../data/train-val-data/crx-domain/reformated/' +\
        'has.contentSettings.json.assigned_labels.json'
    wv_model = '../../data/wv/android_w2v.kv'
    neg_samples = '../../data/train-val-data/crx-domain/' + \
        'crx_for_negative_samples.json'

    import time
    start_t = time.time()
    for src_neg in tqdm(range(1, 10)):
        src_model = src_model_t.format(src_neg)
        src_model_alias = src_model_alias_t.format(src_neg)
        for negvspos in range(1, 10):
            w_copy = True
            exp_dir = exp_dir_t.format(src_model_alias, negvspos, w_copy)
            logger = get_logger(exp_dir)

            train(src_model,
                  src_model_alias,
                  tgt_data,
                  logger,
                  exp_dir,
                  neg_samples,
                  wv_model,
                  BoW_encoder,
                  data_neg_vs_pos=negvspos,
                  epochs=20,
                  weight_copy=w_copy)

            print("-----one exp done---- \n")

    print('cost:', time.time() - start_t)


if __name__ == '__main__':

    main()
