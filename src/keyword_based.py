import sys
import json
from utils import load_data


def check_keywords(sentence, keywords):
    """
    :param sentence: a string
    :param keywords: each item in the list contains words must appear in the sentence
    """
    for item in keywords:
        matched = True
        for w in item:
            status = w in sentence
            matched = matched and status
        if matched:
            return True
    return False


def load_keywords(keywords_filename):
    """
    :param keywords_filename: json file contains multiple sets of keywords
    """
    with open(keywords_filename, 'r') as ifile:
        keywords = json.load(ifile)

    # later might expand keywords sets here

    return keywords



def performance_eval(keywords, sentences, labels):
    """
    go through all samples, evaluate on each
    """
    TP = 0
    TN = 0
    FP = 0
    FN = 0
    for idx, sentence in enumerate(sentences):
        label = labels[idx][1]
        pred = check_keywords(sentence, keywords)

        if pred and label  == 1:
            TP += 1
        if pred and label == 0:
            FP += 1
        if not pred and label == 0:
            TN += 1
        if not pred and label == 1:
            FN += 1

    acc = (TP + TN) / (TP + TN + FP + FN)
    precision = (TP) / (TP + FP)
    recall = TP / (TP + FN)
    F1 = 2 * precision * recall / (precision + recall)

    return acc, precision, recall, F1



def main():
    """"""

    keywords_file = sys.argv[1]
    data_file = sys.argv[2]

    keywords = load_keywords(keywords_file)
    X_train, X_test, y_train, y_test = load_data(data_file,
                                                 random_state=45,
                                                 test_proportion=0.9,
                                                 make_balance=True)



    results = performance_eval(keywords, X_test, y_test)

    print('acc {}; prec {}; recall {}; f1 {};'.format(results[0],
                                                      results[1],
                                                      results[2],
                                                      results[3]))


if __name__ == '__main__':
    main()

