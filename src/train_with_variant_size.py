import re
import argparse
from os.path import join, expanduser
from os import listdir
from DAN.train import train as train_one
from time import strftime, localtime


def main():
    """"""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--data-dir', required=True)
    arg_parser.add_argument('--root-exp-dir', required=True)
    arg_parser.add_argument('--save-model-name')

    arg_parser.add_argument('--data-type', help='doc | sent',
                            required=True)
    arg_parser.add_argument('--processed', default=False,
                            help='processed or not',
                            type=bool)

    arg_parser.add_argument('--balance-train', default=True, type=bool)
    arg_parser.add_argument('--random-state', type=int)
    arg_parser.add_argument('--wv-path',
                            help="gensim KeyedVectors file; record vocabulary")
    arg_parser.add_argument('--vocab',
                            help="vocabulary file; list of words")
    arg_parser.add_argument('--word-dropout', default=0.2, type=float)
    arg_parser.add_argument('--nn-dropout', default=0.5, type=float)
    arg_parser.add_argument('--poolings', default='avg', type=str)

    arg_parser.add_argument('--wv-dim', default=300, type=int)
    arg_parser.add_argument('--hidden-layers', default='300,300',
                            type=str)
    arg_parser.add_argument('--classes', default=2, type=int)

    arg_parser.add_argument('--batch-size', default=20, type=int)
    arg_parser.add_argument('--grad-norm-limit', default=3.0,
                            type=float)
    arg_parser.add_argument('--epoch', default=20, type=int)
    arg_parser.add_argument('--gpu', default=-1, type=int)
    arg_parser.add_argument('--learning-rate', default=0.01, type=float)

    arg_parser.add_argument('--save-model-to', default='model.pt',
                            type=str)
    arg_parser.add_argument('--expdir',
                            default="~/Dev/desc2perm/data/exp")
    arg_parser.add_argument('--load-pretrain', type=str)

    args = arg_parser.parse_args()

    # -------- preprocess args
    if args.gpu < 0:
        print('using cpu')
        setattr(args, 'device', 'cpu')
    else:
        gpu_dev = 'cuda:' + str(args.gpu)
        print('using gpu', gpu_dev)
        setattr(args, 'device', gpu_dev)

    setattr(args, 'model_time', strftime('%Y-%m-%d-%H:%M:%S',
                                         localtime()))
    if not args.wv_path and not args.vocab:
        raise Exception(
            "need word vector file (gensim KeyedVectors) or vocabulary file")

    # process poolings arg
    pooling_str = args.poolings
    poolings = pooling_str.split(',')
    args.poolings = poolings

    # process hidden layer structure
    hidden_str = args.hidden_layers
    hidden_layers = [int(i) for i in hidden_str.split(',')]
    args.hidden_layers = hidden_layers

    # expand user dir
    args.wv_path = expanduser(args.wv_path)
    args.data_dir = expanduser(args.data_dir)
    args.root_exp_dir = expanduser(args.root_exp_dir)
    if args.vocab:
        args.vocab = expanduser(args.vocab)
    if args.expdir:
        args.expdir = expanduser(args.expdir)
    if args.load_pretrain:
        args.load_pretrain = expanduser(args.load_pretrain)
    # ---------- end preprocess args

    val_file = join(args.data_dir, 'val.json')
    test_file = join(args.data_dir, 'test.json')

    train_re = "^train\\.[0-9]*\\.json$"
    train_re = re.compile(train_re)

    # list available training data for different size
    datafiles = listdir(args.data_dir)
    for f in datafiles:
        f = f.strip()
        if train_re.match(f):
            sub_expdir = join(args.root_exp_dir, f)
            train_file = join(args.data_dir, f)
            setattr(args, 'train_data', train_file)
            setattr(args, 'val_data', val_file)
            setattr(args, 'test_data', test_file)
            setattr(args, 'expdir', sub_expdir)
            setattr(args, 'model_time', strftime('%Y-%m-%d-%H:%M:%S',
                                                 localtime()))
            print(args.expdir, args.train_data)
            train_one(args)


if __name__ == '__main__':
    main()
