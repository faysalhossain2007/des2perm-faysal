from train_func import tgt_doc_level_train as train
from utils import get_logger, BoW_encoder


def main():
    src_model_t = "../../data/exp/coarseloc/only_weather_apps/neg{}_dropoutNone_BoW25/model.pickle"
    # src_model = None
    src_model_alias_t = 'only_weather_neg{}'
    exp_dir_t = '../../data/exp/geoloc_{}'

    tgt_data = '../../../data_annotation/chrome-extensions-data/src/extracted_data/has.geolocation.json.assigned_labels.json'
    wv_model = '../../data/wv/android_w2v.kv'
    neg_samples = '../../data/train-val-data/crx-domain/crx_for_negative_samples.json'

    for src_neg in range(1, 10):
        src_model = src_model_t.format(src_neg)
        src_model_alias = src_model_alias_t.format(src_neg)
        exp_dir = exp_dir_t.format(src_model_alias)

        logger = get_logger(exp_dir)
        for negvspos in range(1, 10):
            train(src_model,
                  src_model_alias,
                  tgt_data,
                  logger,
                  exp_dir,
                  neg_samples,
                  wv_model,
                  BoW_encoder,
                  data_neg_vs_pos=negvspos)


if __name__ == '__main__':
    main()
