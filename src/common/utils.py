import json
import logging
import os
import random
import sys
from os.path import join, exists
from os import makedirs

import numpy as np
import spacy
import torch
from gensim.models import KeyedVectors
from gensim.utils import simple_preprocess
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset

sys.path.append('../encoder')


class LabeledDataset(Dataset):
    def __init__(self, X_emb, y, X_sents):
        self.data = list(zip(X_emb, y))
        self.sentences = X_sents

    def __len__(self):
        return len(self.sentences)

    def __getitem__(self, idx):
        return self.data[idx]

    def getSent(self, idx):
        return self.sentences[idx]


def sent_encoder_setup():
    """

    :rtype: BLSTMEncoder
    """

    GLOVE_PATH = '../../data/GloVe/glove.840B.300d.txt'
    MODEL_PATH = '../../encoder/infersent.allnli.pickle'
    # MODEL_PATH = '../encoder/new_trained.pickle.encoder'

    GLOVE_PATH = os.path.expanduser(GLOVE_PATH)
    MODEL_PATH = os.path.abspath(MODEL_PATH)

    # model setup
    map_loc = "cuda:0" if torch.cuda.is_available() else "cpu"
    model = torch.load(MODEL_PATH, map_location=map_loc)
    model.set_glove_path(GLOVE_PATH)
    model.build_vocab_k_words(K=100000)

    return model


def save_train_test_splits(data, writeout_dir):
    """
    :param data: consist of  X_train, X_test, y_train_onehot, y_test_onehot
    :return:
    """
    if not os.path.exists(writeout_dir):
        os.makedirs(writeout_dir)

    with open(join(writeout_dir, 'train.txt'),
              'w', encoding="utf-8") as output_file:
        for i in range(len(data[0])):
            out_str = "{} :: {}\n".format(data[2][i], data[0][i])
            output_file.write(out_str)
    with open(join(writeout_dir, 'test.txt'),
              'w', encoding="utf-8") as out_file:
        for i in range(len(data[1])):
            out_str = "{} :: {}\n".format(data[3][i], data[1][i])
            out_file.write(out_str)


def load_data(data_path, negatives='../data/android_for_negative_samples.json',
              random_state=None, test_proportion=0.4,
              neg_vs_pos=1):
    """ use transformed data
    :param neg_vs_pos: the counter example to positives,
        1 means 1 positive samples has 1 counter negative
    """
    random.seed(random_state)
    with open(data_path, 'r') as input_file:
        raw_data = json.load(input_file)

    X_pos = []
    X_neg = []

    y_pos = []
    y_neg = []

    random.seed(random_state)

    for item in raw_data:
        description = item['sentence']
        description = description.replace('\n', ' ')
        description = description.replace('\r', ' ')
        if int(item['label']) > 0:
            X_pos.append(description.lower())
            y_pos.append(1)
        else:
            X_neg.append(description.lower())
            y_neg.append(0)

    # to eliminate randomness from json load
    X_pos = sorted(X_pos)
    X_neg = sorted(X_neg)

    if neg_vs_pos == -1:
        X = X_pos + X_neg
        y = y_pos + y_neg
    else:
        need_neg_size = neg_vs_pos * len(y_pos)
        if need_neg_size < len(y_neg):
            reduced_neg = random.sample(X_neg, need_neg_size)
            X = X_pos + reduced_neg
            y = y_pos + y_neg[:need_neg_size]
        else:
            # enlarge negatives
            with open(negatives, 'r') as ifile:
                negative_pool = json.load(ifile)
            extra_neg_size = need_neg_size - len(y_neg)
            extra_neg = random.sample(negative_pool, extra_neg_size)
            X = X_pos + X_neg + extra_neg
            y = y_pos + [0] * need_neg_size

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=test_proportion, random_state=random_state)

    # transfer y as one-hot representation
    # y_train_onehot = np.zeros((len(y_train), 2), dtype=np.float32)
    # y_test_onehot = np.zeros((len(y_test), 2), dtype=np.float32)
    # for idx, i in enumerate(y_train):
    #     y_train_onehot[idx][i] = 1
    # for idx, i in enumerate(y_test):
    #     y_test_onehot[idx][i] = 1

    # return X_train, X_test, y_train_onehot, y_test_onehot
    return X_train, X_test, y_train, y_test


def eval_performance(logger, test_loader, device,
                     clf, test_labeled,
                     epoch_n, performance_collector):

    TP = 0
    TN = 0
    FP = 0
    FN = 0

    with torch.no_grad():

        for _i, (X, y) in enumerate(test_loader):
            X = X.to(device)
            y = y.to(device)
            pred = clf(X)
            sig = torch.nn.Sigmoid()
            pred = sig(pred).view(-1,) > 0.5
            pred = pred.item()
            # pred = pred.max(1, keepdim=True)[1].item()
            # y = y.max(1, keepdim=True)[1].item()
            y = y.item()
            if y == pred and y == 1:
                TP += 1
            elif y == pred and y == 0:
                TN += 1
            elif y != pred and y == 1:
                FN += 1
            elif y != pred and y == 0:
                FP += 1

            if y != pred:
                failure = {
                    'sentence': test_labeled.getSent(_i),
                    'prediction': pred,
                    'human-label': y
                }
                logger.info(json.dumps(failure))
        acc = (TN + TP) / len(test_loader)
        precision = TP / (TP + FP + 0.001)
        recall = TP / (TP + FN + 0.001)
        f1 = 2 * precision * recall / (precision + recall+0.0001)
        output_temp = "epoch {} on test: acc {}; prec {}; recall {}; f1 {}"
        logger.info(output_temp.format(epoch_n,
                                       (TN + TP) / len(test_loader),
                                       precision,
                                       recall, f1))
        logger.info("neg: {}, pos: {}".format((TN + FN), (TP + FP)))
        # test_acc_history.append((TP + TN) / len(test_loader))
        performance = {'acc': acc, 'prec': precision,
                       'recall': recall, 'f1': f1}
        for metric in performance_collector:
            performance_collector[metric].append(performance[metric])

        return performance


def save_history_acc(hist_acc, writeoutdir,
                     transfer_flag, freeze_flag,
                     lr, test_size):
    """"""
    if not os.path.exists(writeoutdir):
        os.makedirs(writeoutdir)

    ostr_temp = "/acc_hist_lr{}_test{}_trans{}_freeze{}.csv"
    outputfilename = writeoutdir + ostr_temp.format(lr,
                                                    test_size,
                                                    transfer_flag,
                                                    freeze_flag)
    with open(outputfilename, 'a') as outputfile:
        for idx, item in enumerate(hist_acc):
            outputfile.write(str(round(item, 4)))
            if idx != len(hist_acc) - 1:
                outputfile.write(',')
            else:
                outputfile.write('\n')


def clean_sentence(sentence,
                   only_main_words=True):
    """ remove useless words
    lemmatization
    """
    nlp = spacy.load('en_core_web_sm')
    doc = nlp(sentence)

    def is_main(word):
        # tags = ['NN', 'VB', 'JJ', 'RB', 'PR', '.']
        tags = ['NN', 'VB', 'JJ', '.']
        for prefix in tags:
            if prefix in word.tag_:
                return True
        return False

    tokens = []
    for token in doc:
        if only_main_words and not is_main(token):
            pass
        else:
            tokens.append(token)

    rtn = ' '.join(str(x) for x in tokens)
    return rtn


def BoW_encoder(sentences, embedding_model, preloaded=False):
    """ Bag of Words sentence encoder
    :type sentences: string
    :param embedding_model: point to file saving keyedvector file
    """
    if preloaded:
        wv = embedding_model
    else:
        model_path = embedding_model
        wv = KeyedVectors.load(model_path, mmap='r')

    sents_emb = []
    for s in sentences:
        tokens = simple_preprocess(s)
        tokens = filter(lambda x: x in wv.vocab, tokens)
        tokens = list(tokens)
        if len(tokens) < 1:
            s_emb = np.zeros(wv.vector_size, dtype=np.float32)
        else:
            embeddings = wv[tokens]
            # average word embeddings
            s_emb = np.mean(embeddings, axis=0)
        sents_emb.append(s_emb)

    sents_emb = np.array(sents_emb)
    return sents_emb


def get_logger(exp_dir):
    """ placing logging file under exp_dir

    """
    if not exists(exp_dir):
        makedirs(exp_dir)
    logger = logging.getLogger(os.path.basename(exp_dir))
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler(join(exp_dir, 'log.txt'))
    ch = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s - %(name)s- %(levelname)s :: %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    logger.addHandler(fh)
    logger.addHandler(ch)

    return logger
