import pickle

import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import Dataset


class MultiLabelMLP(nn.Module):
    """"""

    def __init__(self, structure, dropout=0.5):
        """ use ReLU default activation"""
        super(MultiLabelMLP, self).__init__()
        layers = []
        for i in range(len(structure) - 1):
            input_size = structure[i]
            output_size = structure[i + 1]
            layers.append(nn.Linear(input_size, output_size))
            if i != (len(structure) - 2):
                # no need for last layer
                layers.append(nn.ReLU())
                if dropout is not None:
                    layers.append(nn.Dropout(p=dropout))
                else:
                    pass

        self.model = nn.Sequential(*layers)

    def forward(self, x):
        """ Forward process of the neural networks
        :param x: input batch
        :type x: Tensor
        :rtype: Tensor
        """
        out = self.model(x)
        # out = torch.sigmoid(out)
        return out
