"""
using CNN on adjacency matrix
"""
from torch import nn
import torch
import torch.nn.functional as F


class BoWCNN(nn.Module):
    def __init__(self):
        """ fixing the structure first"""
        super(BoWCNN, self).__init__()
        # input 30 x 30 -> output 14 x 14
        self.conv1 = nn.Conv2d(1, 1, 4, 2)
        # input 14 x 14 -> output 4 x 4 -> will be flatten
        self.conv2 = nn.Conv2d(1, 1, 8, 2)
        # linear layers
        self.l1 = nn.Linear(300+16, 1000)
        self.l2 = nn.Linear(1000, 100)
        self.ol = nn.Linear(100, 1)

    def flatten(self, x):
        x = x.view(x.size(0), -1)
        return x

    def CNNonAdjacency(self, m):
        """"""
        o = F.relu(self.conv1(m))
        o = F.relu(self.conv2(o))
        return self.flatten(o)

    def forward(self, s_emb, adj_m):
        """
        """
        cnn_o = self.CNNonAdjacency(adj_m)
        comb_emb = torch.cat((s_emb, cnn_o), dim=1)
        o = F.relu(self.l1(comb_emb))
        o = F.relu(self.l2(o))
        o = self.ol(o)
        return o
