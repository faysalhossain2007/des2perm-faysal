
import json
import logging
import random
from os.path import join
from typing import Callable, Tuple, Dict
from gensim.models import KeyedVectors

import numpy as np
import torch
from sklearn.metrics import (accuracy_score, f1_score, precision_score,
                             recall_score)
from torch import optim
from torch.utils.data import DataLoader

from .MLP_model import MultiLabelMLP
from .utils import (BoW_encoder, LabeledDataset, eval_performance, load_data,
                   save_train_test_splits, sent_encoder_setup)


class PerformanceCollector:
    def __init__(self):
        self.acc = []
        self.prec = []
        self.recall = []
        self.f1 = []

    def add_record(self, metrics):
        """ adding acc, prec, recall, f1 values"""
        self.acc.append(metrics['acc'])
        self.prec.append(metrics['prec'])
        self.recall.append(metrics['recall'])
        self.f1.append(metrics['f1'])

    def serialize(self):
        """
        """
        performance_dict = {
            'acc': self.acc,
            'prec': self.prec,
            'recall': self.recall,
            'f1': self.f1
            }
        return performance_dict


def source_domain_train(logger, datafile,
                        neg_samples_file,
                        n_epochs, batch_size,
                        exp_dir, output_model,
                        sent_encoder,
                        embedding_model,
                        test_size=0.2,
                        learning_rate=0.1,
                        dropout=0.5,
                        random_state=None,
                        hidden_layers=[1000, 100],
                        dataset_neg_vs_pos=1):
    """ training process"""
    # hyper parameters
    # net_structure = [300, 1000, 100, 2]

    # encoder = sent_encoder_setup()
    dataset = load_data(datafile,
                        neg_samples_file,
                        random_state=random_state,
                        test_proportion=test_size,
                        neg_vs_pos=dataset_neg_vs_pos)

    # encode sentences
    if sent_encoder == 'InferSent':
        encoder = sent_encoder_setup()

        X_train_embeddings = encoder.encode(dataset[0])
        X_test_embeddings = encoder.encode(dataset[1])
    elif sent_encoder == 'BoW':
        X_train_embeddings = BoW_encoder(dataset[0], embedding_model)
        X_test_embeddings = BoW_encoder(dataset[1], embedding_model)
    else:
        raise Exception('invalid sentence encoder type; allowed encoder: BoW, InferSent')

    # exp_dir = exp_basedir + '_' + sent_encoder + str(random_state)
    save_train_test_splits(dataset, exp_dir)

    train_labeled = LabeledDataset(X_train_embeddings, dataset[2], dataset[0])
    test_labeled = LabeledDataset(X_test_embeddings, dataset[3], dataset[1])

    # init network
    # only deal with binary classification for now
    net_structure = [X_train_embeddings.shape[1]] + hidden_layers + [1]
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    clf = MultiLabelMLP(net_structure, dropout).to(device)
    opt = optim.SGD(clf.parameters(), lr=learning_rate)
    # loss_fn = torch.nn.BCELoss()
    loss_fn = torch.nn.BCEWithLogitsLoss(
        pos_weight=torch.tensor(dataset_neg_vs_pos, dtype=torch.float))

    # training process
    performances = {
        'acc': [],
        'prec': [],
        'recall': [],
        'f1': []
    }

    for _ in range(n_epochs):
        train_loader = DataLoader(train_labeled, batch_size, shuffle=True)

        for batch_idx, batch_data in enumerate(train_loader):
            X = batch_data[0].to(device)
            y = batch_data[1].to(device, dtype=torch.float)

            outputs = clf(X)
            # sig = torch.nn.Sigmoid()
            # loss = loss_fn(sig(outputs), y)
            y = y.view(-1, 1)
            loss = loss_fn(outputs, y.view(-1, 1))
            opt.zero_grad()
            loss.backward()
            opt.step()

            logger.info('loss: {}'.format(loss.item()))

        # eval on test
        test_loader = DataLoader(test_labeled, shuffle=False)

        eval_performance(logger, test_loader, device,
                         clf, test_labeled, _,
                         performances)

    # end training

    # saving model parameters
    model_path = join(exp_dir, output_model)
    torch.save(clf.state_dict(),
               model_path)

    # save out performance
    performance_logfile = join(exp_dir, 'performance_log.json')
    with open(performance_logfile, 'a+') as ofile:
        log_obj = {'logname': exp_dir.split('/')[-1],
                   'performances': performances}
        json.dump(log_obj, ofile)
        ofile.write('\n')


def weight_init(src_model_path, target_model, freeze, except_last=True):
    """"""
    if torch.cuda.is_available():
        pretrained_model = torch.load(src_model_path)
    else:
        pretrained_model = torch.load(src_model_path, map_location='cpu')

    except_last1layer = {}
    for idx, key in enumerate(pretrained_model):
        if idx < len(pretrained_model) - 2:
            except_last1layer[key] = pretrained_model[key]

    # TO-FIX quick try if transfer all weights
    # target_model.load_state_dict(except_last1layer, False)
    target_model.load_state_dict(pretrained_model, False)

    if freeze:
        # set lower layers requires_grad = False
        for idx, param in enumerate(target_model.parameters()):
            if idx < len(except_last1layer):
                param.requires_grad = False


def target_domain_training(logger,
                           data_file, exp_dir,
                           negative_samples, src_model,
                           sent_encoder,
                           embedding_model,
                           src_model_alias=None,
                           net_struct=[4096, 1000, 100, 2],
                           train_epochs=20,
                           learning_rate=0.01, batch_size=10,
                           dropout=0.5,
                           train_test_splitting_seed=None,
                           test_proportion=0.4,
                           dataset_neg_vs_pos=1,
                           freeze_transferred=False,
                           data_process_filters=None):
    """
    :param src_model: if is not None, enable transfer

    """
    # load data

    X_train, X_test, y_train, y_test = load_data(data_file,
                                                 negative_samples,
                                                 train_test_splitting_seed,
                                                 test_proportion,
                                                 neg_vs_pos=dataset_neg_vs_pos)
    # apply data preprocess
    if data_process_filters:
        for func in data_process_filters:
            X_train = func(X_train)
            X_test = func(X_test)

    # encode sentences
    if sent_encoder == 'InferSent':
        encoder = sent_encoder_setup()

        X_train_embeddings = encoder.encode(X_train)
        X_test_embeddings = encoder.encode(X_test)
    elif sent_encoder == 'BoW':
        X_train_embeddings = BoW_encoder(X_train, embedding_model)
        X_test_embeddings = BoW_encoder(X_test, embedding_model)
    else:
        raise Exception('invalid sentence encoder type; allowed encoder: BoW, InferSent')

    train_labeled = LabeledDataset(X_train_embeddings, y_train, X_train)
    test_labeled = LabeledDataset(X_test_embeddings, y_test, X_test)

    # init networks
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    clf = MultiLabelMLP(net_struct, dropout).to(device)
    opt = optim.SGD(clf.parameters(), lr=learning_rate)
    # loss_fn = torch.nn.BCELoss()
    loss_fn = torch.nn.BCEWithLogitsLoss(
        pos_weight=torch.tensor(dataset_neg_vs_pos, dtype=torch.float))

    # weight init/transfer
    if src_model:
        weight_init(src_model, clf, freeze_transferred)

    performances = {
        'acc': [],
        'prec': [],
        'recall': [],
        'f1': []
    }

    test_loader = DataLoader(test_labeled, shuffle=False)
    eval_performance(logger, test_loader, device,
                     clf, test_labeled, 0,
                     performances)

    for _ in range(train_epochs):
        train_loader = DataLoader(train_labeled, batch_size, shuffle=True)

        for batch_idx, batch_data in enumerate(train_loader):
            X = batch_data[0].to(device)
            y = batch_data[1].to(device)

            outputs = clf(X)
            loss = loss_fn(outputs, y)
            opt.zero_grad()
            loss.backward()
            opt.step()

            logger.info('loss: {}'.format(loss.item()))

        eval_performance(logger, test_loader, device,
                         clf, test_labeled, _,
                         performances)

    save_train_test_splits((X_train, X_test, y_train, y_test),
                           exp_dir)

    # currently no need to save target model

    # save out performance
    performance_logfile = join(exp_dir, 'performance_log.json')
    logname = exp_dir.split('/')[-1]
    if src_model:
        if src_model_alias:
            logname = '{}_{}_{}'.format(src_model_alias,
                                        freeze_transferred,
                                        logname)
        else:
            src_model_name = src_model.split('/')[-1].split('.')[0]
            logname = '{}_{}_{}'.format(src_model_name,
                                        freeze_transferred,
                                        logname)

    with open(performance_logfile, 'a+') as ofile:
        log_obj = {'logname': logname,
                   'performances': performances}
        json.dump(log_obj, ofile)
        ofile.write('\n')

    torch.save(clf.state_dict(), join(exp_dir, 'model.pickle'))


def load_src_model(model_path, dropout):
    """"""
    if model_path is None:
        return None

    if torch.cuda.is_available():
        model_weights = torch.load(model_path)
    else:
        model_weights = torch.load(model_path)

    net_structure = [300, 1000, 100, 1]

    mlp_model = MultiLabelMLP(net_structure, dropout=dropout)
    mlp_model.load_state_dict(model_weights, False)

    return mlp_model


def tgt_data_split(data_path, model):
    """ load the data
    using model to predict on each sentence

    return the document level selected data
    data need by processed
    """
    # TMP specific embedding
    wv_embedding_path = "../../data/wv/android_w2v.kv"

    # load the source data
    with open(data_path) as ifile:
        data = json.load(ifile)

    if model is None:
        # random select 10 for training, 10 for validation
        random.shuffle(data)
        return data[:15], data[15:20], data[20:]

    preds = []
    preds_stat = []
    for doc in data:
        sentences = doc['sentences']
        sents_str = []
        for item in sentences:
            sents_str.append(item['s'])
        sents_emb = BoW_encoder(sents_str, wv_embedding_path)
        sents_emb = torch.tensor(sents_emb)
        _preds = model(sents_emb)
        _preds = _preds.max(dim=1)[1].cpu().numpy()

        preds.append(_preds)
        preds_stat.append(np.count_nonzero(_preds))

    # sort on stats; return idx
    preds_stat = np.array(preds_stat)
    sorted_idx = np.argsort(preds_stat)

    # select 10 for training, 10 for validation
    train_set = []
    val_set = []
    test_set = []

    sorted_idx = sorted_idx.tolist()
    for i in range(len(sorted_idx)):
        if i < 15:
            item = data[sorted_idx.pop()]
            train_set.append(item)
        elif i < 20:
            item = data[sorted_idx.pop()]
            val_set.append(item)
        else:
            item = data[sorted_idx.pop()]
            test_set.append(item)

    return train_set, val_set, test_set


def data_preprocess(train: list, val: list, test: list,
                    neg_pos_ratio: int,
                    neg_samples: str):
    """
    extract sentences from rich data object
    control the neg-vs-pos ratio in train

    """
    def extract_sents(docs):
        """
        return 2d array each item contains all sentences in a document
        """
        sents = []
        m_labels = []
        for d in docs:
            _sents = []
            _labels = []
            for s in d['sentences']:
                _sents.append(s['s'])
                _labels.append(s['manual-eval'])
            sents.append(_sents)
            m_labels.append(_labels)

        return sents, m_labels

    def control_neg_ratio(dataX: list, dataY: list,
                          ratio: int, neg_samples: list):
        """
        control negative samples vs negative samples
        Args:
            dataX (list): contains sentences for each document.
            dataY (list): contains corresponding labels.
            ratio (int): one positive sentence vs <ratio> negative samples.
            neg_samples (list): negative samples pool, in case negatives
                                in DataX is not enough
        """
        pos_sents = []
        neg_sents = []
        for idx, doc in enumerate(dataX):
            doc_labels = dataY[idx]
            for i, s in enumerate(doc):
                if doc_labels[i] == 1:
                    pos_sents.append(s)
                else:
                    neg_sents.append(s)
        n = len(pos_sents)
        m = len(neg_sents)

        if n * ratio > m:
            d = n * ratio - m
            sampled_negs = random.sample(neg_samples, d)
            neg_sents += sampled_negs
        else:
            neg_sents = random.sample(neg_sents, n * ratio)

        p_dataX = pos_sents + neg_sents
        p_dataY = [1] * n + [0] * (n * ratio)
        return p_dataX, p_dataY

    # load negative samples pool
    with open(neg_samples) as ifile:
        neg_samples = json.load(ifile)

    # get for training
    trainX, trainY = extract_sents(train)
    # process training to control the neg vs pos ratio
    trainX, trainY = control_neg_ratio(trainX,
                                       trainY,
                                       neg_pos_ratio,
                                       neg_samples)

    # get for test and validation
    testX, testY = extract_sents(test)
    valX, valY = extract_sents(val)

    return (trainX, trainY), (valX, valY), (testX, testY)


def training_data_parse(batch_data, sent_encoder, wv_model_path, device):
    """

    """
    # extract X and Y
    X = []
    y = []
    for item in batch_data:
        X.append(item[0])
        y.append(item[1])

    X_emb = sent_encoder(X, wv_model_path)
    X_emb = torch.tensor(X_emb, dtype=torch.float)
    X_emb = X_emb.to(device)

    # # transform y to one-hot encoding
    # y_onehot = np.zeros((len(y), 2), dtype=np.float32)
    # for idx, i in enumerate(y):
    #     y_onehot[idx][i] = 1

    # y_onehot = torch.tensor(y_onehot, dtype=torch.float)
    # y_onehot = y_onehot.to(device)
    y = torch.tensor(y, dtype=torch.float)
    y = y.to(device)

    return X_emb, y


def performance_eval(epoch, model, eval_on,
                     sent_encoder, wv_model_path, device,
                     logger,
                     doc_collector: PerformanceCollector,
                     sent_collector: PerformanceCollector)->Tuple[Dict, Dict]:
    """
    pefromance evaluation on the `eval_on` data
    log out the evaluation results
    Args:
        eval_on (tuple): contains sentences and labels.
                         sentences and labels are grouped in document level

    """
    descriptions = eval_on[0]
    labels = eval_on[1]

    docPreds = []
    sentPreds = []

    # for failure cases debugging
    sentences = []

    # encoding all sentences
    sentences_emb = []
    wv_model = KeyedVectors.load(wv_model_path)
    for sents in descriptions:
        sents_emb = sent_encoder(sents, wv_model, preloaded=True)
        sentences.extend(sents)
        sentences_emb.append(sents_emb)

    with torch.no_grad():
        for sents_emb in sentences_emb:
            sents_emb = torch.tensor(sents_emb,
                                     dtype=torch.float).to(device)
            outputs = model(sents_emb)
            sig = torch.nn.Sigmoid()
            outputs = sig(outputs).view(-1,)
            # preds = outputs.max(dim=1)[1].cpu().numpy().tolist()
            preds = outputs > 0.5
            preds = preds.cpu().numpy().tolist()
            sentPreds.extend(preds)
            if 1 in preds:
                docPreds.append(1)
            else:
                docPreds.append(0)

    # flatting labels
    docLabels = []
    sentLabels = []
    for sLabels in labels:
        sentLabels.extend(sLabels)
        if 1 in sLabels:
            docLabels.append(1)
        else:
            docLabels.append(0)

    def eval_metric(truths, preds):
        """"""
        acc = accuracy_score(truths, preds)
        prec = precision_score(truths, preds)
        recall = recall_score(truths, preds)
        f1 = f1_score(truths, preds)
        return {'acc': acc, 'prec': prec, 'recall': recall, 'f1': f1}

    doc_metrics = eval_metric(docLabels, docPreds)
    sent_metrics = eval_metric(sentLabels, sentPreds)

    doc_collector.add_record(doc_metrics)
    sent_collector.add_record(sent_metrics)

    # print out failures
    for idx, i in enumerate(sentPreds):
        if i != sentLabels[idx]:
            logger.info('failure {}, label: {}, pred {}'.format(
                sentences[idx], sentLabels[idx], i))

    return doc_metrics, sent_metrics


def output_performance(logger: logging.Logger,
                       metrics: dict, epoch: int,
                       eval_on: str):
    """
    output performance
    """
    doc_metrics = metrics[0]
    sent_metrics = metrics[1]
    template = """
    epoch {}, evaluate on {}, {} level :: acc {}; prec {}; recall {}; f1 {}
    """
    doc_str = template.format(
        epoch, eval_on, 'doc', doc_metrics['acc'], doc_metrics['prec'],
        doc_metrics['recall'], doc_metrics['f1'])
    logger.info(doc_str)

    sent_str = template.format(
        epoch, eval_on, 'sent', sent_metrics['acc'], sent_metrics['prec'],
        sent_metrics['recall'], sent_metrics['f1'])
    logger.info(sent_str)


def save_data(train, val, test, exp_dir):
    """ saving data splitting into exp_dir folder
    """
    with open(join(exp_dir, 'train.json'), 'w') as ofile:
        json.dump(train, ofile, indent=2)
    with open(join(exp_dir, 'val.json'), 'w') as ofile:
        json.dump(val, ofile, indent=2)

    with open(join(exp_dir, 'test.json'), 'w') as ofile:
        json.dump(test, ofile, indent=2)


def tgt_doc_level_train(src_model_path: str,
                        src_model_alias: str,
                        tgt_data_path: str,
                        logger: logging.Logger,
                        exp_dir: str,
                        negative_samples: str,
                        wv_model_path: str,
                        sent_encoder: Callable,
                        net_struct=[300, 1000, 100, 1],
                        learning_rate=0.01,
                        batch_size=10,
                        epochs=20,
                        data_neg_vs_pos=1,
                        weight_copy=True,
                        freeze_transferred=False
                        ):
    """ load the document level target
    load the source domain to classify on
    on all sentences pick up the most
    documents contain positives
    """

    # load the model
    dropout = None
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    src_model = load_src_model(src_model_path, dropout)
    D_train, D_val, D_test = tgt_data_split(tgt_data_path, src_model)
    # saving the train, val, test
    save_data(D_train, D_val, D_test, exp_dir)
    D_train, D_val, D_test = data_preprocess(D_train, D_val, D_test,
                                             data_neg_vs_pos, negative_samples)

    clf = MultiLabelMLP(net_struct, dropout).to(device)
    opt = optim.SGD(clf.parameters(), lr=learning_rate)
    loss_fn = torch.nn.BCEWithLogitsLoss(
        pos_weight=torch.tensor(data_neg_vs_pos, dtype=torch.float))

    if src_model_path and weight_copy:
        weight_init(src_model_path, clf, freeze_transferred)

    val_doc_perform_c = PerformanceCollector()
    val_sent_perform_c = PerformanceCollector()
    test_doc_perform_c = PerformanceCollector()
    test_sent_perform_c = PerformanceCollector()

    D_train = list(zip(D_train[0], D_train[1]))

    # evaluation before training starts
    print('failures on validation')
    val_perform = performance_eval(0, clf, D_val,
                                   BoW_encoder, wv_model_path, device,
                                   logger,
                                   val_doc_perform_c, val_sent_perform_c)
    print('failures on test')
    test_perform = performance_eval(0, clf, D_test,
                                    BoW_encoder, wv_model_path, device,
                                    logger,
                                    test_doc_perform_c,
                                    test_sent_perform_c)
    output_performance(logger, val_perform, 0, 'validation')
    output_performance(logger, test_perform, 0, 'test')

    for e in range(1, epochs+1):
        random.shuffle(D_train)
        train_size = len(D_train)
        for i in range(0, train_size, batch_size):
            if i+batch_size < train_size:
                batch_data = D_train[i: i + batch_size]
            else:
                batch_data = D_train[i:]

            X, y = training_data_parse(batch_data, sent_encoder,
                                       wv_model_path, device)
            outputs = clf(X)
            loss = loss_fn(outputs, y.view(-1, 1))
            opt.zero_grad()
            loss.backward()
            opt.step()

            logger.info('loss: {}'.format(loss.item()))

        # evaluation on validation set and test data set
        print('failures on validation')
        val_perform = performance_eval(e, clf, D_val, sent_encoder,
                                       wv_model_path, device,
                                       logger,
                                       val_doc_perform_c,
                                       val_sent_perform_c)
        print('failures on test')
        test_perform = performance_eval(e, clf, D_test, sent_encoder,
                                        wv_model_path, device,
                                        logger,
                                        test_doc_perform_c,
                                        test_sent_perform_c)

        output_performance(logger, val_perform, e, 'validation')
        output_performance(logger, test_perform, e, 'test')

    # write out performance log
    performance_logfile = join(exp_dir, 'performance_log.json')
    logname = exp_dir.split('/')[-1]
    logname = '{}_{}_{}_neg{}'.format(src_model_alias,
                                      freeze_transferred,
                                      logname, data_neg_vs_pos)

    with open(performance_logfile, 'a+') as ofile:
        log_obj = {'logname': logname,
                   'val_doc_performances': val_doc_perform_c.serialize(),
                   'val_sent_performances': val_sent_perform_c.serialize(),
                   'test_doc_performances': test_doc_perform_c.serialize(),
                   'test_sent_performances': test_sent_perform_c.serialize()}
        json.dump(log_obj, ofile)
        ofile.write('\n')

    # saving the model
    torch.save(clf.state_dict(), join(exp_dir, 'model.pickle'))
