"""training the model at source domain with CNN"""

from torch.utils.data import Dataset, DataLoader
from common import BoWCNN, load_source_data
from tools import parse_sentences
from common import BoW_encoder
from gensim.models import KeyedVectors
import json
import random
from torch import nn
import torch
from sklearn.metrics import (accuracy_score, f1_score, precision_score,
                             recall_score)
from tqdm import tqdm


def eval_metric(truths, preds):
    """"""
    acc = accuracy_score(truths, preds)
    prec = precision_score(truths, preds)
    recall = recall_score(truths, preds)
    f1 = f1_score(truths, preds)
    return {'acc': acc, 'prec': prec, 'recall': recall, 'f1': f1}


class SourceData(Dataset):

    def __init__(self, X, y):
        self.data = list(zip(X, y))

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]


class Trainer():
    def __init__(self, datapath, performance_output_path,
                 r_struct=False, no_structure=False):
        """configuration"""
        self.p_log = performance_output_path
        self.datafile = datapath
        self.randomize_structure = r_struct
        self.no_structure = no_structure
        self.negVSpos = 1
        self.random_seed = 1
        self.bsize = 10
        self.max_length = 30
        self.parsingURL = 'http://localhost:9000'
        self.wv_model = '../data/wv/android_w2v.kv'
        self.wv_model = KeyedVectors.load(self.wv_model)

        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = BoWCNN().to(self.device)
        self.loss_fn = nn.BCEWithLogitsLoss(
            torch.tensor(self.negVSpos, dtype=torch.float))
        self.optimizer = torch.optim.SGD(self.model.parameters(), lr=0.01)

        self.performances = {
            'acc': [],
            'prec': [],
            'recall': [],
            'f1': []
        }

    def performance_test(self, dataset):
        """
        do prediction on dataset
        """
        dataloader = DataLoader(dataset, batch_size=self.bsize)
        truths = []
        predictions = []
        with torch.no_grad():
            for b_idx, b_data in enumerate(dataloader):
                outputs = self.get_model_outputs(b_data)
                outputs = torch.sigmoid(outputs)
                preds = outputs > 0.5
                preds = preds.cpu().numpy().tolist()
                predictions.extend(preds)
                labels = b_data[1].cpu().numpy().tolist()
                truths.extend(labels)

        eval_results = eval_metric(truths, predictions)
        self.performances['acc'].append(eval_results['acc'])
        self.performances['prec'].append(eval_results['prec'])
        self.performances['f1'].append(eval_results['f1'])
        self.performances['recall'].append(eval_results['recall'])
        print(eval_results)

    def get_model_outputs(self, batch_data):
        # X should be a list contains sentences in text
        X = batch_data[0]
        X_adj_m = parse_sentences(self.parsingURL, X)
        if self.randomize_structure:
            random.seed(self.random_seed)
            random.shuffle(X_adj_m)
        if self.no_structure:
            X_adj_m = torch.zeros(len(X_adj_m),
                                  X_adj_m[0].shape[0],
                                  X_adj_m[0].shape[1]).to(
                                      self.device, dtype=torch.float)
        else:
            X_adj_m = torch.tensor(X_adj_m).to(self.device, dtype=torch.float)
        X_emb = BoW_encoder(X, self.wv_model, preloaded=True)
        X_emb = torch.tensor(X_emb).to(self.device, dtype=torch.float)

        outputs = self.model(X_emb, torch.unsqueeze(X_adj_m, dim=1))
        return outputs

    def train(self):
        """training function"""

        X_train, X_test, y_train, y_test = load_source_data(
            self.datafile, random_state=self.random_seed,
            test_proportion=0.2, neg_vs_pos=self.negVSpos)

        trainData = SourceData(X_train, y_train)
        testData = SourceData(X_test, y_test)

        trainDataLoader = DataLoader(trainData, batch_size=self.bsize,
                                     shuffle=True)

        self.performance_test(testData)
        for e in tqdm(range(10)):
            for b_data in trainDataLoader:
                y = b_data[1].to(self.device, dtype=torch.float)
                outputs = self.get_model_outputs(b_data)
                loss = self.loss_fn(outputs, y.view(-1, 1))
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

                print('loss: {}'.format(loss.item()))

            self.performance_test(testData)

        with open(self.p_log, 'w') as ofile:
            json.dump(self.performances, ofile, indent=2)


if __name__ == "__main__":
    # train()
    d = '../data/train-val-data/android-domain/fine-coarse-and-write-apn/combined.json'
    for i in range(10):
        p_log = '../data/exp/withCNN/log{}_no_struct.json'.format(i)
        trainer = Trainer(d, p_log, r_struct=False, no_structure=True)
        trainer.train()
