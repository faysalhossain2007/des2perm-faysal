import json
import requests
import numpy as np
import operator
from gensim.models import KeyedVectors
from scipy.spatial.distance import cosine
from tqdm import tqdm
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score


def sim_two_group_pairs(semantic_pairs, sent_pairs, wv_model, weight=1.0):
    most_sim_pairs_scores = []
    most_sim_pairs = []
    for pair in sent_pairs:
        _scores = []
        _pair_pair = []
        for sem_pair in semantic_pairs:
            _score = pair_similarity(pair, sem_pair[0], wv_model, weight=weight)
            _scores.append(_score)
            _pair_pair.append((pair, sem_pair[0]))
        _max = max(_scores)
        _max_idx = _scores.index(_max)
        _max_pair = _pair_pair[_max_idx]
        
        most_sim_pairs_scores.append(_max)
        most_sim_pairs.append(_max_pair)

    # predict via threshold
    if not most_sim_pairs_scores:
        max_sim_score = 0
        max_sim_pair = None
    else:
        max_sim_score = max(most_sim_pairs_scores)
        max_idx = most_sim_pairs_scores.index(max_sim_score)
        max_sim_pair = most_sim_pairs[max_idx]
    return max_sim_score, max_sim_pair


def getn_TN_TP_FP_FN(truths, preds):
    if len(truths) != len(preds):
        return "length not match"
    TN = 0
    TP = 0
    FP = 0
    FN = 0
    for idx, t in enumerate(truths):
        if t == preds[idx]:
            if t == 1:
                TP += 1
            else:
                TN += 1
        else:
            if preds[idx] == 1:
                FP += 1
            else:
                FN += 1
        
    return  TN, TP, FP, FN


def depparse(text):
    """"""
    url = "http://localhost:9000"
    deparse_url = url + "/?properties=" +\
        "{'annotators':'tokenize,ssplit,pos,depparse','outputFormat':'json'}"
    response = requests.post(deparse_url, data=text)
    response = response.json()
    return response


def extract_compound(dependencies, target_governor):
    compound_words = []
    for d in dependencies:
        if target_governor == d['governor'] and d['dep'] == 'compound':
            compound_words.append(d['dependentGloss'])
    return compound_words


def extract_word_pairs(sent):
    
    parsed = depparse(sent)
    tokens = parsed['sentences'][0]['tokens']
    deps = parsed['sentences'][0]['enhancedPlusPlusDependencies']
    deps_by_governor = {}
    deps_by_dependent = {}
    for d in deps:
        g_id = d['governor']
        d_id = d['dependent']
        if g_id in deps_by_governor:
            deps_by_governor[g_id].append(d)
        else:
            deps_by_governor[g_id] = [d]
        if d_id in deps_by_dependent:
            deps_by_dependent[d_id].append(d)
        else:
            deps_by_dependent[d_id] = [d]
    
    pairs = []
    for d in deps:
#         if d['dep'] == "amod" or d['dep'] == "nmod:poss" or d['dep'] == "advmod":
#         if d['dep'] == "amod" or d['dep'] == "advmod":
        g_id = d['governor']
        d_id = d['dependent']
        if d['dep'] == "amod" or d['dep'] == "nmod:poss" or d['dep'] == "advmod":
#         if d['dep'] == "amod":
            governor = d['governorGloss']
            
            dependent = d['dependentGloss']
            
            pairs.append((dependent.lower(), governor.lower()))
            
            # also consider the compound word with the dependent
            compound_words = extract_compound(deps, g_id)
            for w in compound_words:
                pairs.append((dependent.lower(), w))
                
        if d['dep'] == 'compound':
            pairs.append((d['dependentGloss'].lower(), d['governorGloss'].lower()))
        if d['dep'] == 'dobj':
            compound_words = extract_compound(deps, d_id)
            pairs.append((d['governorGloss'].lower(), d['dependentGloss'].lower()))
            
            for w in compound_words:
                pairs.append((d['governorGloss'].lower(), w))
        
        if d['dep'] == 'nsubj':
            g_pos = tokens[g_id-1]['pos']
            if g_pos == "VBP":
                # get the child of this governer with advmod dependency
                for s_d in deps_by_governor[g_id]:
                    if s_d['dep'] == "advmod":
                        pairs.append((d['dependentGloss'].lower(), s_d['dependentGloss'].lower()))
        
        if d['dep'].startswith('nmod'):
            g_pos = tokens[g_id - 1]['pos']
            if g_pos.startswith("VB"):
                pairs.append((d['governorGloss'].lower(), d['dependentGloss'].lower()))
                

    return list(set(pairs))


def get_pairs_from_sentences(sentences):
    all_pairs = []
        
    for item in sentences:
        label = item['label']
        if int(label) > 0:
            sentence = item['sentence'].lower()
            pairs = extract_word_pairs(sentence)
            #print('label',label, sentence, pairs)
            all_pairs += pairs
    # count pairs
    counter = {}
    for p in all_pairs:
        if p in counter:
            counter[p] += 1
        else:
            counter[p] = 1
    # sort pairs
    sorted_pairs = sorted(counter.items(), key=operator.itemgetter(1))
    
    return sorted_pairs

def get_pairs_for_domain(domain_datafile):
    with open(domain_datafile) as ifile:
        data = json.load(ifile)
        sorted_pairs = get_pairs_from_sentences(data)
    return sorted_pairs

def wv_sim(w1, w2, wv):
    # euclidean distance
    if w1 in wv and w2 in wv:
        w1 = wv[w1]
        w2 = wv[w2]
    #     sim = np.linalg.norm(w1 - w2)
        # cosine similarity
        sim = 1 - cosine(w1, w2)
        return sim
    else:
        return 0

def pair_similarity(p1, p2, wv_model, weight=1.0):
    """
    :param weight: indicate the weight on modified word (second word in pair)
    """
    
    s1 = wv_sim(p1[0], p2[0], wv_model)
    s2 = wv_sim(p1[1], p2[1], wv_model)
    score = s1 + s2 * weight
    score = score * (1 + 1) / (1 + weight)
    return score