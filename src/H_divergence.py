""" computing H-divergence from the paper:
a theory of learning from different domains

"""
import json
import torch
import random
from torch.utils.data import Dataset, DataLoader
from torch.nn import BCELoss
from common.utils import BoW_encoder
from common.MLP_model import MultiLabelMLP
import numpy as np


config = {}


class SentenceDataset(Dataset):
    """"""
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]


def data_preprocess(Ds, Dt, m):
    """
    assigin labels for source domain Ds as 0 and
    target domain Dt as 1

    """
    source_data = []
    target_data = []
    for s in Ds:
        source_data.append((s, 0))
    for s in Dt:
        target_data.append((s, 1))

    if m > len(source_data):
        sampled_src = source_data
    else:
        sampled_src = random.sample(source_data, int(m))
    if m > len(target_data):
        sampled_tgt = target_data
    else:
        sampled_tgt = random.sample(target_data, int(m))
    data = sampled_src + sampled_tgt

    return data


def load_data(ifilepath: str, mode: str) -> list:
    """ extract all sentences from the data file

    Arguments:
        ifilepath: input file path
        mode: 'sent' or 'doc', we have two type of data
    """
    rtn_data = []
    with open(ifilepath, 'r') as ifile:
        raw_data = json.load(ifile)

    for item in raw_data:
        if mode == 'doc':
            sentences = item['sentences']
            for s in sentences:
                rtn_data.append(s['s'])
        elif mode == 'sent':
            rtn_data.append(item['sentence'])
        else:
            raise Exception('wrong mode type')
    return rtn_data


def H_divergence(model, dataset):
    """ model as found h function
    using dataset labels as identification function
    """
    device = config['device']
    m = config['m']
    bsize = config['bsize']
    loader = DataLoader(dataset, batch_size=bsize, shuffle=False)
    tgt_I = 0
    src_I = 0
    src_size = 0
    tgt_size = 0
    with torch.no_grad():
        for b_idx, b_data in enumerate(loader):
            X = b_data[0]
            y = b_data[1]

            X_emb = BoW_encoder(X, config['wv_modelpath'])
            X_emb = torch.tensor(X_emb)
            X_emb = X_emb.to(device)

            outputs = model(X_emb)
            outputs = outputs.squeeze()
            outputs = torch.sigmoid(outputs)

            y_preds = outputs > 0.5

            for i in range(y_preds.shape[0]):
                _pred = y_preds[i].item()
                _truth = y[i].item()

                if _truth == 1:
                    tgt_size += 1
                else:
                    src_size += 1

                if _truth == 1 and _pred == 0:
                    tgt_I += 1
                if _truth == 0 and _pred == 1:
                    src_I += 1

    dH = 2 * (1 - (src_I/src_size + tgt_I/tgt_size))
    return dH


def calc_H_divergence(config):
    """"""
    device = config['device']
    m = config['m']
    bsize = config['bsize']
    n_epochs = config['n_epochs']
    source_domain = config['source_domain']
    target_domain = config['target_domain']
    wv_modelpath = config['wv_modelpath']

    source_data = load_data(source_domain[0], source_domain[1])
    target_data = load_data(target_domain[0], target_domain[1])
    training_data = data_preprocess(source_data, target_data, m)
    training_dataset = SentenceDataset(training_data)

    # construct model MLP
    clf = MultiLabelMLP([300, 500, 1], dropout=None)
    clf.to(device)
    loss_fn = BCELoss()
    # opt = torch.optim.SGD(clf.parameters(), lr=0.01)
    opt = torch.optim.Adam(clf.parameters())

    for e in range(n_epochs):
        training_dataloader = DataLoader(training_dataset,
                                         batch_size=bsize, shuffle=True)
        for b_idx, batch_data in enumerate(training_dataloader):
            X = batch_data[0]
            y = batch_data[1]

            # encoding X as sentence embedding
            X_emb = BoW_encoder(X, wv_modelpath)
            X_emb = torch.tensor(X_emb)
            X_emb = X_emb.to(device)
            y = y.to(device, dtype=torch.float)

            outputs = clf(X_emb)
            outputs = outputs.squeeze()
            loss = loss_fn(torch.sigmoid(outputs), y)
            opt.zero_grad()
            loss.backward()
            opt.step()

            # print('loss: ', loss.item())


    divergence = H_divergence(clf, training_dataset)
    # print(divergence)
    return divergence


def main():

    src_domains = [
        # ('../data/train-val-data/android-domain/fine-and-coarse-location/added-pos-trunks/fine_plus_coarse_pos_trunks.json', 'sent'),
        ('../data/train-val-data/android-domain/access-fine-location/android.permission.ACCESS_FINE_LOCATION.reformated_train_val.json', 'doc'),
        ('../data/train-val-data/android-domain/access-coarse-location/android.permission.ACCESS_COARSE_LOCATION.reformated_train_val.json.patched', 'doc'),
        ('../data/train-val-data/android-domain/android_camera_500.json', 'sent'),
        ('../data/train-val-data/android-domain/android_modify_contacts_546.json', 'sent'),
        ('../data/train-val-data/android-domain/android_read_cal_whyper.json', 'sent'),
        ('../data/train-val-data/android-domain/android_read_con_whyper.json', 'sent'),
        ('../data/train-val-data/android-domain/android_write_settings.json', 'sent'),
        ('../data/train-val-data/android-domain/android_send_sms.json', 'sent'),
        ('../data/train-val-data/android-domain/android_zz_write_apn_settings.json', 'sent')
        ]

    # tgt_domain = ('../../data_annotation/chrome-extensions-data/src/extracted_data/has.geolocation.json.assigned_labels.json', 'doc')
    # tgt_domain = ('../data/train-val-data/ifttt-domain/ifttt_evernote_action.json', 'sent')

    tgt_domains = [
        # ('../data/train-val-data/ifttt-domain/ifttt_evernote_trigger.json', 'sent'),
        # ('../data/train-val-data/ifttt-domain/ifttt_bmwlab_action.json', 'sent'),
        # ('../data/train-val-data/ifttt-domain/ifttt_bmwlab_trigger.json', 'sent'), - problematic
        # ('../data/train-val-data/ifttt-domain/ifttt_fb_action.json.transformed.json', 'sent'),
        # ('../data/train-val-data/ifttt-domain/ifttt_gcal_trigger.json.transformed.json', 'sent'),
        # ('../data/train-val-data/ifttt-domain/ifttt_gcon_trigger.json.transformed.json', 'sent'),
        # ('../data/train-val-data/ifttt-domain/ifttt_instagram_trigger.json', 'sent')

        # ('../data/train-val-data/crx-domain/crx_content_settings.json', 'sent'),
        # ('../data/train-val-data/crx-domain/crx_proxy_cleaned.json', 'sent'),
        # ('../data/train-val-data/crx-domain/geolocation/crx_geolocation_cleaned.json', 'sent')
        ('../data/train-val-data/crx-domain/reformated/has.geolocation.json.assigned_labels.json', 'doc'),
        ('../data/train-val-data/crx-domain/reformated/has.proxy.json.assigned_labels.json', 'doc'),
        ('../data/train-val-data/crx-domain/reformated/has.contentSettings.json.assigned_labels.json', 'doc')

    ]

    config_template = {
        'device': 'cuda:0',
        'm': 1000,
        'bsize': 50,
        'n_epochs': 5,
        'source_domain': None,
        'target_domain': None,
        'wv_modelpath': '../data/wv/android_w2v.kv'
        }

    for t_domain in tgt_domains:
        print('='*40, t_domain[0])

        for domain in src_domains:
            # run 10 times to get the avg
            dHs = []
            for _ in range(10):
                _c = config_template.copy()
                _c['source_domain'] = domain
                _c['target_domain'] = t_domain
                global config
                config = _c
                dH = calc_H_divergence(config)
                dHs.append(dH)

            domain_name = domain[0].split('/')[-1]
            print('from', domain_name, ':', np.mean(dHs))


if __name__ == "__main__":
    main()
