import json
import enchant
import sys
from nltk.tokenize import TweetTokenizer

dict = enchant.Dict("en_US")
tknzr = TweetTokenizer()

threshold = 0.5

def check_en(sentence):
    """
    word tokenize first, then check word by word
    :param sentence:
    :return:
    """
    tokenized = tknzr.tokenize(sentence)
    full_len = len(tokenized)
    en_words = 0
    non_en_words = 0
    for w in tokenized:
        if dict.check(w):
            en_words += 1
        else:
            non_en_words += 1

    if full_len == 0 or en_words / full_len > threshold:
        return True
    else:
        return False


def main(filename):
    """"""
    eng_items = []
    with open(filename, 'r') as ifile:
        data = json.load(ifile)
        for item in data:
            title_str = item['recipe_title']
            desc_str = item['recipe_desc']
            if check_en(title_str) and \
                check_en(desc_str):
                eng_items.append(item)

    with open(filename+'.eng', 'w') as ofile:
        ostr = json.dumps(eng_items, indent=2)
        ofile.write(ostr)


if __name__ == '__main__':
    filename = sys.argv[1]
    main(filename)