import json
import spacy
import heapq
import sys


def get_corpus_ifttt(filename):
    """ get corpus from data of ifttt platform
    :return : a list of sentence
    """
    corpus = []
    with open(filename, 'r', encoding='utf-8') as ifile:
        data = json.load(ifile)
    for row in data:
        corpus.append(row['recipe_title'])
        corpus.append(row['recipe_desc'])

    return corpus


def get_corpus_android(filename):
    """ get corpus from data of android application
    :return : a list of sentences
    """
    corpus = []
    with open(filename, 'r', encoding='utf-8') as ifile:
        data = json.load(ifile)
    for row in data:
        corpus.append(row['sentence'])

    return corpus


def analyze_corpus(corpus, lm):
    """
    :param corpus: sentences
    :param lm: language model
    """

    verb_dict = {}
    noun_dict = {}
    def add_verb(v):
        if v in verb_dict:
            verb_dict[v] += 1
        else:
            verb_dict[v] = 1

    def add_noun(n):
        if n in noun_dict:
            noun_dict[n] += 1
        else:
            noun_dict[n] = 1

    # only consider POS tag now
    for sentence in corpus:
        tokens = lm(sentence)
        for t in tokens:
            tag = t.tag_
            if tag.startswith('VB'):
                add_verb(t.lemma_)
            elif tag.startswith('NN'):
                add_noun(t.lemma_)

    return verb_dict, noun_dict


def most_common_words(filename, filetype, lm_model_name, top_k=10):
    """ return most common words in <filename>
    :param filename: the file we want to parse
    :param filetype: either 'ifttt' or 'android'
    :param lm_model_name: specify the model name from spacy
    """
    nlp = spacy.load(lm_model_name)
    if filetype == 'ifttt':
        corpus = get_corpus_ifttt(filename)
    elif filetype == 'android':
        corpus = get_corpus_android(filename)
    verbs, nouns = analyze_corpus(corpus, nlp)

    top_verbs = heapq.nlargest(top_k, verbs, key=verbs.get)
    top_nouns = heapq.nlargest(top_k, nouns, key=nouns.get)

    return top_verbs, top_nouns


def main():
    """"""
    # analyze_file = '../data/ifttt_fb_action.json.transformed.json'
    # filetype = 'ifttt'
    analyze_file = sys.argv[1]
    filetype = sys.argv[2]
    lm_model_name = 'en_core_web_sm'

    top_verbs, top_nouns = most_common_words(analyze_file,
                                             filetype,
                                             lm_model_name,
                                             20)

    print('common nouns:', top_nouns)
    print('common verbs:', top_verbs)


if __name__ == '__main__':
    main()
