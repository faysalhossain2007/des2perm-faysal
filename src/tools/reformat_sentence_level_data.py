"""
format data to the document level
{
'appid':
'apptype': 'android-app'
'permissions':
'sentences':  #sentence level labeled data go here
}

"""


import json
from os.path import join, dirname


def format_one_app(appid, perm_dict,
                   consider_perm,
                   app_type,
                   sentences,
                   pos_sentence_set):
    # default manual label 0
    # check autocog prediction results later
    formated_sentences = []
    for s in sentences:
        s_item = {
            's': s,
            'manual-eval': 0,
            'autocog': -1
            }
        if s in pos_sentence_set:
            s_item['manual-eval'] = 1
        formated_sentences.append(s_item)

    # look up permissions
    apkname = appid + '.apk'
    permissions = perm_dict[apkname]

    formated_app = {
        'appid': appid,
        'apptype': app_type,
        'consider_perm': consider_perm,
        'permissions': permissions,
        'sentences': formated_sentences
        }
    return formated_app


def main():
    """"""
    consider_perm = 'android.permission.ACCESS_FINE_LOCATION'
    doc_level_filepath = "../../data/document-level-data/android-domain/ACCESS_FINE_LOCATION.for_eval.json"

    # sent_level_folder = '../../data/sentence-level-data/android-domain/access-fine-location/added-pos-trunks'
    sent_level_filepath = "../../data/sentence-level-data/android-domain/access-fine-location/added-pos-trunks/android_access_fine_loc_added_pos_trunks.json"
    # sentence_level_files = [
    #     join(sent_level_folder, 'train/pos.json'),
    #     join(sent_level_folder, 'train/neg.json'),
    #     join(sent_level_folder, 'test/pos.json'),
    #     join(sent_level_folder, 'test/neg.json'),
    #     join(sent_level_folder, 'val/pos.json'),
    #     join(sent_level_folder, 'val/neg.json')
    #     ]

    app_desc_filepath = '../../../data_annotation/49183_apps_desctext.json'
    perm_dict_filepath = '../../../data_annotation/49183_apps_permdict.json'
    app_type = 'android-app'

    # load the test document level data, use appid as key
    with open(doc_level_filepath) as ifile:
        doc_level_data = json.load(ifile)
    test_doc_level_dict = {}
    for app in doc_level_data:
        appid = app['appid']
        test_doc_level_dict[appid] = app


    # load train/val sentence level data
    # split them into positive and negatives
    pos_sentences = set()
    neg_sentences = set()
    # for fpath in sentence_level_files:
    with open(sent_level_filepath) as ifile:
        tmp_data = json.load(ifile)
        for item in tmp_data:
            label = int(item['label'])
            if label == 1:
                pos_sentences.add(item['sentence'].lower())
            else:
                neg_sentences.add(item['sentence'].lower())

    # load all apk data
    # iterate all apps, if an app contains sentences in train/val
    # pick up the app, but need to make sure the app's id does not show
    # in test document level data

    with open(app_desc_filepath) as ifile:
        all_apks_desc = json.load(ifile)
    with open(perm_dict_filepath) as ifile:
        perm_dict = json.load(ifile)

    train_val_doc_level = []
    for apkname in all_apks_desc:
        desc = all_apks_desc[apkname]
        sentences = [s.lower() for s in desc]
        sents_set = set(sentences)

        # only saving apps at least contains 1 positive sentence for training
        # because so many negatives
        intersec = sents_set.intersection(pos_sentences)
        appid = apkname[:-4]
        if bool(intersec) and (appid not in test_doc_level_dict):
            # the app contains sentences in our training&validation
            # make sure the app does not included in test
            formated_one = format_one_app(appid, perm_dict,
                                          consider_perm, app_type,
                                          sentences, pos_sentences)
            train_val_doc_level.append(formated_one)

    print('apps for training and validation:', len(train_val_doc_level))
    # write out file
    out_dir = dirname(sent_level_filepath)
    ofilepath = join(out_dir, consider_perm+'.reformated_train_val.json')
    with open(ofilepath, 'w') as ofile:
        json.dump(train_val_doc_level, ofile, indent=2)


if __name__ == '__main__':
    main()
