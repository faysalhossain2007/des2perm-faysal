import json
import argparse


def main():
    """"""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('input_file', type=str,
                            help="the data file needs be checked")
    arg_parser.add_argument('output_file', type=str,
                            help='file removed inconsistences')

    args = arg_parser.parse_args()
    print('cheking file', args.input_file)

    with open(args.input_file) as ifile:
        data = json.load(ifile)

        sent_dict = {}

        num = 0
        incons_sents = set()
        for item in data:
            sentence = item['sentence']
            sentence = sentence.lower()

            label = item['label']
            if sentence in sent_dict:
                label_exist = sent_dict[sentence]
                if label_exist == label:
                    pass
                else:
                    print('found inconsistence of sentence {}'.format(
                        sentence))
                    num += 1
                    incons_sents.add(sentence)

            sent_dict[sentence] = label
        print('total inconsistent sentences', num)

        cons_items = []
        added_sent = set()
        for item in data:
            sentence = item['sentence']
            sentence = sentence.lower()
            if sentence not in incons_sents and sentence not in added_sent:
                cons_items.append(item)
                added_sent.add(sentence)
        # write out
        with open(args.output_file, 'w') as ofile:
            json.dump(cons_items, ofile, indent=2)


if __name__ == '__main__':
    main()
