""" access coarse location has applications
later crawled for keywords weather
"""
import json
from permission_matching import android_desc2perm_string


def trans_perm_desc(permissions):
    sys_str_perm = []
    for p in permissions:
        if p['permission'] in android_desc2perm_string:
            sys_str_perm.append(android_desc2perm_string[p['permission']])
    return sys_str_perm


def format_one_app(app,
                   consider_perm,
                   app_type,
                   sentences,
                   pos_sentence_set):

    appid = app['appId']
    # default manual label 0
    # check autocog prediction results later
    formated_sentences = []
    for s in sentences:
        s_item = {
            's': s,
            'manual-eval': 0,
            'autocog': -1
            }
        if s in pos_sentence_set:
            s_item['manual-eval'] = 1
        formated_sentences.append(s_item)

    # look up permissions
    permissions = trans_perm_desc(app['permissions'])

    formated_app = {
        'appid': appid,
        'apptype': app_type,
        'consider_perm': consider_perm,
        'permissions': permissions,
        'sentences': formated_sentences
        }
    return formated_app


def main():
    """"""
    consider_perm = 'android.permission.ACCESS_COARSE_LOCATION'
    later_crawled_data = "../../../data_annotation/android_newly_crawled/weather_app/reformated.json"
    # test_data = "../../data/test-data/android-domain/ACCESS_COARSE_LOCATION.for_eval.json"
    sent_level_filepath = "../../data/train-val-data/android-domain/access-coarse-location/added-pos-trunks/android_access_coarse_loc_added_pos_trunks.json"

    app_type = 'android-app'

    # load all later crawled data
    with open(later_crawled_data) as ifile:
        later_crawled_data = json.load(ifile)
    # load all sentence level data
    # where all later crawled descriptions should be labeled
    with open(sent_level_filepath) as ifile:
        sent_level_data = json.load(ifile)
    # load test data
    # don't need this step, because test APPs are selected from 49183 data set

    # categorize sentence level data into positive and negatives
    pos_sentences = set()
    neg_sentences = set()
    for item in sent_level_data:
        label = int(item['label'])
        if label == 1:
            pos_sentences.add(item['sentence'].lower())
        else:
            neg_sentences.add(item['sentence'].lower())

    new_crawled_patches = []
    # iterate all later crawled
    for app in later_crawled_data:
        descriptions = app['description']
        sentences = [s.lower() for s in descriptions]
        sents_set = set(sentences)

        intersec = sents_set.intersection(pos_sentences)
        if bool(intersec):
            """"""
            formated_one = format_one_app(app, consider_perm,
                                          app_type, sentences,
                                          pos_sentences)
            new_crawled_patches.append(formated_one)

    # patch it
    # need_patched_file = "../../data/train-val-data/android-domain/access-coarse-location/android.permission.ACCESS_COARSE_LOCATION.reformated_train_val.json"
    need_patched_file = '../../data/train-val-data/android-domain/access-coarse-location/only_weather.json'

    with open(need_patched_file) as ifile:
        need_patched_data = json.load(ifile)

    with open(need_patched_file+'.patched', 'w') as ofile:
        json.dump(need_patched_data + new_crawled_patches, ofile, indent=2)

    print('patched apps:', len(new_crawled_patches))
    print('pos sent size:', len(pos_sentences))

    non_repeated_pos = set()
    for app in need_patched_data + new_crawled_patches:
        sentences = app['sentences']
        for s in sentences:
            if int(s['manual-eval']) == 1:
                non_repeated_pos.add(s['s'])

    print(len(non_repeated_pos))


if __name__ == '__main__':
    main()
