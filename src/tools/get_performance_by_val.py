import json
import numpy as np
import sys


def cor_test(val, test):
    """ get the performance on test set based on the max
    val performance we can get
    """
    idx_max = np.argmax(val)
    return test[idx_max], idx_max


def performance_for(data):
    """"""
    eval_metrics = ['acc', 'prec', 'recall', 'f1']
    eval_level = ['doc', 'sent']

    rtn = {}
    for l in eval_level:
        val_data = data['val_{}_performances'.format(l)]
        test_data = data['test_{}_performances'.format(l)]

        for m in eval_metrics:
            v = val_data[m]
            t = test_data[m]
            test_score, idx = cor_test(v, t)
            rtn['test-{}-{}-epoch-{}'.format(l, m, idx)] = test_score
    return rtn


def printBestPerformance(logfile):
    """"""
    with open(logfile) as ifile:
        for line in ifile:
            exp_log = json.loads(line)
            best_test = performance_for(exp_log)
            logname = exp_log['logname']
            print('---- logname:', logname)
            print(best_test)
            print('----\n')
            return best_test


def main():
    """"""
    perform_log = sys.argv[1]

    printBestPerformance(perform_log)


if __name__ == '__main__':
    main()
