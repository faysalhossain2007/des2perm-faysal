import argparse
import json


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('actlearn_output',
                            help='outputs from active learning',
                            type=str)
    arg_parser.add_argument('permission',
                            help='permission to label',
                            type=str)
    arg_parser.add_argument('output_filepath',
                            help='write to file',
                            type=str)

    args = arg_parser.parse_args()

    print('reformating file', args.actlearn_output)

    with open(args.actlearn_output, 'r') as ifile:
        data = json.load(ifile)

        reformated = []
        for sentence in data:
            item = {
                'sentence': sentence,
                'permission': args.permission,
                'label': 0
            }
            reformated.append(item)

    with open(args.output_filepath, 'w') as ofile:
        json.dump(reformated, ofile, indent=2)


if __name__ == '__main__':
    main()
