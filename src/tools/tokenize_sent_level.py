import json
import argparse
import spacy


def main():
    """
    """
    nlp = spacy.load('en_core_web_sm')

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--in-file', required=True, type=str)
    arg_parser.add_argument('--out-file', required=True, type=str)

    args = arg_parser.parse_args()

    with open(args.in_file) as ifile:
        data = json.load(ifile)

        for item in data:
            sent = item['sentence']
            tokens = nlp(sent)
            token_txt = []
            for t in tokens:
                token_txt.append(t.text)
            item['sentence'] = ' '.join(token_txt)

    with open(args.out_file, 'w') as ofile:
        json.dump(data, ofile, indent=2)


if __name__ == "__main__":
    main()
