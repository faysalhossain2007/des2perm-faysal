import json

def main():
    """"""
    ifilename = "../data/whyper_combined_record_audio.txt"
    ofilename = ifilename + '.transformed.json'
    permission = 'record audio'

    with open(ifilename, 'r', encoding='utf-8') as ifile:
        data = json.load(ifile)
        transformed = []
        for item in data:
            sentence = item[1]
            label = item[0]
            new_format = {'sentence': sentence,
                          'permission': permission,
                          'label': str(label),
                          "agreement": None,
                          "confidence": None}
            transformed.append(new_format)

    with open(ofilename, 'w', encoding='utf-8') as ofile:
        ostr = json.dumps(transformed, indent=2)
        ofile.write(ostr)


if __name__ == '__main__':
    main()
