import json
import time
import torch
import sys
sys.path.append('../common')
from MLP_model import MultiLabelMLP
from utils import BoW_encoder

from sklearn.metrics import (accuracy_score, precision_score,
                             recall_score, f1_score)


def eval_metrics(y_true, y_pred):
    acc = accuracy_score(y_true, y_pred)
    prec = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    f1 = f1_score(y_true, y_pred)
    return {'acc': acc, 'prec': prec,
            'recall': recall, 'f1': f1}


def load_data(data_path, check_perm):
    with open(data_path) as ifile:
        data = json.load(ifile)

    req_perm_doc_level_y_true = []
    req_perm_sent_level_y_true = []
    no_perm_doc_level_y_true = []
    no_perm_sent_level_y_true = []
    req_perm_apps_sentences = []
    no_perm_apps_sentences = []
    req_perm_doc_level_y_autocog = []
    no_perm_doc_level_y_autocog = []
    req_perm_sent_level_y_autocog = []
    no_perm_sent_level_y_autocog = []

    for idx, app in enumerate(data):
        sentences = app['sentences']
        # perm_stat = app['perm']
        permissions = app['permissions']
        doc_y_manual = 0
        doc_y_autocog = 0
        app_sentences = []
        for s_item in sentences:
            s_manual_label = int(s_item['manual-eval'])
            s_autocog_label = int(s_item['autocog'])
            app_sentences.append(s_item['s'])
            if s_manual_label == 1:
                doc_y_manual = 1
            if s_autocog_label == 1:
                doc_y_autocog = 1

            # if perm_stat.startswith('require'):
            if check_perm in permissions:
                req_perm_sent_level_y_true.append(s_manual_label)
                req_perm_sent_level_y_autocog.append(s_autocog_label)
            else:
                no_perm_sent_level_y_true.append(s_manual_label)
                no_perm_sent_level_y_autocog.append(s_autocog_label)

        # if perm_stat.startswith('require'):
        if check_perm in permissions:
            req_perm_doc_level_y_true.append(doc_y_manual)
            req_perm_doc_level_y_autocog.append(doc_y_autocog)
            req_perm_apps_sentences.append(app_sentences)
        else:
            no_perm_doc_level_y_true.append(doc_y_manual)
            no_perm_doc_level_y_autocog.append(doc_y_autocog)
            no_perm_apps_sentences.append(app_sentences)

    return (req_perm_apps_sentences,
            no_perm_apps_sentences,
            req_perm_doc_level_y_true,
            no_perm_doc_level_y_true,
            req_perm_sent_level_y_true,
            no_perm_sent_level_y_true,
            req_perm_doc_level_y_autocog,
            no_perm_doc_level_y_autocog,
            req_perm_sent_level_y_autocog,
            no_perm_sent_level_y_autocog)


def load_model(model_path, dropout, device):

    model_weights = torch.load(model_path, map_location='cpu')
    net_struct = [300, 1000, 100, 2]

    model = MultiLabelMLP(net_struct, dropout)
    model.load_state_dict(model_weights)
    model.to(device)
    return model


def eval_one_app(sentences, model, wv_mode, device,
                 y_trues=None, verbose=False, autocog_pred=None):
    """ do prediction on sentences belong to one app
    return both document level prediction result and
    sentence level prediction
    """
    sent_embeddings = BoW_encoder(sentences,
                                  embedding_model=wv_mode)
    sent_embeddings = torch.tensor(sent_embeddings).to(device)

    with torch.no_grad():
        preds = model(sent_embeddings)
        sent_level_labels = preds.max(dim=1)[1].cpu().numpy()

    if 1 in sent_level_labels:
        doc_level_y_pred = 1
    else:
        doc_level_y_pred = 0

    if y_trues and verbose:
        for idx, i in enumerate(sent_level_labels):
            if i != y_trues[idx]:
                print('pred: {}; manual: {}'.format(
                    i, y_trues[idx]), sentences[idx])

    if autocog_pred and verbose:
        for idx, i in enumerate(autocog_pred):
            if i != y_trues[idx] and y_trues[idx] == sent_level_labels[idx]:
                print('-- autocog: {}; pred: {}; manual: {}; {}'.format(
                    i, sent_level_labels[idx], y_trues[idx], sentences[idx]))

    return doc_level_y_pred, sent_level_labels.tolist()


def main():
    """"""
    start_time = time.time()
    device = torch.device('cuda:0')
    labeled_data_path = "../../data/document-level-data/android-domain/ACCESS_FINE_LOCATION.for_eval.json"
    ck_perm = "android.permission.ACCESS_FINE_LOCATION"
    wv_model_path = "../../data/wv/android_w2v.kv"
    model_path = '../../data/exp/fineloc/added_pos_trunks/ad300_neg3_dropout0.3_BoW2/model.pickle'
    verbose_eval = True
    dropout = 0.3

    model = load_model(model_path, dropout, device)

    labeled_data = load_data(labeled_data_path, ck_perm)
    Apps_req_perm = labeled_data[0]
    Apps_no_perm = labeled_data[1]
    req_perm_doc_level_y_true = labeled_data[2]
    no_perm_doc_level_y_true = labeled_data[3]
    req_perm_sent_level_y_true = labeled_data[4]
    no_perm_sent_level_y_true = labeled_data[5]
    # req_perm_doc_level_y_autocog = labeled_data[6]
    # no_perm_doc_level_y_autocog = labeled_data[7]
    req_perm_sent_level_y_autocog = labeled_data[8]
    no_perm_sent_level_y_autocog = labeled_data[9]

    # print for debug
    print('app has perm:', len(Apps_req_perm))
    print('app no perm:', len(Apps_no_perm))

    req_perm_doc_level_y_pred = []
    req_perm_sent_level_y_pred = []
    for sentences in Apps_req_perm:
        start_idx = len(req_perm_sent_level_y_pred)
        n = len(sentences)
        sent_y_true = req_perm_sent_level_y_true[start_idx: start_idx+n]
        sent_y_autocog = req_perm_sent_level_y_autocog[start_idx: start_idx+n]
        doc_y_pred, sent_y_pred = eval_one_app(sentences,
                                               model,
                                               wv_model_path,
                                               device,
                                               y_trues=sent_y_true,
                                               verbose=verbose_eval,
                                               autocog_pred=sent_y_autocog)
        req_perm_doc_level_y_pred.append(doc_y_pred)
        req_perm_sent_level_y_pred.extend(sent_y_pred)

    no_perm_doc_level_y_pred = []
    no_perm_sent_level_y_pred = []
    for sentences in Apps_no_perm:
        start_idx = len(no_perm_sent_level_y_pred)
        n = len(sentences)
        sent_y_true = no_perm_sent_level_y_true[start_idx: start_idx+n]
        sent_y_autocog = no_perm_sent_level_y_autocog[start_idx: start_idx+n]
        doc_y_pred, sent_y_pred = eval_one_app(sentences,
                                               model,
                                               wv_model_path,
                                               device,
                                               y_trues=sent_y_true,
                                               verbose=verbose_eval,
                                               autocog_pred=sent_y_autocog)
        no_perm_doc_level_y_pred.append(doc_y_pred)
        no_perm_sent_level_y_pred.extend(sent_y_pred)

    # print out
    print('== eval on both req-perm and no-perm')
    doc_eval_results = eval_metrics(
        (req_perm_doc_level_y_true + no_perm_doc_level_y_true),
        (req_perm_doc_level_y_pred + no_perm_doc_level_y_pred))
    print('doc level:', doc_eval_results)

    sent_eval_results = eval_metrics(
        (req_perm_sent_level_y_true + no_perm_sent_level_y_true),
        (req_perm_sent_level_y_pred + no_perm_sent_level_y_pred))
    print('sent level:', sent_eval_results)
    print('-'*20)

    # on apps only require permission
    print('== eval on app req perm')
    doc_eval_results = eval_metrics(
        req_perm_doc_level_y_true, req_perm_doc_level_y_pred)
    sent_eval_results = eval_metrics(
        req_perm_sent_level_y_true, req_perm_sent_level_y_pred)
    print('doc level:', doc_eval_results)
    print('sent level:', sent_eval_results)
    print('-'*20)

    # on apps no perm requirements
    print('== eval on app no perm requirement')
    doc_eval_results = eval_metrics(
        no_perm_doc_level_y_true, no_perm_doc_level_y_pred)
    sent_eval_results = eval_metrics(
        no_perm_sent_level_y_true, no_perm_sent_level_y_pred)

    print('doc level:', doc_eval_results)
    print('sent level:', sent_eval_results)

    print('cost time {} seconds'.format(time.time() - start_time))


if __name__ == '__main__':
    main()
