import argparse
import json
import os
import random
from os.path import exists, expanduser, join


def main():
    """
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--data-file', required=True,
                            type=str)
    arg_parser.add_argument('--out-dir', required=True,
                            type=str)
    args = arg_parser.parse_args()

    # expanduser
    args.data_file = expanduser(args.data_file)
    args.out_dir = expanduser(args.out_dir)

    if not exists(args.out_dir):
        os.makedirs(args.out_dir)

    with open(args.data_file) as ifile:
        data = json.load(ifile)

    # shuffle the data
    random.shuffle(data)

    # 10 documents for training
    train = data[:10]

    # 5 documents for validation
    val = data[10:15]

    # rest for testing
    test = data[15:]

    with open(join(args.out_dir, 'train.json'), 'w') as ofile:
        json.dump(train, ofile, indent=2)
    with open(join(args.out_dir, 'test.json'), 'w') as ofile:
        json.dump(test, ofile, indent=2)
    with open(join(args.out_dir, 'val.json'), 'w') as ofile:
        json.dump(val, ofile, indent=2)


if __name__ == "__main__":
    main()
