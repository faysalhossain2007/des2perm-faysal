import sys
import numpy as np
import os
import json

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt


eval_metrics = ['acc', 'prec', 'recall', 'f1']


def get_performance_log(dirname, log_collector):
    filename = os.path.join(dirname, 'performance_log.json')

    with open(filename) as ifile:
        for line in ifile:
            log = json.loads(line)
            logname = log['logname']
            if logname in log_collector:
                pass
            else:
                p_metric = {'acc': [], 'prec': [], 'recall': [], 'f1': []}
                log_collector[logname] = {
                    'val': {'doc': p_metric.copy(), 'sent': p_metric.copy()},
                    'test': {'doc': p_metric.copy(), 'sent': p_metric.copy()}
                    }

            for m in eval_metrics:
                log_collector[logname]['val']['doc'][m].append(
                    log['val_doc_performances'][m])
                log_collector[logname]['test']['doc'][m].append(
                    log['test_doc_performances'][m])

                log_collector[logname]['val']['sent'][m].append(
                    log['val_sent_performances'][m])
                log_collector[logname]['test']['sent'][m].append(
                    log['test_sent_performances'][m])

    return log_collector


def median_std(values):
    """"""
    median = np.median(values, axis=0)
    std = np.std(values, axis=0)
    return median, std


def process_log(logs):
    """ compute median std for each evaluation metric

    """
    processed_logs = {}
    for logname in logs:
        processed_logs[logname] = {
            'val': {'doc': {}, 'sent': {}},
            'test': {'doc': {}, 'sent': {}}
        }
        for m in eval_metrics:
            val_doc_log_values = logs[logname]['val']['doc'][m]
            test_doc_log_values = logs[logname]['test']['doc'][m]

            val_sent_log_values = logs[logname]['val']['sent'][m]
            test_sent_log_values = logs[logname]['test']['sent'][m]

            processed_logs[logname]['val']['doc'][m] = median_std(
                val_doc_log_values)
            processed_logs[logname]['test']['doc'][m] = median_std(
                test_doc_log_values)
            processed_logs[logname]['val']['sent'][m] = median_std(
                val_sent_log_values)
            processed_logs[logname]['test']['sent'][m] = median_std(
                test_sent_log_values)
    return processed_logs


def plot_fig(data, o_dir, disable_std=True):
    """ output figures to the o_dir"""
    if not os.path.exists(o_dir):
        os.makedirs(o_dir)

    for m in eval_metrics:
        ofilename = '{}.png'.format(m)

        test_sets = ['val', 'test']
        test_levels = ['doc', 'sent']

        fig = plt.figure(figsize=(15, 10))
        ax = plt.subplot(111)
        for logname in data:
            linelabel = logname
            for s in test_sets:
                for l in test_levels:
                    y = data[logname][s][l][m][0]
                    if not disable_std:
                        y_err = data[logname][s][l][m][1]
                    else:
                        y_err = np.zeros(len(y))
                    x = np.arange(len(y))
                    ax.errorbar(x, y, yerr=y_err,
                                label=linelabel+'-{}-{}'.format(s, l))
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.2,
                         box.width, box.height * 0.8])
        ax.legend(loc='upper center',
                  bbox_to_anchor=(0.5, -0.05), ncol=2)
        ax.set_xlabel('epochs')
        ax.set_ylabel(m)
        fig_path = os.path.join(o_dir, ofilename)
        fig.savefig(fig_path)
        plt.close()


def main():
    """ invoke this func"""
    odir = sys.argv[1]
    input_dirs = sys.argv[2:]

    logs = {}
    for dirname in input_dirs:
        get_performance_log(dirname, logs)

    processed_data = process_log(logs)

    plot_fig(processed_data, odir)


if __name__ == '__main__':
    main()
