import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys
from importlib import import_module

def main():
    data_modulename = sys.argv[1]
    ofilename = sys.argv[2]

    exp_results = import_module(data_modulename)

    for line in exp_results.datas:
        label = line['label']
        data_str = line['data'].strip('\n').split('\n')
        exp_datas = []
        for i in data_str:
            i = i.strip('\n').strip()
            if i == "":
                continue
            row = np.fromstring(i.strip('\n').strip(), sep=',', dtype=float)
            exp_datas.append(row)

        exp_datas = np.array(exp_datas)
        y = np.median(exp_datas, axis=0)
        # y = np.mean(exp_datas, axis=0)
        x = np.arange(len(y))
        std_err = np.std(exp_datas, axis=0)
        # std_err = 0
        _ = plt.errorbar(x, y, yerr=std_err, label=label)

    plt.legend()
    plt.xlabel('epochs')
    plt.ylabel('accuracy')
    plt.title(exp_results.graph_title)

    # plt.show()
    plt.savefig(ofilename)

if __name__ == '__main__':
    main()



