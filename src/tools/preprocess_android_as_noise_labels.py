import json
import argparse
from os.path import expanduser, join
import os
import spacy
import random
from tqdm import tqdm
from copy import deepcopy
from sklearn.model_selection import train_test_split


random.seed(1)

def main():
    """"""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        '--desc-file',
        default="~/Dev/Whyper/data/49183_apps_desctext.json")
    arg_parser.add_argument(
        '--perm-file',
        default="~/Dev/Whyper/data/49183_apps_permdict.json")
    arg_parser.add_argument('--consider-perm', required=True, type=str)
    arg_parser.add_argument('--out-dir', required=True, type=str)

    args = arg_parser.parse_args()

    args.desc_file = expanduser(args.desc_file)
    args.perm_file = expanduser(args.perm_file)
    args.out_file = expanduser(args.out_dir)

    with open(args.desc_file) as ifile:
        desc_data = json.load(ifile)
    with open(args.perm_file) as ifile:
        perm_data = json.load(ifile)

    nlp = spacy.load('en_core_web_sm')

    consider_pos_apps = set()
    for apk in perm_data:
        required_perms = perm_data[apk]
        if args.consider_perm in required_perms:
            consider_pos_apps.add(apk)
    # sample other apks as negatives
    all_apks = set(perm_data.keys())
    pot_neg_apks = all_apks - consider_pos_apps
    consider_neg_apks = random.sample(list(pot_neg_apks),
                                      len(consider_pos_apps))

    sent_split_output_data = []
    sent_agg_output_data = []

    # record potential positives
    consider_pos_apps = list(consider_pos_apps)
    for apk in tqdm(consider_pos_apps):
        if apk in desc_data:
            desc = desc_data[apk]
            split_desc, agg_desc = process_desc(
                args, nlp, apk, perm_data[apk],
                desc, 1)
            sent_split_output_data.append(split_desc)
            sent_agg_output_data.append(agg_desc)

    # record potential negatives
    for apk in tqdm(consider_neg_apks):
        if apk in desc_data:
            desc = desc_data[apk]
            split_desc, agg_desc = process_desc(
                args, nlp, apk, perm_data[apk],
                desc, 0)
            sent_split_output_data.append(split_desc)
            sent_agg_output_data.append(agg_desc)

    # splitting into train, val, test
    train_ratio = 0.8
    val_ratio = 0.1
    test_ratio = 0.1
    train_sent_split, test_sent_split,\
        train_sent_agg, test_sent_agg = train_test_split(
            sent_split_output_data, sent_agg_output_data,
            test_size=test_ratio, random_state=1)

    train_sent_split, val_sent_split, \
        train_sent_agg, val_sent_agg = train_test_split(
            train_sent_split, train_sent_agg,
            test_size=(val_ratio/(val_ratio+train_ratio)),
            random_state=1)

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)
    # output desc sentence split
    with open(join(args.out_dir, 'train.split.json'), 'w') as ofile:
        json.dump(train_sent_split, ofile, indent=2)

    with open(join(args.out_dir, 'val.split.json'), 'w') as ofile:
        json.dump(val_sent_split, ofile, indent=2)

    with open(join(args.out_dir, 'test.split.json'), 'w') as ofile:
        json.dump(test_sent_split, ofile, indent=2)

    # output desc agg
    with open(join(args.out_dir, 'train.agg.json'), 'w') as ofile:
        json.dump(train_sent_agg, ofile, indent=2)
    with open(join(args.out_dir, 'val.agg.json'), 'w') as ofile:
        json.dump(val_sent_agg, ofile, indent=2)
    with open(join(args.out_dir, 'test.agg.json'), 'w') as ofile:
        json.dump(test_sent_agg, ofile, indent=2)


def process_desc(args, nlp_model, apk, permissions, desc, label):

    doc_item = {
        'app': apk,
        'appid': apk,
        'permissions': permissions,
        'consider_perm': args.consider_perm,
        'apptype': 'android'
        }
    tokenized_desc = []
    for s in desc:
        s_tokens = nlp_model(s)
        s_token_txt = []
        for t in s_tokens:
            s_token_txt.append(t.text)
        tokenized_desc.append(' '.join(s_token_txt))

    agg_desc = ' . '.join(tokenized_desc)
    agg_doc_item = deepcopy(doc_item)
    agg_doc_item['sentences'] = [
        {
            's': agg_desc,
            'manual-eval': label
        }
        ]

    for idx, s in enumerate(tokenized_desc):
        tokenized_desc[idx] = {
            's': s,
            'manual-eval': label
            }
    doc_item['sentences'] = tokenized_desc

    return doc_item, agg_doc_item


if __name__ == "__main__":
    main()
