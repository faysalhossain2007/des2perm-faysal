"""
feed failure cases of training log output
first store failure output lines into a file
feed that filename as command parameter 
"""
import json
import sys

def main():
    """"""
    input_files = sys.argv[1:]
    failure_sets = []

    for fname in input_files:
        sentences = {}
        with open(fname, 'r') as ifile:
            for line in ifile:
                item = json.loads(line)
                sentences[item['sentence']] = {'prediction':item['prediction'],
                                               'human-label':item['human-label']}
        failure_sets.append(sentences)

    # compute overlaps
    for idx, s in enumerate(failure_sets):
        fname = input_files[idx]
        for j in range(0, len(input_files)):
            if j == idx:
                continue
            c_fname = input_files[j]
            c_set = failure_sets[j]
            removed_sent = []
            for sentence in s:
                if sentence in c_set:
                    pass
                else:
                    removed_sent.append((sentence, s[sentence]))
            print('='*10, 'start')
            print('In', fname, 'Not In', c_fname, json.dumps(removed_sent, indent=2))
            print('-'*10, 'end')


if __name__ == '__main__':
    main()
