import json

def main():
    """"""
    ifilename = "../data/ifttt_fb_action.json"
    ofilename = ifilename + '.transformed.json'

    with open(ifilename, 'r', encoding='utf-8') as ifile:
        data = json.load(ifile)
        transformed = []
        for item in data:
            sentence = item['recipe_title'] + '. ' + item['recipe_desc']
            item['sentence'] = sentence
            item['permission'] = '{}({})'.format(item['perm_title'],
                                                 item['perm_type'])
            item['agreement'] = None
            item['confidence'] = None

            transformed.append(item)

    with open(ofilename, 'w', encoding='utf-8') as ofile:
        ostr = json.dumps(transformed, indent=2)
        ofile.write(ostr)


if __name__ == '__main__':
    main()
