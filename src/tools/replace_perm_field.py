import json


def main():
    """"""
    ifilename = "../../data/document-level-data/android-domain/ACCESS_COARSE_LOCATION.for_eval.json"
    ofilename = ifilename + '.replaced_perm'

    # data files check the permission list
    perm_dict_file = "../../../data_annotation/49183_apps_permdict.json"
    with open(perm_dict_file, 'r') as ifile:
        perm_dict = json.load(ifile)

    with open(ifilename, 'r') as ifile:
        apps_weneed = json.load(ifile)

    for app in apps_weneed:
        appname = app['appname']
        apkname = appname + '.apk'
        permissions = perm_dict[apkname]
        del app['perm']
        app['permissions'] = permissions

    # write out replaced
    with open(ofilename, 'w') as ofile:
        json.dump(apps_weneed, ofile, indent=2)


if __name__ == "__main__":
    main()
