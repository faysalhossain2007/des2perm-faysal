import json


def main():
    """"""
    ifilename = "../../data/document-level-data/android-domain/ACCESS_FINE_LOCATION.for_eval.json"
    added_type = 'android-app'
    ofilename = ifilename + '.added_app_type'

    with open(ifilename) as ifile:
        data = json.load(ifile)

    for item in data:
        item['apptype'] = added_type

    with open(ofilename, 'w') as ofile:
        json.dump(data, ofile, indent=2)


if __name__ == '__main__':
    main()
