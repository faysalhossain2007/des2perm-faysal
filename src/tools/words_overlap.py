import sys
import json
import spacy


def check_overlap(corpus1, corpus2):
    """
    :type corpus1: string
    :param corpus1: file path of the corpus 1
    :type corpus2: string
    :param corpus2: file path of the corpus 2

    """
    with open(corpus1, 'r') as ifile:
        d1 = json.load(ifile)
        c1 = compute_unigram(d1)

    with open(corpus2, 'r') as ifile:
        d2 = json.load(ifile)
        c2 = compute_unigram(d2)

    overlaps = {}
    for word in c1:
        if word in c2:
            overlaps[word] = {
                corpus1: c1[word],
                corpus2: c2[word]
            }
    return overlaps

def compute_unigram(corpus):
    """

    """
    unigram = {}
    nlp = spacy.load('en_core_web_sm')
    for item in corpus:
        sentences = item['sentence']
        doc = nlp(sentences)
        for token in doc:
            if 'NN' in token.tag_ or 'VB' in token.tag_:
                pass
            else:
                continue

            if token.lemma_ in unigram:
                unigram[token.lemma_] += 1
            else:
                unigram[token.lemma_] = 1
    return unigram


if __name__ == '__main__':
    file1 = sys.argv[1]
    file2 = sys.argv[2]
    overlaps = check_overlap(file1, file2)
    print('total overlaps:', len(overlaps))
    # ostr = json.dumps(overlaps, indent=2)
    # print(ostr)
