from .depparse2matrix import depparse, to_matrix, parse_sentences


__all__ = [depparse, to_matrix, parse_sentences]
