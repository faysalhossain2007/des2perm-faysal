import argparse
import json
import random
from os.path import expanduser, join, exists
import os

import numpy as np
from sklearn.model_selection import train_test_split


def main():
    """
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--data-file', required=True,
                            type=str)
    arg_parser.add_argument('--out-dir', required=True,
                            type=str)
    args = arg_parser.parse_args()

    # expanduser
    args.data_file = expanduser(args.data_file)
    args.out_dir = expanduser(args.out_dir)

    if not exists(args.out_dir):
        os.makedirs(args.out_dir)

    with open(args.data_file) as ifile:
        data = json.load(ifile)

    test_ratio = 0.2
    val_ratio = 0.1
    train_ratio = 0.7

    train, test = train_test_split(data, test_size=test_ratio,
                                   random_state=1)
    train, val = train_test_split(
        train,
        test_size=(val_ratio/(val_ratio + train_ratio)))

    with open(join(args.out_dir, 'train.json'), 'w') as ofile:
        json.dump(train, ofile, indent=2)
    with open(join(args.out_dir, 'test.json'), 'w') as ofile:
        json.dump(test, ofile, indent=2)
    with open(join(args.out_dir, 'val.json'), 'w') as ofile:
        json.dump(val, ofile, indent=2)

    # splitting train further
    for r in np.arange(100, len(train), 100):
        # write out
        with open(join(
                args.out_dir, 'train.{}.json'.format(r)),
                  'w') as ofile:
            json.dump(train[:r], ofile, indent=2)


if __name__ == "__main__":
    main()
