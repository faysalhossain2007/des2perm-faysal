import argparse
import json


def main():
    """"""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('data1',
                            help='path of 1st data file',
                            type=str)
    arg_parser.add_argument('data2',
                            help='path of 2nd data file',
                            type=str)
    arg_parser.add_argument('output_path',
                            help='path for destination file',
                            type=str)
    args = arg_parser.parse_args()

    with open(args.data1, 'r') as ifile:
        data1 = json.load(ifile)
    with open(args.data2, 'r') as ifile:
        data2 = json.load(ifile)

    concated = data1 + data2
    with open(args.output_path, 'w') as ofile:
        json.dump(concated, ofile, indent=2)


if __name__ == '__main__':
    main()
