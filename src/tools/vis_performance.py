import sys

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import sys
from os.path import join, exists
from os import makedirs
import json


eval_metrics = ['acc', 'prec', 'recall', 'f1']


def get_performance_log(dirname, log_collector):
    filename = join(dirname, 'performance_log.json')
    with open(filename) as ifile:
        for line in ifile:
            log = json.loads(line)
            logname = log['logname']
            if logname in log_collector:
                for m in eval_metrics:
                    log_collector[logname][m].append(log['performances'][m])
            else:
                log_collector[logname] = {}
                for m in eval_metrics:
                    log_collector[logname][m] = [log['performances'][m]]

    return log_collector


def median_std(values):
    median = np.median(values, axis=0)
    std = np.std(values, axis=0)
    return median, std


def process_logs(logs):
    """
    array contains multiple logs
    """
    # assume at least contains one log

    processed_logs = {}
    for logname in logs:
        processed_logs[logname] = {}
        for m in eval_metrics:
            log_values = logs[logname][m]
            processed_logs[logname][m] = median_std(log_values)

    return processed_logs


def plot_fig(lines, o_dir, ofilename, fig_title):
    """
    :param metric: acc, prec, recall, f1
    """
    o_dir = join(o_dir, ofilename)
    if not exists(o_dir):
        makedirs(o_dir)

    for m in eval_metrics:
        fig_name = '{}.png'.format(m)
        fig = plt.figure(figsize=(15, 10))
        ax = plt.subplot(111)
        for logname in lines:
            label = logname
            y = lines[logname][m][0]
            y_err = lines[logname][m][1]
            x = np.arange(len(y))

            ax.errorbar(x, y, yerr=y_err, label=label)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.2,
                         box.width, box.height * 0.8])
        ax.legend(loc='upper center',
                  bbox_to_anchor=(0.5, -0.05), ncol=2)
        ax.set_xlabel('epochs')
        ax.set_ylabel(m)
        ax.set_title(fig_title)
        fig_path = join(o_dir, fig_name)
        fig.savefig(fig_path)
        plt.close()


def main():
    """"""
    o_fig_name = sys.argv[1]
    o_dir = sys.argv[2]
    fig_title = sys.argv[3]
    input_dirs = sys.argv[4:]

    logs = {}
    for dirname in input_dirs:
        get_performance_log(dirname, logs)

    lines = process_logs(logs)

    # plotting
    plot_fig(lines, o_dir, o_fig_name, fig_title)


if __name__ == '__main__':
    main()
