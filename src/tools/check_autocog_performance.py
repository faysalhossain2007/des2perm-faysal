"""
performance check on document level
and sentence level, based on manually labeled data

"""

import json
from sklearn.metrics import (accuracy_score, precision_score,
                             recall_score, f1_score)


def eval_metrics(y_true, y_pred):
    acc = accuracy_score(y_true, y_pred)
    prec = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    f1 = f1_score(y_true, y_pred)
    return {'acc': acc, 'prec': prec,
            'recall': recall, 'f1': f1}


def main():
    """"""
    labeled_file = "../../data/document-level-data/android-domain/ACCESS_COARSE_LOCATION.for_eval.json"
    check_perm = 'android.permission.ACCESS_COARSE_LOCATION'

    with open(labeled_file, 'r') as ifile:
        labeled_data = json.load(ifile)

    # evaluation on document level
    req_perm_doc_level_y_autocog = []
    req_perm_doc_level_y_manual = []
    no_perm_doc_level_y_autocog = []
    no_perm_doc_level_y_manual = []
    for idx, app in enumerate(labeled_data):
        sentences = app['sentences']
        # perm_stat = app['perm']
        permissions = app['permissions']
        doc_y_autocog = 0
        doc_y_manual = 0
        for s_item in sentences:
            s_autocog_label = int(s_item['autocog'])
            s_manual_label = int(s_item['manual-eval'])
            if s_autocog_label == 1:
                doc_y_autocog = 1
            if s_manual_label == 1:
                doc_y_manual = 1
        # if perm_stat.startswith('require'):
        if check_perm in permissions:
            req_perm_doc_level_y_autocog.append(doc_y_autocog)
            req_perm_doc_level_y_manual.append(doc_y_manual)
        else:
            no_perm_doc_level_y_autocog.append(doc_y_autocog)
            no_perm_doc_level_y_manual.append(doc_y_manual)
    # debug print
    print('apps has perm:', len(req_perm_doc_level_y_manual))
    print('apps has perm:', len(no_perm_doc_level_y_manual))

    # evaluation on sentence level
    req_perm_sent_level_y_autocog = []
    req_perm_sent_level_y_manual = []
    no_perm_sent_level_y_autocog = []
    no_perm_sent_level_y_manual = []
    for app in labeled_data:
        sentences = app['sentences']
        # perm_stat = app['perm']
        permissions = app['permissions']

        for s_item in sentences:
            autocog_label = s_item['autocog']
            manual_label = s_item['manual-eval']
            # if perm_stat.startswith('require'):
            if check_perm in permissions:
                req_perm_sent_level_y_autocog.append(autocog_label)
                req_perm_sent_level_y_manual.append(manual_label)
            else:
                no_perm_sent_level_y_autocog.append(autocog_label)
                no_perm_sent_level_y_manual.append(manual_label)

    # print
    print('== eval on both app requires perm and no perm')
    doc_eval_results = eval_metrics(
        (req_perm_doc_level_y_manual +
         no_perm_doc_level_y_manual),
        (req_perm_doc_level_y_autocog +
         no_perm_doc_level_y_autocog))
    print('doc level:', doc_eval_results)
    # print
    sent_eval_results = eval_metrics(
        (req_perm_sent_level_y_manual +
         no_perm_sent_level_y_manual),
        (req_perm_sent_level_y_autocog +
         no_perm_sent_level_y_autocog))
    print('sent level:', sent_eval_results)
    print('='*20)

    # eval on apps that only require perm
    print('== only on apps require perm')
    doc_eval_results = eval_metrics(
        req_perm_doc_level_y_manual,
        req_perm_doc_level_y_autocog)

    print('doc-level:', doc_eval_results)
    # print
    sent_eval_results = eval_metrics(
        req_perm_sent_level_y_manual,
        req_perm_sent_level_y_autocog)
    print('sent level:', sent_eval_results)

    print('='*20)

    # eval on app no perm requirements
    print('== only apps no perm requirement')
    doc_eval_results = eval_metrics(
        no_perm_doc_level_y_manual,
        no_perm_doc_level_y_autocog)
    print('doc-level:', doc_eval_results)

    sent_eval_results = eval_metrics(
        no_perm_sent_level_y_manual,
        no_perm_sent_level_y_autocog)
    print('sent level:', sent_eval_results)


if __name__ == '__main__':
    main()
