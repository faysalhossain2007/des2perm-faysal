import json
from os.path import join


def main():
    """"""
    # load sentence level data
    folder_name = "../../data/sentence-level-data/android-domain/access-fine-location/added-pos-trunks"
    sentence_level_files = [
        join(folder_name, 'train/pos.json'),
        join(folder_name, 'train/neg.json'),
        join(folder_name, 'test/pos.json'),
        join(folder_name, 'test/neg.json'),
        join(folder_name, 'val/pos.json'),
        join(folder_name, 'val/neg.json')
        ]
    sentence_level_data = []
    for fpath in sentence_level_files:
        with open(fpath) as ifile:
            tmp_data = json.load(ifile)
            sentence_level_data.extend(tmp_data)

    # extract all sentences in the data
    sentences_in_train_val = set()
    for item in sentence_level_data:
        s = item['sentence']
        sentences_in_train_val.add(s.lower())

    # load document level data and extract sentences
    doc_level_filepath = "../../data/document-level-data/android-domain/ACCESS_FINE_LOCATION.for_eval.json"
    with open(doc_level_filepath) as ifile:
        doc_level_data = json.load(ifile)

    sentences_in_test = set()
    for item in doc_level_data:
        sentences = item['sentences']
        for s in sentences:
            sentences_in_test.add(s['s'].lower())

    overlapping = sentences_in_test.intersection(sentences_in_train_val)

    print('overlapping sentences:', len(overlapping))


if __name__ == '__main__':
    main()
