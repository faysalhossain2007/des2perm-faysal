import requests
import numpy as np


def depparse(url, text):
    """"""
    deparse_url = url + "/?properties=" +\
        "{'annotators':'tokenize,ssplit,depparse','outputFormat':'json'}"
    response = requests.post(deparse_url, data=text)
    response = response.json()
    return response


def parse_sentences(url, sentences):
    matrixs = []
    for s in sentences:
        parsed = depparse(url, s)
        if len(parsed['sentences']) > 0:
            deptree = parsed['sentences'][0]['enhancedDependencies']
        else:
            deptree = []
        m = to_matrix(deptree)
        matrixs.append(m)
    return matrixs


def to_matrix(deptree, max_len=30):
    adjacency_m = np.zeros((max_len, max_len))
    for idx, item in enumerate(deptree):
        g = item['governor']
        d = item['dependent']
        if g < 30 and d < 30:
            adjacency_m[g][d] = 1
    return adjacency_m


def main():
    """"""
    server_url = 'http://localhost:9000'
    test_str = 'The quick brown fox jumped over the lazy dog.'

    parsed = depparse(server_url, test_str)
    deptree = parsed['sentences'][0]['enhancedDependencies']
    m = to_matrix(deptree)
    print(m)


if __name__ == "__main__":
    main()
