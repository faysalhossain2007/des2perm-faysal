from torch.utils.data import Dataset


class SentLevelData(Dataset):
    def __init__(self, idxs_data):
        self.data = idxs_data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        return self.data[idx]


def customized_collate_fn(data):
    X = []
    y = []
    for item in data:
        X.append(item[0])
        y.append(item[1])
    return X, y
