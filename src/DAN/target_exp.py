"""
Based on base configuration file
generate configurations for source domain model training and
target model training.

template base configuration is give at template_base_conf.json

"""

import argparse
import copy
import json
import os
import pickle
import random
import shutil
from collections import deque
from os.path import basename, exists, expanduser, join
from time import localtime, strftime

import numpy as np
import time
import torch
from tqdm import tqdm

from .train import train as model_train
from .utils import load_data


class Args():
    """used for passing to training function
    faking as results from arg parser
    """
    def __init__(self):
        pass


def get_base_args():
    args = Args()
    setattr(args, "fake_summary_writer", True)
    setattr(args, "train_data", "")
    setattr(args, "val_data", "")
    setattr(args, "test_data", "")
    setattr(args, "data_type", "")
    setattr(args, "processed", False)
    setattr(args, "balance_train", True)
    setattr(args, "random_state", None)
    setattr(args, "optimizer", None)
    setattr(args, "wv_path", None)
    setattr(args, "wv_dim", 300)
    setattr(args, "vocab", None)
    setattr(args, "word_dropout", None)
    setattr(args, "nn_dropout", None)
    setattr(args, "poolings", None)
    setattr(args, "freeze_embedding", None)
    setattr(args, "freeze_lower_layers", None)
    setattr(args, "hidden_layers", None)
    setattr(args, "classes", 2)
    setattr(args, "batch_size", 20)
    setattr(args, "grad_norm_limit", 3.0)
    setattr(args, "epoch", 50)
    setattr(args, "gpu", -1)
    setattr(args, "learning_rate", None)
    setattr(args, "save_model_to", None)
    setattr(args, "expdir", None)
    setattr(args, "load_pretrain", None)
    return args


def preprocess_base_conf(base_conf):
    """ expanduser"""
    # expanduser
    base_conf["base_exp_dir"] = expanduser(base_conf["base_exp_dir"])
    for source_files in base_conf["source_domains"]:
        source_files[1] = expanduser(source_files[1])
    base_conf["target_domain"][1] = expanduser(base_conf["target_domain"][1])

    # expand wv path
    for idx, wv_p in enumerate(base_conf["wv_paths"]):
        base_conf["wv_paths"][idx] = expanduser(wv_p)
    return base_conf


def process_src_data(base_conf):
    # aggregate all source data into one single
    # splitting them into train.json, val.json, test.json
    # save into base_expdir/sources/
    base_exp_dir = base_conf["base_exp_dir"]
    data_dir = join(base_exp_dir, "data/sources")
    if not exists(data_dir):
        os.makedirs(data_dir)
    # current only support sent-level data
    # assume all sentence level data
    all_src_data = []
    for data_type, data_path in base_conf["source_domains"]:
        if data_type != "sent":
            raise Exception("currently not implemented other types")
        with open(data_path) as ifile:
            data = json.load(ifile)
        all_src_data.extend(data)

    # random.shuffle data with given random state
    random.seed(base_conf["random_state"])
    random.shuffle(all_src_data)
    n = len(all_src_data)
    train_size = int(0.7 * n)
    val_size = int(0.1 * n)
    train = all_src_data[:train_size]
    val = all_src_data[train_size: train_size+val_size]
    test = all_src_data[train_size+val_size:]
    with open(join(data_dir, 'train.json'), 'w') as ofile:
        json.dump(train,
                  ofile,
                  indent=2)
    with open(join(data_dir, 'val.json'), 'w') as ofile:
        json.dump(val,
                  ofile,
                  indent=2)
    with open(join(data_dir, 'test.json'), 'w') as ofile:
        json.dump(test,
                  ofile,
                  indent=2)


def generate_src_conf(base_conf):
    """"""
    src_args_list = []
    base_exp_dir = base_conf["base_exp_dir"]
    src_model_name = base_conf["source_model_name"]
    rnd_state = base_conf["random_state"]
    epochs = base_conf["epoch"]
    # not meaningful to freeze lower layers
    for hidden_structure in base_conf["hidden_layers"]:
        for wd in base_conf["word_dropouts"]:
            for nnd in base_conf["nn_dropouts"]:
                for lr in base_conf["learning_rates"]:
                    for opt in base_conf["optimizers"]:
                        for pool in base_conf["poolings"]:
                            for freeze_e in base_conf["freeze_embeddings"]:
                                for wv_p in base_conf["wv_paths"]:
                                    for b_train in base_conf["balance_train"]:
                                        wv_name = basename(wv_p)
                                        args = get_base_args()

                                        setattr(args, "train_data", join(
                                            base_exp_dir, "data/sources/{}/train.json.p".format(wv_name)))
                                        setattr(args, "val_data", join(
                                            base_exp_dir, "data/sources/{}/val.json.p".format(wv_name)))
                                        setattr(args, "test_data", join(
                                            base_exp_dir, "data/sources/{}/test.json.p".format(wv_name)))
                                        setattr(args, "processed", True)
                                        setattr(args, "data_type", "sent")  # fixed for now
                                        setattr(args, "balance_train", b_train)
                                        setattr(args, "random_state", rnd_state)
                                        setattr(args, "optimizer", opt)
                                        setattr(args, "wv_path", wv_p)
                                        setattr(args, "word_dropout", wd)
                                        setattr(args, "nn_dropout", nnd)
                                        setattr(args, "poolings", pool)
                                        setattr(args, "freeze_embedding", freeze_e)
                                        setattr(args, "freeze_lower_layers", False)
                                        setattr(args, "wv_dim", 300)
                                        setattr(args, "hidden_layers", hidden_structure)
                                        setattr(args, "epoch", epochs)
                                        setattr(args, "device", base_conf["device"])
                                        setattr(args, "learning_rate", lr)
                                        setattr(args, "save_model_to", "tmp/" + src_model_name)
                                        setattr(args, "expdir", join(
                                            base_exp_dir, "sources"))

                                        src_args_list.append(args)
    return src_args_list


def generate_hidden_layers_conf(base_conf):
    """
    """
    hidden_layers_range = base_conf['hidden_layers']
    for idx, l in enumerate(hidden_layers_range):
        hidden_layers_range[idx] = np.arange(l[0], l[1], l[2], dtype=int).tolist()

    hidden_layers_range = deque(hidden_layers_range)

    def rec_construct(layer_confs, collector):
        """
        :type layer_confs: collections.deque
        :param collector: initialized with [[]]
        """
        if not layer_confs:
            return collector
        layer_neurons = layer_confs.popleft()
        extended_collector = []
        for pl in collector:
            for n in layer_neurons:
                extended_collector.append(pl+[n])

        return rec_construct(layer_confs, extended_collector)

    possible_structures = rec_construct(hidden_layers_range,
                                        [[]])
    return possible_structures


def process_tgt_data(base_conf):
    data_dir = join(base_conf["base_exp_dir"], "data/target")
    if not exists(data_dir):
        os.makedirs(data_dir)
    random.seed(base_conf["random_state"])
    with open(base_conf["target_domain"][1]) as ifile:
        target_data = json.load(ifile)
    random.shuffle(target_data)

    # 10 for training
    train = target_data[:10]
    # 5 for validation
    val = target_data[10:15]
    test = target_data[15:]

    with open(join(data_dir, 'train.json'), 'w') as ofile:
        json.dump(train, ofile, indent=2)
    with open(join(data_dir, 'val.json'), 'w') as ofile:
        json.dump(val, ofile, indent=2)
    with open(join(data_dir, 'test.json'), 'w') as ofile:
        json.dump(test, ofile, indent=2)


def generate_target_conf(base_conf, src_args):
    """
    """
    # process target data

    tgt_arg_list = []
    src_model_name = join(src_args.expdir, src_args.exp_time,
                          src_args.save_model_to)
    data_dir = join(base_conf["base_exp_dir"], "data/target")
    exp_dir = join(src_args.expdir, src_args.exp_time, "targets")
    tgt_model_name = base_conf["target_model_name"]
    rnd_state = base_conf["random_state"]
    epochs = base_conf["epoch"]
    for wd in base_conf["word_dropouts"]:
        for nnd in base_conf["nn_dropouts"]:
            for lr in base_conf["learning_rates"]:
                for opt in base_conf["optimizers"]:
                    for freeze_e in base_conf["freeze_embeddings"]:
                        for freeze_ll in base_conf["freeze_lower_layers"]:
                            for b_train in base_conf["balance_train"]:
                                wv_name = basename(src_args.wv_path)
                                args = get_base_args()
                                # hidden structure same as source
                                setattr(args, "train_data", join(
                                    data_dir, wv_name, "train.json.p"))
                                setattr(args, "val_data", join(
                                    data_dir, wv_name, "val.json.p"))
                                setattr(args, "test_data", join(
                                    data_dir, wv_name, "test.json.p"))
                                setattr(args, "processed", True)
                                setattr(args, "data_type", "doc")
                                setattr(args, "balance_train", b_train)
                                setattr(args, "random_state", rnd_state)
                                setattr(args, "optimizer", opt)
                                setattr(args, "wv_path", src_args.wv_path)
                                setattr(args, "word_dropout", wd)
                                setattr(args, "nn_dropout", nnd)
                                setattr(args, "poolings", src_args.poolings)
                                setattr(args, "freeze_embedding", freeze_e)
                                setattr(args, "freeze_lower_layers", freeze_ll)
                                setattr(args, "wv_dim", 300)
                                setattr(args, "hidden_layers", src_args.hidden_layers)
                                setattr(args, "epoch", epochs)
                                setattr(args, "device", src_args.device)
                                setattr(args, "learning_rate", lr)
                                setattr(args, "save_model_to", tgt_model_name)
                                setattr(args, "expdir", exp_dir)

                                # no transfer as base line
                                tgt_arg_list.append(args)

                                # transfer from previous best
                                t_args = copy.deepcopy(args)
                                setattr(t_args, "load_pretrain", src_model_name + '.best')
                                setattr(t_args, "from_src_setup", src_args.__dict__)
                                tgt_arg_list.append(t_args)

                                # transfer from lastest
                                t_args2 = copy.deepcopy(args)
                                setattr(t_args2, "load_pretrain", src_model_name + ".last")
                                setattr(t_args2, "from_src_setup", src_args.__dict__)
                                tgt_arg_list.append(t_args2)

    return tgt_arg_list


def expand_range_options(base_conf):
    """"""
    # expand word dropout options
    wd_range = base_conf["word_dropouts"]
    base_conf["word_dropouts"] = np.arange(wd_range[0], wd_range[1],
                                           wd_range[2]).tolist()
    # expand nn dropout options
    nnd_range = base_conf["nn_dropouts"]
    base_conf["nn_dropouts"] = np.arange(nnd_range[0], nnd_range[1],
                                         nnd_range[2]).tolist()

    lr_range = base_conf["learning_rates"]
    base_conf["learning_rates"] = np.arange(lr_range[0], lr_range[1],
                                            lr_range[2]).tolist()
    # expand possible hidden layers
    base_conf["hidden_layers"] = generate_hidden_layers_conf(base_conf)
    return base_conf


def indexing_data(base_conf):
    src_data_dir = join(base_conf["base_exp_dir"], "data/sources")
    tgt_data_dir = join(base_conf["base_exp_dir"], "data/target")

    # process source
    for wv_p in base_conf["wv_paths"]:
        wv_name = basename(wv_p)
        processed_dir = join(src_data_dir, wv_name)
        if not exists(processed_dir):
            os.makedirs(processed_dir)
        fake_args = Args()
        setattr(fake_args, "random_state", base_conf["random_state"])
        setattr(fake_args, "train_data", join(src_data_dir, 'train.json'))
        setattr(fake_args, "val_data", join(src_data_dir, "val.json"))
        setattr(fake_args, "test_data", join(src_data_dir, "test.json"))
        setattr(fake_args, "data_type", "sent")
        setattr(fake_args, "wv_path", wv_p)
        setattr(fake_args, "processed", False)
        setattr(fake_args, "balance_train", False)  # balance process will be executed later
        train, val, test = load_data(fake_args)
        with open(join(processed_dir, "train.json.p"), 'wb') as ofile:
            pickle.dump(train, ofile)
        with open(join(processed_dir, "val.json.p"), 'wb') as ofile:
            pickle.dump(val, ofile)
        with open(join(processed_dir, "test.json.p"), 'wb') as ofile:
            pickle.dump(test, ofile)

    # process target
    for wv_p in base_conf["wv_paths"]:
        wv_name = basename(wv_p)
        processed_dir = join(tgt_data_dir, wv_name)
        if not exists(processed_dir):
            os.makedirs(processed_dir)
        fake_args = Args()
        setattr(fake_args, "random_state", base_conf["random_state"])
        setattr(fake_args, "train_data", join(tgt_data_dir, 'train.json'))
        setattr(fake_args, "val_data", join(tgt_data_dir, "val.json"))
        setattr(fake_args, "test_data", join(tgt_data_dir, "test.json"))
        setattr(fake_args, "data_type", "doc")
        setattr(fake_args, "wv_path", wv_p)
        setattr(fake_args, "processed", False)
        setattr(fake_args, "balance_train", False)  # balance process will be executed later
        train, val, test = load_data(fake_args)

        with open(join(processed_dir, "train.json.p"), 'wb') as ofile:
            pickle.dump(train, ofile)
        with open(join(processed_dir, "val.json.p"), 'wb') as ofile:
            pickle.dump(val, ofile)
        with open(join(processed_dir, "test.json.p"), 'wb') as ofile:
            pickle.dump(test, ofile)


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--base-conf',
                            help="path for the base configuration file",
                            required=True)
    exp_args = arg_parser.parse_args()

    with open(expanduser(exp_args.base_conf)) as conf_file:
        base_conf = json.load(
            conf_file)

    base_conf = preprocess_base_conf(base_conf)

    # expanding ranging options
    base_conf = expand_range_options(base_conf)
    delete_tmp = base_conf["delete_tmp"]

    # aggregate source data
    process_src_data(base_conf)
    # process target data -> splitting it into train/val/test
    process_tgt_data(base_conf)
    # indexing data to reduce loading time
    indexing_data(base_conf)

    # set up the device for experiments
    if base_conf["gpu"] < 0:
        base_conf["device"] = "cpu"
    else:
        base_conf["device"] = "cuda:" + str(base_conf["gpu"])

    # training start with source model
    # for each source model run different target configuration
    src_training_configs = generate_src_conf(base_conf)
    print("total possible source configs", len(src_training_configs))
    for src_args in tqdm(src_training_configs):
        src_exp_time = strftime("%Y-%m-%d-%H:%M:%S", localtime())
        setattr(src_args, "exp_time", src_exp_time)
        # ensure tmp dir exist
        tmp_dir = join(src_args.expdir, src_exp_time, 'tmp/')
        os.makedirs(tmp_dir)

        # training source model
        model_train(src_args)
        # save source training configuration
        with open(join(src_args.expdir,
                       src_exp_time, "src_conf.json"), 'w') as ofile:
            json.dump(src_args.__dict__, ofile, indent=2)

        # after training source model, generates training configs for target
        tgt_training_configs = generate_target_conf(base_conf, src_args)
        for tgt_args in tqdm(tgt_training_configs):
            tgt_exp_time = strftime("%Y-%m-%d-%H:%M:%S", localtime())
            setattr(tgt_args, "exp_time", tgt_exp_time)

            # training target model
            model_train(tgt_args)
            # save target configuration
            with open(join(tgt_args.expdir,
                           tgt_exp_time, "tgt_conf.json"), 'w') as ofile:
                json.dump(tgt_args.__dict__, ofile, indent=2)

        if delete_tmp:
            shutil.rmtree(tmp_dir)


if __name__ == '__main__':
    main()
