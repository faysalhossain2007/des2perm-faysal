import json
import random

import torch
import torch.nn as nn
import torch.nn.functional as F

from DAN.gensimKV2TorchEmbedding import kv2np_array, END_IDX, UNK_IDX


class DAN(nn.Module):
    """ encapsule MLP and embedding layer
    """

    def __init__(self, args):
        super(DAN, self).__init__()
        torch.manual_seed(args.random_state)
        self.device = args.device
        self.w_dropout = args.word_dropout
        self.pooling_operators = args.poolings
        self.embeddings = self.init_embeddings(args)
        self.embeddings.weight.requires_grad = not args.freeze_embedding

        # set input size based on poolings
        setattr(args, 'input_size',
                len(self.pooling_operators) * args.wv_dim)
        self.mlp = DAN_MLP(args)

    def init_embeddings(self, args):
        wv_path = args.wv_path
        wv_dim = args.wv_dim
        if wv_path:
            vocab, wv_vec = kv2np_array(wv_path, args.random_state)
            vocab_size = len(vocab)
            embeddings = nn.Embedding(
                vocab_size,
                wv_dim, padding_idx=1)
            embeddings.weight.data.copy_(torch.from_numpy(wv_vec))
        else:
            # if no init wv model
            # read vocab file and create new embeddings
            # assume json file
            vocab = args.vocab
            with open(vocab) as ifile:
                vocab = json.load(ifile)
            vocab_size = len(vocab)
            embeddings = nn.Embedding(
                vocab_size, wv_dim)
        return embeddings

    def poolings(self, X, X_lens):
        """
        :param X: N x L x E

        :return: N x pE; pE = numOfPoolings * E
        """
        pools = {
            'avg': sentence_avg_pool,
            'max': sentence_max_pool
            }
        pooling_outs = []
        for p in self.pooling_operators:
            out = pools[p](X, X_lens)
            pooling_outs.append(out)

        # concate along with column
        return torch.cat(pooling_outs, 1)

    def forward(self, X):
        """
        :param X: indexed sentences
        """
        if self.training:
            X = word_dropout(X, self.w_dropout)
        # embedding lookup
        max_l = 0
        for s in X:
            if len(s) > max_l:
                max_l = len(s)
        # padding for short sentences
        X_lens = []
        for idx, s in enumerate(X):
            # may delete all words
            if not s:
                s = [UNK_IDX]
                X[idx] = s
            n = len(s)
            X_lens.append(len(s))

            if n < max_l:
                s += [END_IDX] * (max_l-n)
                X[idx] = s

        X = torch.tensor(X, dtype=torch.int64, device=self.device)
        X_lens = torch.tensor(X_lens, dtype=torch.float, device=self.device)
        X_emb = self.embeddings(X)
        # pooling
        X_emb = self.poolings(X_emb, X_lens)
        # feeding into mlp
        output = self.mlp(X_emb)

        return output


class DAN_MLP(nn.Module):
    """customized MLP for Deep Avgeraging Networks"""

    def __init__(self, args):
        super(DAN_MLP, self).__init__()
        hidden_layers = args.hidden_layers
        input_size = args.input_size
        output_size = args.classes
        nn_dropout = args.nn_dropout

        net_structure = [input_size] + hidden_layers + [output_size]
        fc_layers = []
        for i in range(len(net_structure) - 1):
            in_size = net_structure[i]
            out_size = net_structure[i+1]
            hidden_layer = nn.Linear(in_size, out_size)
            fc_layers.append(hidden_layer)

            if i != (len(net_structure) - 2):
                fc_layers.append(nn.Dropout(p=nn_dropout))

            if args.freeze_lower_layers:
                """freeze lower layers"""
                if i < len(net_structure) - 2:
                    hidden_layer.weight.requires_grad = False
                    hidden_layer.bias.requires_grad = False

        self.model = nn.Sequential(*fc_layers)

    def forward(self, X):
        """ Inputs are embeddings after pooling"""
        output = self.model(X)
        return F.log_softmax(output, dim=1)


def word_dropout(sentences, p):
    """drop words in each sentence
    :param p: the probability of dropping a word
    """
    processed = []
    for s in sentences:
        retain_words = []
        for w in s:
            r = random.random()
            if r > p:
                retain_words.append(w)
        processed.append(retain_words)

    return processed


def sentence_avg_pool(X, X_lens):
    """avg pooling on input sentence
    with certain word dropout rate
    :param X: N x L x E; N: number of sentences
              L: sentence length, E: embedding size
    :param X_lens: length of each sentence
    :return: N x E
    """
    X_sum = torch.sum(X, dim=1)
    X_avg = X_sum / X_lens.view(-1, 1)
    return X_avg


def sentence_max_pool(X, X_lens):
    """ temporal max pooling over input sentence
    :param X: N x L x E

    :return: N x E
    """
    try:
        X_max, _ = torch.max(X, dim=1)
    except Exception as e:
        print(e)
        print(X, X_lens)
        X_max = torch.tensor(0)

    return X_max
