"""
using source domain for target data selection

Based on base configuration file
generate configurations for source domain model training and
target model training.

template base configuration is give at template_base_conf.json

"""

import argparse
import copy
import json
import os
import pickle
import random
import shutil
from collections import deque
from os.path import basename, exists, expanduser, join
from time import localtime, strftime

import numpy as np
import torch
from tqdm import tqdm

from .train import train as model_train
from .utils import load_data, indexing_data, _load
from .gensimKV2TorchEmbedding import kv2np_array
from .target_exp import (Args, get_base_args, preprocess_base_conf,
                         process_src_data, generate_src_conf,
                         expand_range_options)


def data_selection(base_conf, src_model, src_args):
    """using source model to select data for training and
    validation
    :type src_model: DAN
    """
    data_dir = join(base_conf["base_exp_dir"], "data/target/ds",
                    src_args.exp_time, src_args.save_model_to)
    if not exists(data_dir):
        os.makedirs(data_dir)

    src_model.eval()
    # indexing data with src_args.wv_path
    with open(base_conf["target_domain"][1]) as ifile:
        tgt_raw = json.load(ifile)
        # because it is a array in json file they should have same order
    tgt_data = _load(base_conf["target_domain"][1], "doc", "test")

    vocab, _ = kv2np_array(src_args.wv_path, src_args.random_state)
    faked_args = Args()
    setattr(faked_args, "data_type", "doc")  # for indexing_data function
    indexed_tgt_data = indexing_data(faked_args, tgt_data, vocab, "test")

    # ranking data
    doc_ranks = []
    for d in indexed_tgt_data:
        d_X = [s[0] for s in d]
        _scores = src_model(list(d_X))
        _, _preds = torch.max(_scores, dim=1)
        doc_ranks.append(torch.sum(_preds).item())

    # split data
    sorted_idx = np.argsort(doc_ranks)[::-1]  # descending order
    sorted_idx = sorted_idx.tolist()

    train_set = []
    val_set = []
    test_set = []

    for _di in sorted_idx[:10]:
        train_set.append(tgt_raw[_di])
    for _di in sorted_idx[10:15]:
        val_set.append(tgt_raw[_di])
    for _di in sorted_idx[15:]:
        test_set.append(tgt_raw[_di])

    # save data
    with open(join(data_dir, 'train.json'), 'w') as ofile:
        json.dump(train_set, ofile, indent=2)
    with open(join(data_dir, 'val.json'), 'w') as ofile:
        json.dump(val_set, ofile, indent=2)
    with open(join(data_dir, 'test.json'), 'w') as ofile:
        json.dump(test_set, ofile, indent=2)

    # indexing those data again
    indexing_tgt_data(data_dir, src_args.wv_path, base_conf["random_state"])

    # change model status
    src_model.train()


def generate_target_conf(base_conf, src_args):
    """
    """

    tgt_arg_list = []
    src_model_name = join(src_args.expdir, src_args.exp_time,
                          src_args.save_model_to)
    data_dir = join(base_conf["base_exp_dir"], "data/target/ds",
                    src_args.exp_time, src_args.save_model_to)
    wv_name = basename(src_args.wv_path)

    exp_dir = join(src_args.expdir, src_args.exp_time, "targets")
    tgt_model_name = base_conf["target_model_name"]
    rnd_state = base_conf["random_state"]
    epochs = base_conf["epoch"]
    for wd in base_conf["word_dropouts"]:
        for nnd in base_conf["nn_dropouts"]:
            for lr in base_conf["learning_rates"]:
                for opt in base_conf["optimizers"]:
                    for freeze_e in base_conf["freeze_embeddings"]:
                        for freeze_ll in base_conf["freeze_lower_layers"]:
                            for b_train in base_conf["balance_train"]:
                                wv_name = basename(src_args.wv_path)
                                args = get_base_args()
                                # hidden structure same as source
                                setattr(args, "train_data", join(
                                    data_dir, wv_name, "train.json.p"))
                                setattr(args, "val_data", join(
                                    data_dir, wv_name, "val.json.p"))
                                setattr(args, "test_data", join(
                                    data_dir, wv_name, "test.json.p"))
                                setattr(args, "processed", True)
                                setattr(args, "data_type", "doc")
                                setattr(args, "balance_train", b_train)
                                setattr(args, "random_state", rnd_state)
                                setattr(args, "optimizer", opt)
                                setattr(args, "wv_path", src_args.wv_path)
                                setattr(args, "word_dropout", wd)
                                setattr(args, "nn_dropout", nnd)
                                setattr(args, "poolings", src_args.poolings)
                                setattr(args, "freeze_embedding", freeze_e)
                                setattr(args, "freeze_lower_layers", freeze_ll)
                                setattr(args, "wv_dim", 300)
                                setattr(args, "hidden_layers", src_args.hidden_layers)
                                setattr(args, "epoch", epochs)
                                setattr(args, "device", src_args.device)
                                setattr(args, "learning_rate", lr)
                                setattr(args, "save_model_to", tgt_model_name)
                                setattr(args, "expdir", exp_dir)

                                # no transfer as base line
                                tgt_arg_list.append(args)

                                # transfer from previous best
                                t_args = copy.deepcopy(args)
                                setattr(t_args, "load_pretrain", src_model_name + '.best')
                                setattr(t_args, "from_src_setup", src_args.__dict__)
                                tgt_arg_list.append(t_args)

                                # transfer from lastest
                                t_args2 = copy.deepcopy(args)
                                setattr(t_args2, "load_pretrain", src_model_name + ".last")
                                setattr(t_args2, "from_src_setup", src_args.__dict__)
                                tgt_arg_list.append(t_args2)

    return tgt_arg_list


def indexing_src_data(base_conf):
    src_data_dir = join(base_conf["base_exp_dir"], "data/sources")

    # process source
    for wv_p in base_conf["wv_paths"]:
        wv_name = basename(wv_p)
        processed_dir = join(src_data_dir, wv_name)
        if not exists(processed_dir):
            os.makedirs(processed_dir)
        fake_args = Args()
        setattr(fake_args, "random_state", base_conf["random_state"])
        setattr(fake_args, "train_data", join(src_data_dir, 'train.json'))
        setattr(fake_args, "val_data", join(src_data_dir, "val.json"))
        setattr(fake_args, "test_data", join(src_data_dir, "test.json"))
        setattr(fake_args, "data_type", "sent")
        setattr(fake_args, "wv_path", wv_p)
        setattr(fake_args, "processed", False)
        setattr(fake_args, "balance_train", False)  # balance process will be executed later
        train, val, test = load_data(fake_args)
        with open(join(processed_dir, "train.json.p"), 'wb') as ofile:
            pickle.dump(train, ofile)
        with open(join(processed_dir, "val.json.p"), 'wb') as ofile:
            pickle.dump(val, ofile)
        with open(join(processed_dir, "test.json.p"), 'wb') as ofile:
            pickle.dump(test, ofile)


def indexing_tgt_data(tgt_data_dir, wv_path, random_state):
    wv_name = basename(wv_path)
    processed_dir = join(tgt_data_dir, wv_name)
    if not exists(processed_dir):
        os.makedirs(processed_dir)
    fake_args = Args()
    setattr(fake_args, "random_state", random_state)
    setattr(fake_args, "train_data", join(tgt_data_dir, 'train.json'))
    setattr(fake_args, "val_data", join(tgt_data_dir, "val.json"))
    setattr(fake_args, "test_data", join(tgt_data_dir, "test.json"))
    setattr(fake_args, "data_type", "doc")
    setattr(fake_args, "wv_path", wv_path)
    setattr(fake_args, "processed", False)
    setattr(fake_args, "balance_train", False)  # balance process will be executed later
    train, val, test = load_data(fake_args)

    with open(join(processed_dir, "train.json.p"), 'wb') as ofile:
        pickle.dump(train, ofile)
    with open(join(processed_dir, "val.json.p"), 'wb') as ofile:
        pickle.dump(val, ofile)
    with open(join(processed_dir, "test.json.p"), 'wb') as ofile:
        pickle.dump(test, ofile)


def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--base-conf',
                            help="path for the base configuration file",
                            required=True)

    arg_parser.add_argument('--load-pretrain', type=str)

    exp_args = arg_parser.parse_args()

    with open(expanduser(exp_args.base_conf)) as conf_file:
        base_conf = json.load(
            conf_file)

    base_conf = preprocess_base_conf(base_conf)

    # expanding ranging options
    base_conf = expand_range_options(base_conf)
    delete_tmp = base_conf["delete_tmp"]

    # aggregate source data
    process_src_data(base_conf)
    # indexing data to reduce loading time
    indexing_src_data(base_conf)

    # set up the device for experiments
    if base_conf["gpu"] < 0:
        base_conf["device"] = "cpu"
    else:
        base_conf["device"] = "cuda:" + str(base_conf["gpu"])

    # training start with source model
    # for each source model run different target configuration
    src_training_configs = generate_src_conf(base_conf)
    print("total possible source configs", len(src_training_configs))
    for src_args in tqdm(src_training_configs):
        src_exp_time = strftime("%Y-%m-%d-%H:%M:%S", localtime())
        setattr(src_args, "exp_time", src_exp_time)
        # ensure tmp dir exist
        tmp_dir = join(src_args.expdir, src_exp_time, 'tmp/')
        os.makedirs(tmp_dir)

        # training source model
        src_model = model_train(src_args, True)
        # save source training configuration
        with open(join(src_args.expdir,
                       src_exp_time, "src_conf.json"), 'w') as ofile:
            json.dump(src_args.__dict__, ofile, indent=2)

        # after training source model, generates training configs for target
        # use source domain model do data selection
        # it will save the splitted data into
        # "<base_exp_dir>/data/target/ds/<src-model-time>"
        data_selection(base_conf, src_model, src_args)

        tgt_training_configs = generate_target_conf(base_conf, src_args)
        for tgt_args in tqdm(tgt_training_configs):
            tgt_exp_time = strftime("%Y-%m-%d-%H:%M:%S", localtime())
            setattr(tgt_args, "exp_time", tgt_exp_time)

            # training target model
            model_train(tgt_args, False)
            # save target configuration
            with open(join(tgt_args.expdir,
                           tgt_exp_time, "tgt_conf.json"), 'w') as ofile:
                json.dump(tgt_args.__dict__, ofile, indent=2)

        if delete_tmp:
            shutil.rmtree(tmp_dir)


if __name__ == '__main__':
    main()
