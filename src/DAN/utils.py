import argparse
import json
import pickle
import random
from os.path import dirname, exists
from os import makedirs

import numpy as np
import torch
from tqdm import tqdm

from .gensimKV2TorchEmbedding import UNK_IDX, kv2np_array


class Logger():
    def __init__(self, log_path):
        """"""
        self.log_path = log_path

    def log(self, info):
        """"""
        log_dir = dirname(self.log_path)
        if not exists(log_dir):
            makedirs(log_dir)
        with open(self.log_path, 'a') as ofile:
            ofile.write(str(info))
            ofile.write('\n')


def log_pf(args, e, summary_writer,
           val_pf, test_pf, best_val_f1):
    save_current_model = False
    if args.data_type == "sent":
        # comparing on sentence level
        summary_writer.add_scalar('f1/sent-val',
                                  val_pf['sent-level']['f1'], e)
        summary_writer.add_scalar('f1/sent-test',
                                  test_pf['sent-level']['f1'], e)
        if best_val_f1['sent'] > val_pf['sent-level']['f1']:
            pass
        else:
            best_val_f1['sent'] = val_pf['sent-level']['f1']
            # write out the model, new best model
            save_current_model = True
    else:
        # comparing on doc level
        summary_writer.add_scalar('f1/doc-val',
                                  val_pf['doc-level']['f1'], e)
        summary_writer.add_scalar('f1/doc-test',
                                  test_pf['doc-level']['f1'], e)
        if best_val_f1['doc'] > val_pf['doc-level']['f1']:
            pass
        else:
            best_val_f1['doc'] = val_pf['doc-level']['f1']
            # write out the model, new best model
            save_current_model = True

    return save_current_model


def indexing_data(args, data, vocab, mode):
    """ tokenize each sentence, get the index of each word
    """
    def _index_sentence(s, vocab):
        """"""
        tokens = s.split()
        w_idxs = []
        for t in tokens:
            w = t.lower().strip()
            if w in vocab:
                w_idx = vocab.index(w)
            else:
                w_idx = UNK_IDX
            w_idxs.append(w_idx)

        return w_idxs

    print('indexing {} data'.format(mode))
    if mode == 'train' or args.data_type == 'sent':
        processed = []
        for d in tqdm(data):
            sent = d[0]
            y = d[1]
            sent = _index_sentence(sent, vocab)
            processed.append((sent, y))
        return processed
    else:
        processed = []
        for doc_data in tqdm(data):
            doc_processed = []
            for d in zip(doc_data[0], doc_data[1]):
                sent = d[0]
                y = d[1]
                sent = _index_sentence(sent, vocab)
                doc_processed.append((sent, y))
            processed.append(doc_processed)
        return processed


def _load(datafile, data_type, is_train):

    X = []
    y = []
    with open(datafile) as ifile:
        data = json.load(ifile)
        for item in data:
            if data_type == 'sent':
                X.append(item['sentence'])
                y.append(int(item['label']))

            elif data_type == 'doc':
                sentences = item['sentences']
                doc_X = []
                doc_y = []
                for s in sentences:
                    doc_X.append(s['s'])
                    doc_y.append(int(s['manual-eval']))

                if is_train == 'train':
                    X.extend(doc_X)
                    y.extend(doc_y)
                else:
                    X.append(doc_X)
                    y.append(doc_y)

    return list(zip(X, y))


def load_data(args):
    """
    load train/val/test
    """

    if not args.processed:
        train_data = _load(args.train_data, args.data_type, 'train')
        val_data = _load(args.val_data, args.data_type, 'val')
        test_data = _load(args.test_data, args.data_type, 'test')

        # indexing data
        if args.wv_path:
            vocab, _ = kv2np_array(args.wv_path, args.random_state)
        else:
            with open(args.vocab) as ifile:
                vocab = json.load(ifile)
        train_data = indexing_data(args, train_data, vocab, 'train')
        val_data = indexing_data(args, val_data, vocab, 'val')
        test_data = indexing_data(args, test_data, vocab, 'test')

    else:
        train_data = pickle.load(open(args.train_data, 'rb'))
        val_data = pickle.load(open(args.val_data, 'rb'))
        test_data = pickle.load(open(args.test_data, 'rb'))

    if args.balance_train:
        """ balancing training data"""
        random.seed(args.random_state)
        train_pos = []
        train_neg = []

        for entry in train_data:
            if entry[1] > 0:
                train_pos.append(entry)
            else:
                train_neg.append(entry)
        sampled_pos = random.choices(
            train_pos, k=(len(train_neg) - len(train_pos)))
        train_data = sampled_pos + train_neg + train_pos
        random.shuffle(train_data)

    return train_data, val_data, test_data


def performance_test(args, model, device, dataset):
    # loss_fn = nn.CrossEntropyLoss()
    def eval_one(d):
        X, y = d
        pred = model([X])
        _, pred = pred.max(dim=1)
        if pred.item() == y:
            if y == 0:
                return np.array([0, 1, 0, 0])
            else:
                return np.array([1, 0, 0, 0])
        else:
            if y == 0:
                return np.array([0, 0, 1, 0])
            else:
                return np.array([0, 0, 0, 1])
    model.eval()
    with torch.no_grad():
        if args.data_type == 'sent':
            sent_level = np.zeros(4)
            for s in dataset:
                rst = eval_one(s)
                sent_level += rst

            pf = metrics(sent_level[0],
                         sent_level[1],
                         sent_level[2],
                         sent_level[3])
            performances = {'sent-level': pf,
                            'sent-tp-tn-fp-fn': sent_level.tolist()}
        else:
            # doc level
            doc_level = np.array([0, 0, 0, 0])
            allsent_level = np.array([0, 0, 0, 0])
            for d in dataset:
                doc_y = 0
                doc_p = 0
                for s in d:
                    rst = eval_one(s)
                    allsent_level += rst
                    if s[1] == 1:
                        doc_y = 1
                    if rst[0] == 1 or rst[2] == 1:
                        doc_p = 1
                # assign doc level
                if doc_y == doc_p and doc_y == 1:
                    doc_level[0] += 1
                elif doc_y == doc_p and doc_y == 0:
                    doc_level[1] += 1
                elif doc_y != doc_p and doc_y == 0:
                    doc_level[2] += 1
                else:
                    doc_level[3] += 1
            doc_pf = metrics(
                doc_level[0], doc_level[1], doc_level[2], doc_level[3])
            sent_pf = metrics(
                allsent_level[0], allsent_level[1], allsent_level[2],
                allsent_level[3])
            performances = {'sent-level': sent_pf,
                            'sent-tp-tn-fp-fn': allsent_level.tolist(),
                            'doc-level': doc_pf,
                            'doc-tp-tn-fp-fn': doc_level.tolist()}

    model.train()
    return performances


def metrics(tp, tn, fp, fn):
    """"""
    prec = float(tp) / (tp + fp + 0.0001)
    acc = float(tp + tn) / (tp + tn + fp + fn)
    recall = float(tp) / (tp + fn)
    f1 = 2 * prec * recall / (prec + recall + 0.00001)
    return {'acc': acc, 'prec': prec, 'recall': recall, 'f1': f1}


def get_default_args_parser():

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--train-data', required=True)
    arg_parser.add_argument('--val-data', required=True)
    arg_parser.add_argument('--test-data', required=True)
    arg_parser.add_argument('--data-type', help='doc | sent',
                            required=True)
    arg_parser.add_argument('--processed', default=False,
                            help='processed or not',
                            type=bool)
    arg_parser.add_argument('--balance-train', default=True, type=bool)
    arg_parser.add_argument('--random-state', default=1, type=int)
    arg_parser.add_argument('--optimizer', default='adagrad', type=str)

    arg_parser.add_argument('--wv-path',
                            help="gensim KeyedVectors file; record vocabulary")
    arg_parser.add_argument('--vocab',
                            help="vocabulary file; list of words")
    arg_parser.add_argument('--word-dropout', default=0.2, type=float)
    arg_parser.add_argument('--nn-dropout', default=0.5, type=float)
    arg_parser.add_argument('--poolings', default='avg', type=str)
    arg_parser.add_argument('--freeze-embedding', default=False, type=bool)
    arg_parser.add_argument('--freeze-lower-layers', default=False, type=bool)

    arg_parser.add_argument('--wv-dim', default=300, type=int)
    arg_parser.add_argument('--hidden-layers', default='300,300',
                            type=str)
    arg_parser.add_argument('--classes', default=2, type=int)

    arg_parser.add_argument('--batch-size', default=20, type=int)
    arg_parser.add_argument('--grad-norm-limit', default=3.0,
                            type=float)
    arg_parser.add_argument('--epoch', default=20, type=int)
    arg_parser.add_argument('--gpu', default=-1, type=int)
    arg_parser.add_argument('--learning-rate', default=0.01, type=float)

    arg_parser.add_argument('--save-model-to', default='model.pt',
                            type=str)
    arg_parser.add_argument('--expdir',
                            default="~/Dev/desc2perm/data/exp")
    arg_parser.add_argument('--load-pretrain', type=str)
    return arg_parser
