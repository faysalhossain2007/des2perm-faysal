from gensim.models import KeyedVectors
import numpy as np
from tqdm import tqdm


UNK = '<unk>'
UNK_IDX = 0
END = '<end>'
END_IDX = 1


def kv2np_array(kv_filepath, random_state):
    """ transform keyedvectors to numpy array
    and return the vocabulary to for word2idx
    """
    np.random.seed(random_state)
    kv_model = KeyedVectors.load(kv_filepath)
    model_vocab = kv_model.index2word
    # remove UNK, and END, because we want to put them at the beginning
    if UNK in model_vocab:
        _idx = model_vocab.index(UNK)
        del model_vocab[_idx]
    if END in model_vocab:
        _idx = model_vocab.index(END)
        del model_vocab[_idx]

    vector_size = kv_model.vector_size
    word_vectors = []

    vocab = [UNK, END] + model_vocab

    # place for UNK
    if UNK in kv_model:
        word_vectors.append(kv_model[UNK])
    else:
        rnd_vec = np.random.uniform(-0.5, 0.5, vector_size)
        word_vectors.append(rnd_vec)

    # place for END
    if END in kv_model:
        word_vectors.append(kv_model[END])
    else:
        rnd_vec = np.zeros(vector_size)
        word_vectors.append(rnd_vec)

    # process the rest words in vocab
    for w in vocab[2:]:
        w_vec = kv_model[w]
        word_vectors.append(w_vec)
    word_vectors = np.array(word_vectors)

    return vocab, word_vectors


def main():
    """ testing function
    using transform the keyedvectors to numpy then assign to
    nn.Embedding
    """

    # word_emb = nn.Embedding()
    kv_filepath = '../desc2perm/data/wv/android_w2v.kv'
    kv_model = KeyedVectors.load(kv_filepath)
    vocab, w_vectors = kv2np_array(kv_filepath)
    test_words = vocab[1:]

    for tw in tqdm(test_words):
        kv_vec = kv_model[tw]

        w_idx = vocab.index(tw)
        np_vec = w_vectors[w_idx]

        if np.array_equal(kv_vec, np_vec):
            # print('match')
            pass
        else:
            print('mismatch', tw)


if __name__ == "__main__":
    main()
