from .utils import load_data
import pickle
import argparse
from os.path import expanduser, join, basename


def main():
    """"""
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--train-data', required=True)
    arg_parser.add_argument('--val-data', required=True)
    arg_parser.add_argument('--test-data', required=True)
    arg_parser.add_argument('--data-type', help='doc | sent',
                            required=True)

    arg_parser.add_argument('--wv-path',
                            help="gensim KeyedVectors file; record vocabulary",
                            required=True)
    arg_parser.add_argument('--out-dir',
                            help="output folder save indexed data",
                            required=True)

    args = arg_parser.parse_args()
    setattr(args, 'processed', False)
    setattr(args, 'balance_train', False)  # do not balance train
    # expanduser
    args.train_data = expanduser(args.train_data)
    args.val_data = expanduser(args.val_data)
    args.test_data = expanduser(args.test_data)
    args.out_dir = expanduser(args.out_dir)

    train, val, test = load_data(args)

    train_file = basename(args.train_data)
    val_file = basename(args.val_data)
    test_file = basename(args.test_data)
    with open(join(args.out_dir, train_file+'.p'), 'wb') as ofile:
        pickle.dump(train, ofile)
    with open(join(args.out_dir, val_file+'.p'), 'wb') as ofile:
        pickle.dump(val, ofile)
    with open(join(args.out_dir, test_file+'.p'), 'wb') as ofile:
        pickle.dump(test, ofile)


if __name__ == '__main__':
    main()
