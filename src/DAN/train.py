import json
from os.path import expanduser, join
from time import localtime, strftime

import torch
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader

from .model import DAN
from .utils import (load_data, performance_test, Logger,
                    get_default_args_parser, log_pf)
from .data_utils import SentLevelData, customized_collate_fn


def get_optimizer(args, parameters):
    optimizers = {
        'sgd': torch.optim.SGD,
        'adam': torch.optim.Adam,
        'adagrad': torch.optim.Adagrad
    }
    optim = optimizers[args.optimizer]
    return optim(parameters, args.learning_rate)


class FakeSummaryWriter():
    """"""
    def __init__(self, **args):
        pass

    def add_scalar(self, tag, scalar_value, global_step=None, walltime=None):
        """"""
        pass

    def close(self):
        pass


def train(args, is_src):
    """
    """
    model = DAN(args)
    print(model)
    device = torch.device(args.device)
    model.to(device)
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = get_optimizer(args, parameters)
    # optimizer = torch.optim.Adagrad(parameters, lr=args.learning_rate)
    loss_fn = torch.nn.CrossEntropyLoss()

    if args.load_pretrain and is_src:
        print('load pretrain model:', args.load_pretrain)
        model.load_state_dict(torch.load(args.load_pretrain,
                                         map_location=args.device))
        return model

    if args.fake_summary_writer:
        summary_writer = FakeSummaryWriter()
    else:
        summary_writer = SummaryWriter(
            log_dir=join(args.expdir, args.exp_time))

    logger = Logger(join(args.expdir, args.exp_time, 'performance.log'))
    train_data, val_data, test_data = load_data(args)

    train_data = SentLevelData(train_data)

    global_step = 0
    best_val_f1 = {'sent': 0, 'doc': 0}
    for e in range(args.epoch):
        train_loader = DataLoader(train_data,
                                  batch_size=args.batch_size,
                                  collate_fn=customized_collate_fn,
                                  shuffle=True)
        correct = 0
        for b_idx, b_data in enumerate(train_loader):
            X, y = b_data
            y = torch.tensor(y, dtype=torch.int64, device=device)

            outputs = model(X)
            optimizer.zero_grad()
            loss = loss_fn(outputs, y)
            loss.backward()

            torch.nn.utils.clip_grad_norm_(
                parameters,
                max_norm=args.grad_norm_limit)

            optimizer.step()

            summary_writer.add_scalar('batch-loss',
                                      loss.item(), global_step)

            if global_step % 100 == 0:
                print('batch {} loss'.format(global_step+1), loss.item())
            global_step += 1

            # compute correct on batch
            _, pred = outputs.max(dim=1)
            correct += (pred == y).sum().float()

        print('epoch', e, 'train acc:', correct / len(train_data))

        # evaluation on validation and test
        val_pf = performance_test(args, model, device, val_data)
        test_pf = performance_test(args, model, device, test_data)
        print('epoch', e, 'val::', str(val_pf))
        logger.log(json.dumps({e: {'val': val_pf}}))
        print('epoch', e, 'test::', str(test_pf))
        logger.log(json.dumps({e: {'test': test_pf}}))

        save_curr = log_pf(args, e, summary_writer,
                           val_pf, test_pf, best_val_f1)
        if save_curr and args.save_model_to:
            torch.save(model.state_dict(),
                       join(args.expdir, args.exp_time,
                            args.save_model_to + '.best'))

    # save last model
    if args.save_model_to:
        torch.save(model.state_dict(),
                   join(args.expdir, args.exp_time,
                        args.save_model_to + '.last'))
    summary_writer.close()  # does not work actually, library bugs
    del summary_writer

    return model


def main():
    """"""
    arg_parser = get_default_args_parser()
    args = arg_parser.parse_args()

    if args.gpu < 0:
        print('using cpu')
        setattr(args, 'device', 'cpu')
    else:
        gpu_dev = 'cuda:' + str(args.gpu)
        print('using gpu', gpu_dev)
        setattr(args, 'device', gpu_dev)

    setattr(args, 'exp_time', strftime('%Y-%m-%d-%H:%M:%S',
                                       localtime()))
    if not args.wv_path and not args.vocab:
        raise Exception(
            "need word vector file (gensim KeyedVectors) or vocabulary file")

    # process poolings arg
    pooling_str = args.poolings
    poolings = pooling_str.split(',')
    args.poolings = poolings

    # process hidden layer structure
    hidden_str = args.hidden_layers
    hidden_layers = [int(i) for i in hidden_str.split(',')]
    args.hidden_layers = hidden_layers

    # expand user dir
    args.wv_path = expanduser(args.wv_path)
    if args.vocab:
        args.vocab = expanduser(args.vocab)
    if args.expdir:
        args.expdir = expanduser(args.expdir)
    if args.load_pretrain:
        args.load_pretrain = expanduser(args.load_pretrain)

    train(args)


if __name__ == '__main__':
    main()
