import json
from collections import OrderedDict
from os.path import expanduser, join
from time import localtime, strftime

import torch
from tensorboardX import SummaryWriter
from torch.utils.data import DataLoader

from .data_utils import SentLevelData, customized_collate_fn
from .model import DAN
from .utils import (Logger, get_default_args_parser, load_data, log_pf,
                    performance_test)


def source_constraints(args, model, source_weights, coefficients):
    """
    :type model: torch.nn.Module
    compute the additional loss
    """
    m_parameters = [n_p for n_p in model.named_parameters()
                    if n_p[1].requires_grad]
    rtn_loss = torch.tensor(0, dtype=torch.float, device=args.device)
    for idx, w in enumerate(source_weights):
        add_loss = torch.tensor(0, dtype=torch.float, device=args.device)
        for name, p in m_parameters:
            if name in w:
                diff = p - w[name]
                add_loss += torch.norm(diff)
        rtn_loss += coefficients[idx] * add_loss
    return rtn_loss


def get_source_models_weights(args):
    """
    read the file contains model path and coefficients

    :return:
    a list of weights
    """
    # expanduser
    args.source_models = expanduser(args.source_models)
    models_path = json.load(open(args.source_models))
    models_weights = []
    models_coefficients = []
    for m, c in models_path:
        m = expanduser(m)
        m_weights = torch.load(m, map_location=args.device)
        models_weights.append(m_weights)
        models_coefficients.append(c)

    return models_weights, models_coefficients


def train(args):
    """training with multiple source model regularization
    loss = loss_{cross_entropy} + \sum_{i} \lambda_i * || w - w_i||^2

    """
    source_models, source_lambdas = get_source_models_weights(args)

    model = DAN(args)
    device = torch.device(args.device)
    model.to(device)
    parameters = filter(lambda p: p.requires_grad, model.parameters())
    optimizer = torch.optim.Adagrad(parameters, lr=args.learning_rate)
    loss_fn = torch.nn.CrossEntropyLoss()

    if args.load_pretrain:
        print('load pretrain model:', args.load_pretrain)
        model.load_state_dict(torch.load(args.load_pretrain,
                                         map_location=args.device))

    summary_writer = SummaryWriter(
        log_dir=join(args.expdir, args.exp_time))
    logger = Logger(join(args.expdir, args.exp_time, 'performance.log'))
    train_data, val_data, test_data = load_data(args)

    train_data = SentLevelData(train_data)

    global_step = 0
    best_val_f1 = {'sent': 0, 'doc': 0}
    for e in range(args.epoch):
        train_loader = DataLoader(train_data,
                                  batch_size=args.batch_size,
                                  collate_fn=customized_collate_fn,
                                  shuffle=True)
        correct = 0
        for b_idx, b_data in enumerate(train_loader):
            X, y = b_data
            y = torch.tensor(y, dtype=torch.int64, device=device)

            outputs = model(X)
            optimizer.zero_grad()
            loss = loss_fn(outputs, y)

            # additional loss from weight constraints
            if args.penalty_factor > 0:
                added_loss = source_constraints(args, model,
                                                source_models,
                                                source_lambdas)
                loss += args.penalty_factor * added_loss

            loss.backward()

            torch.nn.utils.clip_grad_norm_(
                parameters,
                max_norm=args.grad_norm_limit)

            optimizer.step()

            summary_writer.add_scalar('batch-loss',
                                      loss.item(), global_step)

            if global_step % 100 == 0:
                print('batch {} loss'.format(global_step+1), loss.item())
            global_step += 1

            # compute correct on batch
            _, pred = outputs.max(dim=1)
            correct += (pred == y).sum().float()

        print('epoch', e, 'train acc:', correct / len(train_data))

        # evaluation on validation and test
        val_pf = performance_test(args, model, device, val_data)
        test_pf = performance_test(args, model, device, test_data)
        print('epoch', e, 'val::', str(val_pf))
        logger.log(json.dumps({e: {'val': val_pf}}))
        print('epoch', e, 'test::', str(test_pf))
        logger.log(json.dumps({e: {'test': test_pf}}))

        save_curr = log_pf(args, e, summary_writer,
                           val_pf, test_pf, best_val_f1)
        if save_curr and args.save_model_to:
            torch.save(model.state_dict(),
                       join(args.expdir,
                            args.save_model_to + '.best'))

    # save last model
    if args.save_model_to:
        torch.save(model.state_dict(),
                   join(args.expdir, args.save_model_to + '.last'))


def main():
    """"""
    arg_parser = get_default_args_parser()
    arg_parser.add_argument('--source-models',
                            help="JSON file records path for source models",
                            required=True)
    arg_parser.add_argument('--penalty-factor', type=float,
                            help="coefficient of the penalty term",
                            default=0.5)

    args = arg_parser.parse_args()

    if args.gpu < 0:
        print('using cpu')
        setattr(args, 'device', 'cpu')
    else:
        gpu_dev = 'cuda:' + str(args.gpu)
        print('using gpu', gpu_dev)
        setattr(args, 'device', gpu_dev)

    setattr(args, 'exp_time', strftime('%Y-%m-%d-%H:%M:%S',
                                         localtime()))
    if not args.wv_path and not args.vocab:
        raise Exception(
            "need word vector file (gensim KeyedVectors) or vocabulary file")

    # process poolings arg
    pooling_str = args.poolings
    poolings = pooling_str.split(',')
    args.poolings = poolings

    # process hidden layer structure
    hidden_str = args.hidden_layers
    hidden_layers = [int(i) for i in hidden_str.split(',')]
    args.hidden_layers = hidden_layers

    # expand user dir
    args.wv_path = expanduser(args.wv_path)
    if args.vocab:
        args.vocab = expanduser(args.vocab)
    if args.expdir:
        args.expdir = expanduser(args.expdir)
    if args.load_pretrain:
        args.load_pretrain = expanduser(args.load_pretrain)

    train(args)


if __name__ == '__main__':
    main()
