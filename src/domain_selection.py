import json
import math

import enchant
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords, words
from nltk.tokenize import TweetTokenizer, sent_tokenize, word_tokenize

dict = enchant.Dict("en_US")
tknzr = TweetTokenizer()


en_words = set(words.words())
en_stopwords = set(stopwords.words('english'))


def is_word(word):
    return dict.check(word)


def is_stopword(word):
    return bool(word in en_stopwords)


def get_unigram(corpus, remove_stopwords=True):
    """ corpus consist of words
    """
    unigram = {}

    for w in corpus:
        w = w.lower()
        if is_word(w) and (remove_stopwords and not is_stopword(w)):
            if w in unigram:
                unigram[w] += 1
            else:
                unigram[w] = 1
    length_of_corpus = float(sum(unigram.values()))
    for w in unigram:
        unigram[w] /= length_of_corpus

    return unigram


def get_corpus_from_labeled_data(filename):
    """ parse the json file extract sentences"""
    corpus = []
    with open(filename, 'r', encoding='utf-8') as ifile:
        data = json.load(ifile)
        for row in data:
            sentence = row['sentence']
            tokens = tknzr.tokenize(sentence.lower())
            corpus.extend(tokens)
    return corpus


def get_corpus_from_ifttt_data(filename):
    """ data at ifttt platform currently follow different format"""
    corpus = []
    with open(filename, 'r', encoding='utf-8') as ifile:
        data = json.load(ifile)
        for row in data:
            sentence = row['recipe_title'] + ' ' + row['recipe_desc']
            tokens = tknzr.tokenize(sentence.lower())
            corpus.extend(tokens)
    return corpus


def cal_perplexity(unigram, corpus_file):
    """compute perplexity of corpus
    based on provided unigram"""
    sum_log_p = 0.0
    words_count = 0
    with open(corpus_file, 'r') as corpus_f:
        for line in corpus_f:
            hashid, review = line.split(' ', 1)
            sentences = sent_tokenize(review)
            for sent in sentences:
                words = word_tokenize(sent)
                # words_count += len(words)
                sent_p = 1.0
                for w in words:
                    w = w.lower()
                    if w in unigram:
                        words_count += 1
                        sent_p *= unigram[w]
                if sent_p == 0.0:
                    sent_p = 1e-50

                log_p = math.log(sent_p, 2)
                sum_log_p += log_p

    y = -1 * sum_log_p / words_count
    p = pow(2, y)
    return p


# def domain_size(domains):
#     for d in domains:
#         with open(d, 'r') as d_f:
#             words_count = 0
#             for line in d_f:
#                 hashid, review = line.split(' ', 1)
#                 sentences = sent_tokenize(review)
#                 for sent in sentences:
#                     words = word_tokenize(sent)
#                     words_count += len(words)
#             print('{} contains {} words'.format(d, words_count))


def cal_KL_divergence(P, Q):
    """ refer to wikipedia"""
    D_KL = 0
    for w in P:
        P_i = P[w]
        if w in Q:
            Q_i = Q[w]
        else:
            Q_i = 1e-7

        tmp = P_i * math.log(P_i / Q_i)
        D_KL += tmp
    return D_KL


def main():
    """"""
    src_domains = ['../data/android_camera_500.json',
                   '../data/android_read_cal_whyper.json',
                   '../data/android_read_con_whyper.json',
                   '../data/android_record_audio_whyper.json',
                   '../data/android_modify_contacts_546.json']

    tgt_domains = ['../data/ifttt_fb_action.json.transformed.json',
                   '../data/ifttt_gcal_trigger.json.transformed.json',
                   '../data/ifttt_gcon_trigger.json.transformed.json']

    src_unigrams = []
    tgt_unigram = []

    for filename in src_domains:
        corpus = get_corpus_from_labeled_data(filename)
        unigram = get_unigram(corpus)
        src_unigrams.append(unigram)
    for filename in tgt_domains:
        corpus = get_corpus_from_labeled_data(filename)
        unigram = get_unigram(corpus)
        tgt_unigram.append(unigram)


    for i, s_filename in enumerate(src_domains):
        P_unigram = src_unigrams[i]
        for j, t_filename in enumerate(tgt_domains):
            Q_unigram = tgt_unigram[j]
            kl_val = cal_KL_divergence(P_unigram, Q_unigram)
            print('{} <-> {}: {}'.format(s_filename, t_filename, kl_val))

    print('='*40)

    for i, t_filename in enumerate(tgt_domains):
        P_unigram = tgt_unigram[i]
        for j, s_filename in enumerate(src_domains):
            Q_unigram = src_unigrams[j]
            kl_val = cal_KL_divergence(P_unigram, Q_unigram)
            print("{} <-> {}: {}".format(t_filename, s_filename, kl_val))

    # for i in range(5):
    #     d_kl = cal_KL_divergence(target_unigram, domain_unigram[i])
    #     print('{} with target domain KL divergence {}'.format(extra_domains[i], d_kl))
    #     print('-' * 40)

    # for i in range(5):
    #     for j in range(5):
    #         if j is not i:
    #             D_KL = cal_KL_divergence(domain_unigram[i], domain_unigram[j])
    #             print('KL Divergence between {} and {}'.format(extra_domains[i], extra_domains[j]))
    #             print(D_KL)
    #             print('-' * 40)


if __name__ == '__main__':
    main()
